# Create an aws account

Here's an overview of the process:

 ![flowchart_create_activate_screenshot](assets/flowchart_create_activate_screenshot.03c94286f3bd003ef89200a1d0ece97eaf42b971.PNG) 

###   Create your account 

1. Go to the [Amazon Web Services home page](https://aws.amazon.com/).

2. Choose **Sign Up**.
    **Note:** If you've signed in to AWS recently, it might say **Sign In to the Console**.

 ![000085-AWS Console - Signup](assets/000085-AWS Console - Signup.png)

3. Type the requested account information, and then choose **Continue**.
    **Note:** If **Create a new AWS account** isn't visible, first choose **Sign in to a different account**, and then choose **Create a new AWS account**. When creating a new account, be sure that you enter your account information correctly, especially your email address. If you enter your email address incorrectly, you might not be able to access your account or change your password in the future.

4. Choose **Personal** or **Professional**. (I choose **Personal**)
    **Note:** These two account types are identical in functionality.

5. Type the requested company or personal information.

   ![000087-AWS Console - Signup](assets/000087-AWS Console - Signup.png)

6. Read the [AWS Customer Agreement](https://aws.amazon.com/agreement/), and then check the box.

7. Choose **Create Account and Continue**.

**Note:** After you receive an email to confirm that your account is created, you can sign in to your new account using the email address and password you supplied. However, you must continue with the activation process before you can use AWS services.

###   Add a payment method 

![000088-AWS Console - Signup](assets/000088-AWS Console - Signup.png)

On the **Payment Information** page, type the requested information associated with your payment method. If the address for your payment method is the same as the address you provided for your account, choose **Secure Submit**.

Otherwise, choose **Use a new address**, type the billing address for your payment method, and then choose **Secure Submit**.

###   Verify your phone number 

1. On the **Phone Verification** page, type a phone number that you can use to accept incoming phone calls.
2. Enter the code displayed in the captcha.
3. When you're ready to receive a call, choose **Call me now**. In a few moments, an automated system will call you.
4. Type the provided PIN on your phone's keypad. After the process is complete, choose **Continue**.

###   Choose an AWS Support plan 

![000089-AWS Console - Signup](assets/000089-AWS Console - Signup-1528336718523.png)

On the **Select a Support Plan** page, choose one of the available Support plans. For a description of the available Support plans and their benefits, see [AWS Support - Features](https://aws.amazon.com/premiumsupport/features/).

After you choose a Support plan, a confirmation page indicates that your account is being activated. Accounts are usually activated within a few minutes, but the process might take up to 24 hours.

**Note:** You can sign in to your AWS account during this time. The AWS home page might continue to display a button that shows "Complete Sign Up" during this time, even if you've completed all the steps in the sign-up process.

When your account is fully activated, you'll receive a confirmation email. After you receive this email, you have full access to all AWS services.

# What is a root account ? IAM account ? what is the difference ? What is the best pratice ? List them.

## The AWS Account Root User

When you first create an Amazon Web Services (AWS) account, you begin with a single sign-in identity that has complete access to all AWS services and resources in the account. This identity is called the AWS account *root user* and is accessed by signing in with the email address and password that you used to create the account.

## What Is IAM?

AWS Identity and Access Management (IAM) is a web service that helps you securely control access to AWS resources. You use IAM to control who is authenticated (signed in) and authorized (has permissions) to use resources. 

 When you first create an AWS account, you begin with a single sign-in identity that has complete access to all AWS services and resources in the account. This identity is called the AWS account *root user* and is accessed by signing in with the email address and password that you used to create the account. We strongly recommend that you do not use the root user for your everyday tasks, even the administrative ones. Instead, adhere to the [best practice of using the root user only to create your first IAM user](http://docs.aws.amazon.com/IAM/latest/UserGuide/best-practices.html#create-iam-users). Then securely lock away the root user credentials and use them to perform only a few account and service management tasks. 

## AWS Account Root User Credentials vs. IAM User Credentials

All AWS accounts have root user credentials (that is, the credentials of the account owner). These credentials allow full access to all resources in the account.

For a list of tasks that require root user access:

- [Modify root user details](https://docs.aws.amazon.com/general/latest/gr/aws-sec-cred-types.html#email-and-password-for-your-AWS-account). This includes changing the root user's password. 
- [Change your AWS support plan](http://docs.aws.amazon.com/awssupport/latest/user/getting-started.html). 
- [Change or delete your payment options](http://docs.aws.amazon.com/awsaccountbilling/latest/aboutv2/manage-payments.html) - an IAM user can perform this after you enable billing access for IAM users. For more information, see [Activating Access to the Billing and Cost Management Console](http://docs.aws.amazon.com/awsaccountbilling/latest/aboutv2/grantaccess.html#ControllingAccessWebsite-Activate). 
- [View your account's billing information](http://docs.aws.amazon.com/awsaccountbilling/latest/aboutv2/getting-viewing-bill.html) - an IAM user can perform this after you enable billing access for IAM users. For more information, see [Activating Access to the Billing and Cost Management Console](http://docs.aws.amazon.com/awsaccountbilling/latest/aboutv2/grantaccess.html#ControllingAccessWebsite-Activate)
- [Close an AWS account](http://docs.aws.amazon.com/awsaccountbilling/latest/aboutv2/close-account.html). 
- [Sign up for GovCloud](http://docs.aws.amazon.com/govcloud-us/latest/UserGuide/getting-started-sign-up.html). 
- [Submit a Reverse DNS for Amazon EC2 request](https://aws.amazon.com/blogs/aws/reverse-dns-for-ec2s-elastic-ip-addresses/). The "[this form](https://aws-portal.amazon.com/gp/aws/html-forms-controller/contactus/ec2-email-limit-rdns-request)" link on that page to submit a request works only if you sign in with root user credentials. 
- [Create a CloudFront key pair](http://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/private-content-trusted-signers.html#private-content-creating-cloudfront-key-pairs). 
- [Create an AWS-created X.509 signing certificate](http://docs.aws.amazon.com/AmazonDevPay/latest/DevPayDeveloperGuide/X509Certificates.html#UsingAWSCertificate). (You can still make self-created certificates for IAM users.) 
- [Transfer an Route 53 domain to another AWS account](http://docs.aws.amazon.com/Route53/latest/DeveloperGuide/domain-transfer-between-aws-accounts.html). 
- [Change the Amazon EC2 setting for longer resource IDs](http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/resource-ids.html#resource-ids-working-with-self). Changing this setting as the root user affects all users and roles in the account. Changing it as an IAM user or IAM role affects only that user or role. 
- [Submit a request to perform penetration testing on your AWS infrastructure using the web form](https://aws.amazon.com/premiumsupport/knowledge-center/penetration-testing/). Alternatively, you can submit your request via email without needing root user access. 
- [Request removal of the port 25 email throttle on your EC2 instance](https://aws.amazon.com/premiumsupport/knowledge-center/ec2-port-25-throttle/). 
- [Find your AWS account canonical user ID](https://docs.aws.amazon.com/general/latest/gr/acct-identifiers.html#FindingCanonicalId). You can view your canonical user ID from the AWS Management Console only while signed in as the AWS account root user. You can view your canonical user ID as an IAM user with the AWS API or AWS CLI.
- [Reassigning permissions in a resource-based policy (such as an S3 bucket policy) that were revoked by explicitly denying IAM users access.](http://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_manage-edit.html) Root users are not blocked by an explicit deny like IAM users can be. 

# IAM Best Practices

To help secure your AWS resources, follow these recommendations for the AWS Identity and Access Management (IAM) service. 

## Lock Away Your AWS Account Root User Access Keys 

You use an access key (an access key ID and secret access key) to make programmatic requests to AWS. However, do not use your AWS account root user access key. The access key for your AWS account gives full access to all your resources for all AWS services, including your billing information. You cannot restrict the permissions associated with your AWS account access key. 

Therefore, protect your AWS account access key like you would your credit card numbers or any other sensitive secret. Here are some ways to do that: 

- If you don't already have an access key for your AWS account, don't create one unless you absolutely need to. Instead, use your account email address and password to sign in to the [AWS Management Console](https://console.aws.amazon.com/) and create an IAM user for yourself that has administrative privileges, as explained in the next section. 
- If you do have an access key for your AWS account, delete it. If you must keep it, rotate (change) the access key regularly. To delete or rotate your AWS account access keys, go to the [Security Credentials page](https://console.aws.amazon.com/iam/home?#security_credential) in the AWS Management Console and sign in with your account's email address and password. You can manage your access keys in the **Access keys** section. 
- Never share your AWS account password or access keys with anyone. The remaining sections of this document discuss various ways to avoid having to share your AWS account root user credentials with other users and to avoid having to embed them in an application. 
- Use a strong password to help protect account-level access to the AWS Management Console. For information about managing your AWS account root user password, see [Changing the AWS Account Root User Password](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_passwords_change-root.html). 
- Enable AWS multi-factor authentication (MFA) on your AWS account. For more information, see [Using Multi-Factor Authentication (MFA) in AWS](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_mfa.html). 

## Create Individual IAM Users

Don't use your AWS account root user credentials to access AWS, and don't give your credentials to anyone else. Instead, create individual users for anyone who needs access to your AWS account. Create an IAM user for yourself as well, give that user administrative privileges, and use that IAM user for all your work. For information about how to do this, see [Creating Your First IAM Admin User and Group](https://docs.aws.amazon.com/IAM/latest/UserGuide/getting-started_create-admin-group.html). 

By creating individual IAM users for people accessing your account, you can give each IAM user a unique set of security credentials. You can also grant different permissions to each IAM user. If necessary, you can change or revoke an IAM user's permissions any time. (If you give out your root user credentials, it can be difficult to revoke them, and it is impossible to restrict their permissions.) 

## Use Groups to Assign Permissions to IAM Users 

Instead of defining permissions for individual IAM users, it's usually more convenient to create groups that relate to job functions (administrators, developers, accounting, etc.). Next, define the relevant permissions for each group. Finally, assign IAM users to those groups. All the users in an IAM group inherit the permissions assigned to the group. That way, you can make changes for everyone in a group in just one place. As people move around in your company, you can simply change what IAM group their IAM user belongs to. 

## Use AWS Defined Policies to Assign Permissions Whenever Possible 

We recommend that you use the managed policies that are created and maintained by AWS to grant permissions whenever possible. A key advantage of using these policies is that they are maintained and updated by AWS as new services or new API operations are introduced. 

AWS-managed policies are designed to support common tasks. They typically provide access to a single service or a limited set of actions. For more information about AWS managed policies, see [AWS Managed Policies](https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_managed-vs-inline.html#aws-managed-policies). 

AWS managed policies for job functions can span multiple services and align with common job functions in the IT industry. For a list and descriptions of job function policies, see [AWS Managed Policies for Job Functions](https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_job-functions.html). 

## Grant Least Privilege

When you create IAM policies, follow the standard security advice of granting *least privilege*—that is, granting only the permissions required to perform a task. Determine what users need to do and then craft policies for them that let the users perform *only* those tasks. 

Start with a minimum set of permissions and grant additional permissions as necessary. Doing so is more secure than starting with permissions that are too lenient and then trying to tighten them later. 

You can use access level groupings to understand the level of access that a policy grants. [Policy actions](https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_policies_elements_action.html) are classified as `List`, `Read`, `Write`, `Permissions management`, or `Tagging`. For example, you can choose actions from the `List` and `Read` access levels to grant read-only access to your users. To learn how to use policy summaries to understand access level permissions, see [Use Access Levels to Review IAM Permissions](https://docs.aws.amazon.com/IAM/latest/UserGuide/best-practices.html#use-access-levels-to-review-permissions). 

One feature that can help with this is the **Access Advisor** tab. This tab is available on the IAM console details page whenever you inspect a user, group, role, or policy. The tab includes information about which services are actually used by a user, group, role, or by anyone that uses a policy. You can use this information to identify unnecessary permissions so that you can refine your IAM policies to better adhere to the principle of least privilege. For more information, see [Reducing Policy Scope by Viewing User Activity](https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_access-advisor.html). 

For more information, see the following:

- [Access Management](https://docs.aws.amazon.com/IAM/latest/UserGuide/access.html)
- Policy topics for individual services, which provide examples of how to write policies for service-specific resources. Examples: 
  - [Authentication and Access Control for Amazon DynamoDB](http://docs.aws.amazon.com/amazondynamodb/latest/developerguide/UsingIAMWithDDB.html) in the *Amazon DynamoDB Developer Guide*
  - [Using Bucket Policies and User Policies](http://docs.aws.amazon.com/AmazonS3/latest/dev/using-iam-policies.html) in the *Amazon Simple Storage Service Developer Guide*
  - [Access Control List (ACL) Overview](http://docs.aws.amazon.com/AmazonS3/latest/dev/acl-overview.html) in the *Amazon Simple Storage Service Developer Guide*

## Use Access Levels to Review IAM Permissions 

To improve the security of your AWS account, you should regularly review and monitor each of your IAM policies. Make sure that your policies grant the [least privilege](https://docs.aws.amazon.com/IAM/latest/UserGuide/best-practices.html#grant-least-privilege) that is needed to perform only the necessary actions. 

When you review a policy, you can view the [policy summary](https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_understand.html) that includes a summary of the access level for each service within that policy. AWS categorizes each service action into one of four *access levels* based on what each action does: `List`, `Read`, `Write`, or `Permissions management`. You can use these access levels to determine which actions to include in your policies. 

For example, in the Amazon S3 service, you might want to allow a large group of users to access `List` and `Read` actions. Such actions permit those users to list the buckets and get objects in Amazon S3. However, you should allow only a small group of users to access the Amazon S3 `Write` actions to delete buckets or put objects into an S3 bucket. Additionally, you should restrict permissions to allow only administrators to access the Amazon S3 `Permissions management` actions. This ensures that only a limited number of people can manage bucket policies in Amazon S3. This is especially important for `Permissions management` actions in IAM and AWS Organizations services. 

To view the access level classification that is assigned to each action in a service, see [Actions, Resources, and Condition Keys for AWS Services](https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_policies_actions-resources-contextkeys.html). 

To see the access levels for a policy, you must first locate the policy's summary. The policy summary is included on the **Policies** page for managed policies, and on the **Users** page for policies that are attached to a user. For more information, see [Policy Summary (List of Services)](https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_understand-policy-summary.html). 

Within a policy summary, the **Access level** column shows that the policy provides **Full** or **Limited** access to one or more of the four AWS access levels for the service. Alternately, it might show that the policy provides **Full access** to all the actions within the service. You can use the information within this **Access level** column to understand the level of access that the policy provides. You can then take action to make your AWS account more secure. For details and examples of the access level summary, see [Understanding Access Level Summaries Within Policy Summaries](https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_understand-policy-summary-access-level-summaries.html). 

## Configure a Strong Password Policy for Your Users 

If you allow users to change their own passwords, require that they create strong passwords and that they rotate their passwords periodically. On the [Account Settings](https://console.aws.amazon.com/iam/home?#account_settings) page of the IAM console, you can create a password policy for your account. You can use the password policy to define password requirements, such as minimum length, whether it requires non-alphabetic characters, how frequently it must be rotated, and so on. 

For more information, see [Setting an Account Password Policy for IAM Users](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_passwords_account-policy.html). 

## Enable MFA for Privileged Users

For extra security, enable multi-factor authentication (MFA) for privileged IAM users (users who are allowed access to sensitive resources or API operations). With MFA, users have a device that generates a unique authentication code (a one-time password, or OTP). Users must provide both their normal credentials (like their user name and password) and the OTP. The MFA device can either be a special piece of hardware, or it can be a virtual device (for example, it can run in an app on a smartphone). 

For more information, see [Using Multi-Factor Authentication (MFA) in AWS](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_mfa.html). 

## Use Roles for Applications That Run on Amazon EC2 Instances 

Applications that run on an Amazon EC2 instance need credentials in order to access other AWS services. To provide credentials to the application in a secure way, use IAM *roles*. A role is an entity that has its own set of permissions, but that isn't a user or group. Roles also don't have their own permanent set of credentials the way IAM users do. In the case of Amazon EC2, IAM dynamically provides temporary credentials to the EC2 instance, and these credentials are automatically rotated for you. 

When you launch an EC2 instance, you can specify a role for the instance as a launch parameter. Applications that run on the EC2 instance can use the role's credentials when they access AWS resources. The role's permissions determine what the application is allowed to do. 

For more information, see [Using an IAM Role to Grant Permissions to Applications Running on Amazon EC2 Instances](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_use_switch-role-ec2.html). 

## Delegate by Using Roles Instead of by Sharing Credentials 

You might need to allow users from another AWS account to access resources in your AWS account. If so, don't share security credentials, such as access keys, between accounts. Instead, use IAM roles. You can define a role that specifies what permissions the IAM users in the other account are allowed. You can also designate which AWS accounts have the IAM users that are allowed to assume the role. 

For more information, see [Roles Terms and Concepts](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_terms-and-concepts.html). 

## Rotate Credentials Regularly

Change your own passwords and access keys regularly, and make sure that all IAM users in your account do as well. That way, if a password or access key is compromised without your knowledge, you limit how long the credentials can be used to access your resources. You can apply a password policy to your account to require all your IAM users to rotate their passwords, and you can choose how often they must do so. 

For more information about setting a password policy in your account, see [Setting an Account Password Policy for IAM Users](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_passwords_account-policy.html). 

For more information about rotating access keys for IAM users, see [Rotating Access Keys](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_access-keys.html#Using_RotateAccessKey). 

## Remove Unnecessary Credentials

Remove IAM user credentials (that is, passwords and access keys) that are not needed. For example, an IAM user that is used for an application does not need a password (passwords are necessary only to sign in to AWS websites). Similarly, if a user does not and will never use access keys, there's no reason for the user to have them. Passwords and access keys that have not been used recently might be good candidates for removal. You can find unused passwords or access keys using the console, using the API, or by downloading the credentials report. 

For more information about finding IAM user credentials that have not been used recently, see [Finding Unused Credentials](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_finding-unused.html). 

For more information about deleting passwords for an IAM user, see [Managing Passwords for IAM Users](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_passwords_admin-change-user.html). 

For more information about deactivating or deleting access keys for an IAM user, see [Managing Access Keys for IAM Users](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_access-keys.html). 

For more information about IAM credential reports, see [Getting Credential Reports for Your AWS Account](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_getting-report.html). 

## Use Policy Conditions for Extra Security

To the extent that it's practical, define the conditions under which your IAM policies allow access to a resource. For example, you can write conditions to specify a range of allowable IP addresses that a request must come from. You can also specify that a request is allowed only within a specified date range or time range. You can also set conditions that require the use of SSL or MFA (multi-factor authentication). For example, you can require that a user has authenticated with an MFA device in order to be allowed to terminate an Amazon EC2 instance. 

For more information, see [IAM JSON Policy Elements: Condition](https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_policies_elements_condition.html) in the IAM Policy Elements Reference. 

## Monitor Activity in Your AWS Account

You can use logging features in AWS to determine the actions users have taken in your account and the resources that were used. The log files show the time and date of actions, the source IP for an action, which actions failed due to inadequate permissions, and more. 

Logging features are available in the following AWS services:

- [Amazon CloudFront](https://aws.amazon.com/cloudfront/) – Logs user requests that CloudFront receives. For more information, see [Access Logs](http://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/AccessLogs.html) in the *Amazon CloudFront Developer Guide*. 
- [AWS CloudTrail](https://aws.amazon.com/cloudtrail/) – Logs AWS API calls and related events made by or on behalf of an AWS account. For more information, see the [*AWS CloudTrail User Guide*](http://docs.aws.amazon.com/awscloudtrail/latest/userguide/). 
- [Amazon CloudWatch](https://aws.amazon.com/cloudwatch/) – Monitors your AWS Cloud resources and the applications you run on AWS. You can set alarms in CloudWatch based on metrics that you define. For more information, see the [*Amazon CloudWatch User Guide*](http://docs.aws.amazon.com/AmazonCloudWatch/latest/DeveloperGuide/). 
- [AWS Config](https://aws.amazon.com/config/) – Provides detailed historical information about the configuration of your AWS resources, including your IAM users, groups, roles, and policies. For example, you can use AWS Config to determine the permissions that belonged to a user or group at a specific time. For more information, see the [*AWS Config Developer Guide*](http://docs.aws.amazon.com/config/latest/developerguide/). 
- [Amazon Simple Storage Service (Amazon S3)](https://aws.amazon.com/s3/) – Logs access requests to your Amazon S3 buckets. For more information, see [Server Access Logging](http://docs.aws.amazon.com/AmazonS3/latest/dev/ServerLogs.html) in the *Amazon Simple Storage Service Developer Guide*. 

## Video Presentation About IAM Best Practices

The following video includes a conference presentation that covers these best practices and shows additional details about how to work with the features discussed here. 

[![AWS re:Invent 2015: IAM Best Practices to Live By (SEC302)](http://img.youtube.com/vi/_wiGpBQGCjU/0.jpg)](http://www.youtube.com/watch?v=_wiGpBQGCjU)

## Region and Availability Zone Concepts

Each region is completely independent. Each Availability Zone is isolated, but the Availability Zones in a region are connected through low-latency links. The following diagram illustrates the relationship between regions and Availability Zones. 

![aws_regions](assets/aws_regions.png)	 

Amazon EC2 resources are either global, tied to a region, or tied to an Availability Zone. For more information, see [Resource Locations](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/resources.html). 

### Regions

Each Amazon EC2 region is designed to be completely isolated from the other Amazon EC2 regions. This achieves the greatest possible fault tolerance and stability. 

When you view your resources, you'll only see the resources tied to the region you've specified. This is because regions are isolated from each other, and we don't replicate resources across regions automatically. 

When you launch an instance, you must select an AMI that's in the same region. If the AMI is in another region, you can copy the AMI to the region you're using. For more information, see [Copying an AMI](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/CopyingAMIs.html). 

Note that there is a charge for data transfer between regions. For more information, see [Amazon EC2 Pricing - Data Transfer](https://aws.amazon.com/ec2/pricing/on-demand/#Data_Transfer). 

### Availability Zones

When you launch an instance, you can select an Availability Zone or let us choose one for you. If you distribute your instances across multiple Availability Zones and one instance fails, you can design your application so that an instance in another Availability Zone can handle requests. 

You can also use Elastic IP addresses to mask the failure of an instance in one Availability Zone by rapidly remapping the address to an instance in another Availability Zone. For more information, see [Elastic IP Addresses](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/elastic-ip-addresses-eip.html). 

An Availability Zone is represented by a region code followed by a letter identifier; for example, `us-east-1a`. To ensure that resources are distributed across the Availability Zones for a region, we independently map Availability Zones to identifiers for each account. For example, your Availability Zone `us-east-1a` might not be the same location as `us-east-1a` for another account. There's no way for you to coordinate Availability Zones between accounts. 

As Availability Zones grow over time, our ability to expand them can become constrained. If this happens, we might restrict you from launching an instance in a constrained Availability Zone unless you already have an instance in that Availability Zone. Eventually, we might also remove the constrained Availability Zone from the list of Availability Zones for new customers. Therefore, your account might have a different number of available Availability Zones in a region than another account. 

You can list the Availability Zones that are available to your account. For more information, see [Describing Your Regions and Availability Zones](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-regions-availability-zones.html#using-regions-availability-zones-describe). 

**Available Regions**

Your account determines the regions that are available to you. For example:

- An AWS account provides multiple regions so that you can launch Amazon EC2 instances in locations that meet your requirements. For example, you might want to launch instances in Europe to be closer to your European customers or to meet legal requirements. 
- An AWS GovCloud (US) account provides access to the AWS GovCloud (US) region only. For more information, see [AWS GovCloud (US) Region](https://aws.amazon.com/govcloud-us/). 
- An Amazon AWS (China) account provides access to the China (Beijing) region only. 

The following table lists the regions provided by an AWS account. You can't describe or access additional regions from an AWS account, such as AWS GovCloud (US) or China (Beijing). 

| **Code**       | **Name**                   |
| -------------- | -------------------------- |
| us-east-1      | US East (N. Virginia)      |
| us-east-2      | US East (Ohio)             |
| us-west-1      | US West (N. California)    |
| us-west-2      | US West (Oregon)           |
| ca-central-1   | Canada (Central)           |
| eu-central-1   | EU (Frankfurt)             |
| eu-west-1      | EU (Ireland)               |
| eu-west-2      | EU (London)                |
| eu-west-3      | EU (Paris)                 |
| ap-northeast-1 | Asia Pacific (Tokyo)       |
| ap-northeast-2 | Asia Pacific (Seoul)       |
| ap-northeast-3 | Asia Pacific (Osaka-Local) |
| ap-southeast-1 | Asia Pacific (Singapore)   |
| ap-southeast-2 | Asia Pacific (Sydney)      |
| ap-south-1     | Asia Pacific (Mumbai)      |
| sa-east-1      | South America (São Paulo)  |

For more information, see [AWS Global Infrastructure](https://aws.amazon.com/about-aws/global-infrastructure/). 

The number and mapping of Availability Zones per region may vary between AWS accounts. To get a list of the Availability Zones that are available to your account, you can use the Amazon EC2 console or the command line interface. For more information, see [Describing Your Regions and Availability Zones](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-regions-availability-zones.html#using-regions-availability-zones-describe). 

## What Is the AWS Management Console?

The [AWS Management Console](https://console.aws.amazon.com/) is a web application that comprises and refers to a broad collection of service consoles for managing Amazon Web Services. When you first sign in, you see the console home page. 

 ![img](assets/console-home.png) 

The home page provides access to each service console as well as an intuitive user interface for exploring AWS and getting helpful tips. Among other things, the individual service consoles offer tools for working with [Amazon S3](https://console.aws.amazon.com/s3/) buckets, launching and connecting to [Amazon EC2](https://console.aws.amazon.com/ec2/) instances, setting [Amazon CloudWatch](https://console.aws.amazon.com/cloudwatch/) alarms, and getting information about your account and about [billing](https://console.aws.amazon.com/billing/). 

 ![img](assets/console-parts-new.png) 

# Overview of Access Management: Permissions and Policies 

The access management portion of AWS Identity and Access Management (IAM) helps you to define what a user or other entity is allowed to do in an account, often referred to as *authorization*. Permissions are granted through policies. A policy is an entity in AWS that, when attached to an identity or resource, defines their permissions. AWS evaluates these policies when a principal, such as a user, makes a request. Permissions in the policies determine whether the request is allowed or denied. Policies are stored in AWS as JSON documents attached to principals as *identity-based policies*, or to resources as *resource-based policies*. 

## Policies and Users

By default, IAM users can't access anything in your account. You grant permissions to a user by creating an identity-based policy, which is a policy attached to the user. The following example shows a policy that grants permission to perform all Amazon DynamoDB actions (`dynamodb:*`) on the Books table in the account 123456789012 within the us-east-2 region. 

```
{
 "Version": "2012-10-17",
 "Statement": {
 "Effect": "Allow",
 "Action": "dynamodb:*",
 "Resource": "arn:aws:dynamodb:us-east-2:123456789012:table/Books"
 }
}
```

 When you attach the policy to an IAM user, then the user has those DynamoDB permissions. Typically, users in your account have multiple policies that together represent the permissions for that user. 

Any actions or resources that are not explicitly allowed are denied by default. For example, if the above policy is the only policy attached to a user, then that user is allowed to perform DynamoDB actions on the Books table only. Actions on all other tables are prohibited. Similarly, the user is not allowed to perform any actions in Amazon EC2, Amazon S3, or in any other AWS service, because permissions to work with those services are not included in the policy. 

The IAM console includes *policy summary* tables that describe the access level, resources, and conditions that are allowed or denied for each service in a policy. Policies are summarized in three tables: the [policy summary](https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_understand-policy-summary.html), the [service summary](https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_understand-service-summary.html), and the [action summary](https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_understand-action-summary.html). The *policy summary* table includes a list of services. Choose a service there to see the *service summary*. This summary table includes a list of the actions and associated permissions for the chosen service. You can choose an action from that table to view the *action summary*. This table includes a list of resources and conditions for the chosen action. 

 ![ Policy summaries diagram image that illustrates the 3 tables and their relationship ](assets/policy_summaries-diagram.png) 

You can view policy summaries on the **Users** page for all policies (managed and inline) that are attached to that user. View summaries on the **Policies** page for all managed policies. 

For example, the previous policy is summarized in the AWS Management Console as follows:

 ![ DynamoDB Example Summary ](assets/policies-summary-dynamodbexample.png) 

You can also view the JSON document for the policy. For information about viewing the summary or JSON document, see [Understanding Permissions Granted by a Policy](https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_understand.html). 

## Policies and Groups

You can organize IAM users into *IAM groups* and attach a policy to a group. In that case, individual users still have their own credentials, but all the users in a group have the permissions that are attached to the group. Use groups for easier permissions management, and to follow our [IAM Best Practices](https://docs.aws.amazon.com/IAM/latest/UserGuide/best-practices.html). 

 ![ Users can be organized into groups to make it easier to manage permissions, because users have the permissions assigned to a group. ](assets/iam-intro-users-and-groups.diagram.png) 

Users or groups can have multiple policies attached to them that grant different permissions. In that case, the users' permissions are calculated based on the combination of policies. But the basic principle still applies: If the user has not been granted an explicit permission for an action and a resource, the user does not have those permissions. 

## Federated Users and Roles

Federated users don't have permanent identities in your AWS account the way that IAM users do. To assign permissions to federated users, you can create an entity referred to as a *role* and define permissions for the role. When a federated user signs in to AWS, the user is associated with the role and is granted the permissions that are defined in the role. For more information, see [Creating a Role for a Third-Party Identity Provider (Federation)](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_create_for-idp.html). 

## Identity-based and Resource-based Policies 

Identity-based policies are permission policies that you can attach to a principal (or identity), such as an IAM user, role, or group. Resource-based policies are JSON policy documents that you attach to a resource such as an Amazon S3 bucket. 

Identity-based policies control what actions that identity can perform, on which resources, and under what conditions. Identity-based policies can be further categorized: 

- **Managed policies** – Standalone identity-based policies that you can attach to multiple users, groups, and roles in your AWS account. You can use two types of managed policies: 
  - **AWS managed policies** – Managed policies that are created and managed by AWS. If you are new to using policies, we recommend that you start by using AWS managed policies. 
  - **Customer managed policies** – Managed policies that you create and manage in your AWS account. Customer managed policies provide more precise control over your policies than AWS managed policies. You can create and edit an IAM policy in the visual editor or by creating the JSON policy document directly. For more information, see [Creating IAM Policies](https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_create.html) and [Editing IAM Policies](https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_manage-edit.html). 
- **Inline policies** – Policies that you create and manage and that are *embedded* directly into a single user, group, or role. 

Resource-based policies control what actions a specified principal can perform on that resource and under what conditions. Resource-based policies are inline policies, and there are no managed resource-based policies. 

Although IAM identities are technically AWS resources, you cannot attach a resource-based policy to an IAM identity. You must use identity-based policies in IAM. To see which services support resource-based policies, see [AWS Services That Work with IAM](https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_aws-services-that-work-with-iam.html). To learn more about resource-based policies, see [Identity-Based Policies and Resource-Based Policies](https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_identity-vs-resource.html). 

*Trust policies* are resource-based policies that are attached to a role that define which principals can assume the role. When you create a role in IAM, the role must have two things: The first is a trust policy that indicates who can assume the role. The second is a permission policy that indicates what they can do with that role. Remember that adding an account to the trust policy of a role is only half of establishing the trust relationship. By default, no users in the trusted accounts can assume the role until the administrator for that account grants the users the permission to assume the role. For more information, see [Granting a User Permissions to Switch Roles](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_use_permissions-to-switch.html). 

Important

Throughout the AWS documentation, when we refer to an IAM policy without mentioning any of the specific categories above, we mean an identity-based, customer managed policy. 

When a user tries to perform an operation in AWS, IAM checks all the policies that exist on the user, its groups, or the affected resource. If there is an identity-based policy and a resource-based policy that apply to the same operation, then both must allow the operation. This means that the identity-based policy must allow the action on the resource. This means that the resource-based policy must also allow the action for the principal making the call. 

For example, in Amazon S3, you can attach a resource-based policy to a bucket. This is called a bucket policy. The following example shows an S3 bucket policy that allows an IAM user named bob in AWS account 777788889999 to put objects into the bucket to which the policy is attached. 

```
{
 "Version": "2012-10-17",
 "Statement": {
 "Effect": "Allow",
 "Principal": {"AWS": "arn:aws:iam::777788889999:user/bob"},
 "Action": [
	"s3:PutObject",
	"s3:PutObjectAcl"
 ]
 }
}
```

Resource-based policies include a `Principal` element that specifies who is granted the permissions. In the preceding example, the `Principal` element is set to the [Amazon Resource Name](http://docs.aws.amazon.com/general/latest/gr/aws-arns-and-namespaces.html) (ARN) of an IAM user named bob in AWS account 777788889999. This indicates that the resource (in this case, the S3 bucket) is accessible to that IAM user but no one else. 

# What Is Amazon S3?

Amazon Simple Storage Service is storage for the Internet. It is designed to make web-scale computing easier for developers. 

Amazon S3 has a simple web services interface that you can use to store and retrieve any amount of data, at any time, from anywhere on the web. It gives any developer access to the same highly scalable, reliable, fast, inexpensive data storage infrastructure that Amazon uses to run its own global network of web sites. The service aims to maximize benefits of scale and to pass those benefits on to developers. 

This  guide explains the core concepts of Amazon S3, such as buckets and objects, and how  to work with these resources using the Amazon S3 application programming interface (API). 

# What Is Amazon EC2?

Amazon Elastic Compute Cloud (Amazon EC2) provides scalable computing capacity in the Amazon Web Services (AWS) cloud. Using Amazon EC2 eliminates your need to invest in hardware up front, so you can develop and deploy applications faster. You can use Amazon EC2 to launch as many or as few virtual servers as you need, configure security and networking, and manage storage. Amazon EC2 enables you to scale up or down to handle changes in requirements or spikes in popularity, reducing your need to forecast traffic. 

For more information about cloud computing, see [What is Cloud Computing?](https://aws.amazon.com/what-is-cloud-computing/)

## Features of Amazon EC2

Amazon EC2 provides the following features:

- Virtual computing environments, known as *instances*
- Preconfigured templates for your instances, known as *Amazon Machine Images (AMIs)*, that package the bits you need for your server (including the operating system and additional software) 
- Various configurations of CPU, memory, storage, and networking capacity for your instances, known as *instance types*
- Secure login information for your instances using *key pairs* (AWS stores the public key, and you store the private key in a secure place) 
- Storage volumes for temporary data that's deleted when you stop or terminate your instance, known as *instance store volumes*
- Persistent storage volumes for your data using Amazon Elastic Block Store (Amazon EBS), known as *Amazon EBS volumes*
- Multiple physical locations for your resources, such as instances and Amazon EBS volumes, known as *regions* and *Availability Zones*
- A firewall that enables you to specify the protocols, ports, and source IP ranges that can reach your instances using *security groups*
- Static IPv4 addresses for dynamic cloud computing, known as *Elastic IP addresses*
- Metadata, known as *tags*, that you can create and assign to your Amazon EC2 resources 
- Virtual networks you can create that are logically isolated from the rest of the AWS cloud, and that you can optionally connect to your own network, known as *virtual private clouds* (VPCs) 

# What Is Amazon Relational Database Service (Amazon RDS)?

Amazon Relational Database Service (Amazon RDS) is a web service that makes it easier to set up, operate, and scale a relational database in the cloud. It provides cost-efficient, resizable capacity for an industry-standard relational database and manages common database administration tasks.  

## Overview of Amazon RDS

Why do you want a managed relational database service? Because Amazon RDS takes over many of the difficult or tedious management tasks of a relational database: 

- When you buy a server, you get CPU, memory, storage, and IOPS, all bundled together. With Amazon RDS, these are split apart so that you can scale them independently. If you need more CPU, less IOPS, or more storage, you can easily allocate them.  
- Amazon RDS manages backups, software patching, automatic failure detection, and recovery.  
- To deliver a managed service experience, Amazon RDS doesn't provide shell access to DB instances, and it restricts access to certain system procedures and tables that require advanced privileges.  
- You can have automated backups performed when you need them, or manually create your own backup snapshot. You can use these backups to restore a database. The Amazon RDS restore process works reliably and efficiently.  
- You can get high availability with a primary instance and a synchronous secondary instance that you can fail over to when problems occur. You can also use MySQL, MariaDB, or PostgreSQL Read Replicas to increase read scaling.  
- You can use the database products you are already familiar with: MySQL, MariaDB, PostgreSQL, Oracle, Microsoft SQL Server, and the new, MySQL-compatible Amazon Aurora DB engine (for information, see [Amazon Aurora on Amazon RDS](https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/CHAP_Aurora.html)).  
- In addition to the security in your database package, you can help control who can access your RDS databases by using AWS Identity and Access Management (IAM) to define users and permissions. You can also help protect your databases by putting them in a virtual private cloud.  

# Create an IAM User

Amazon IAM (Identity and Access Management) enables you to  manage users and user permissions in AWS. You can create one or more IAM  users in your AWS account. You might create an IAM user for someone who  needs access to your AWS console, or when you have a new application  that needs to make API calls to AWS. This is to add an extra layer of  security to your AWS account.

In this chapter, we are going to create a new IAM user for a couple of the AWS related tools we are going to be using later.

### Create User

First, log in to your [AWS Console](https://console.aws.amazon.com) and select IAM from the list of services.

![Select IAM Service Screenshot](assets/select-iam-service.png)

Select **Users**.

![Select IAM Users Screenshot](assets/select-iam-users.png)

Select **Add User**.

![Add IAM User Screenshot](assets/add-iam-user.png)

Enter a **User name** and check **AWS Management Console access** and fill form follow image, then select **Next: Permissions**.

![000093-IAM Management Console](assets/000093-IAM Management Console.png)

Create a **new group** for **new admin** user

![000094-IAM Management Console](assets/000094-IAM Management Console.png)

Click **Create group** 

![000095-IAM Management Console](assets/000095-IAM Management Console.png)

Click **Create User** and result:

![000096-IAM Management Console](assets/000096-IAM Management Console.png)

From Dashboard:

![000098-IAM Management Console](assets/000098-IAM Management Console.png)

IAM users sign-in link:

```
https://623651272467.signin.aws.amazon.com/console
```

with `623651272467` is `Account ID` 

Sign-in 

![000097-Microsoft Edge](assets/000097-Microsoft Edge.png)

If login failed try reset password for user `admin.tma` by click **Manager password** -> **Set password** -> **Custom password** 

![000099-IAM Management Console](assets/000099-IAM Management Console.png)

Try access to IAM Service:

![000100-Microsoft Edge](assets/000100-Microsoft Edge.png)

Try access to S3 Service:

![000103-Microsoft Edge](assets/000103-Microsoft Edge.png)

Try access to EC2 Service:

![000102-Microsoft Edge](assets/000102-Microsoft Edge.png)



## Create another IAM user, create a new group named "access-iam-s3-ec2"

Create new **Group**:

![000104-Microsoft Edge](assets/000104-Microsoft Edge.png)

Attach IAM policy

![000105-Microsoft Edge](assets/000105-Microsoft Edge.png)

Attach S3 Policy

![000107-Microsoft Edge](assets/000107-Microsoft Edge.png)

Click **Next Step**

![000108-Microsoft Edge](assets/000108-Microsoft Edge.png)

Review group:

![000109-Microsoft Edge](assets/000109-Microsoft Edge.png)



![000110-Microsoft Edge](assets/000110-Microsoft Edge.png)

Create **new user**:

![000111-Microsoft Edge](assets/000111-Microsoft Edge.png)

Add **user** to **group**:

![000112-Microsoft Edge](assets/000112-Microsoft Edge.png)

Create User:

![000113-Microsoft Edge](assets/000113-Microsoft Edge.png)

New user:

![000114-Microsoft Edge](assets/000114-Microsoft Edge.png)

Access to new account:

![000115-Microsoft Edge](assets/000115-Microsoft Edge.png)

Access to EC2 service:

![000116-Microsoft Edge](assets/000116-Microsoft Edge.png)

Attach EC2 Full Access for **access-iam-s3-ec2**

![000117-Microsoft Edge](assets/000117-Microsoft Edge.png)

Try access from **user.access_1** again:

![000119-Microsoft Edge](assets/000119-Microsoft Edge.png)

## Research AWS Access key ID, how to create an Ec2 and create one

**To create, disable, or delete an access key for your AWS account root user**

1. Use your AWS account email address and password to sign in to the [AWS Management Console](https://console.aws.amazon.com/) as the AWS account root user. 

   > Note
   >
   > If you previously signed in to the console with *IAM user* credentials, your browser might remember this preference and open your account-specific sign-in page. You cannot use the IAM user sign-in page to sign in with your AWS account root user credentials. If you see the IAM user sign-in page, choose **Sign-in using root user credentials** near the bottom of the page to return to the main sign-in page. From there, you can type your AWS account email address and password. 

2. On the **IAM Dashboard** page, choose your account name in the navigation bar, and then choose **My Security Credentials**.  

   ![000122-IAM Management Console](assets/000122-IAM Management Console.png)

3. If you see a warning about accessing the security credentials for your AWS account, choose **Continue to Security Credentials**. 

   ![000121-IAM Management Console](assets/000121-IAM Management Console.png)

4. Expand the **Access keys (access key ID and secret access key)** section. 

   ![000123-IAM Management Console](assets/000123-IAM Management Console.png)

5. Choose your preferred action:

   - **To create an access key**

      Choose **Create New Access Key**. Then choose **Download Key File** to save the access key ID and secret access key to a file on your computer. After you close the dialog box, you can't retrieve this secret access key again. 

   - **To disable an existing access key**

      Choose **Make Inactive** next to the access key that you are disabling. To reenable an inactive access key, choose **Make Active**. 

   - **To delete an existing access key**

      Before you delete an access key, make sure it's no longer in use. For more information, see [Finding unused access keys](http://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_finding-unused.html#finding-unused-access-keys) in the *IAM User Guide*. You can't recover an access key after deleting it. To delete your access key, choose **Delete** next to the access key that you you want to delete. 

# Create Your EC2 Resources and Launch Your EC2 Instance 

Before you can launch and connect to an Amazon EC2 instance, you need to create a key pair, unless you already have one. You can create a key pair using the Amazon EC2 console and then you can launch your EC2 instance. 

> Note
>
> Using Amazon EFS with Microsoft Windows Amazon EC2 instances is not supported.

**To create a key pair**

- Follow the steps in [Setting Up with Amazon EC2](http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/get-set-up-for-amazon-ec2.html) in the *Amazon EC2 User Guide for Linux Instances* to create a key pair. If you already have a key pair, you do not need to create a new one and you can use your existing key pair for this exercise. 

**To launch the EC2 instance**

1. Open the Amazon EC2 console at <https://console.aws.amazon.com/ec2/>. 

2. Choose **Launch Instance**. 

3. In **Step 1: Choose an Amazon Machine Image (AMI)**, find an Amazon Linux AMI at the top of the list and choose **Select**. 

   ![000124-Microsoft Edge](assets/000124-Microsoft Edge.png)

4. In **Step 2: Choose an Instance Type**, choose **Next: Configure Instance Details**. 

   ![000125-Microsoft Edge](assets/000125-Microsoft Edge.png)

5. In **Step 3: Configure Instance Details**, choose **Network**, and then choose the entry for your default VPC. It should look something like `vpc-xxxxxxx (172.31.0.0/16) (default)`. 

   1. Choose **Subnet**, and then choose a subnet in any Availability Zone. 

   2. Choose **Next: Add Storage**. 

      ![000127-Microsoft Edge](assets/000127-Microsoft Edge.png)

6. Choose **Next: Tag Instance**. 

   ![000128-Microsoft Edge](assets/000128-Microsoft Edge.png)

7. Name your instance and choose **Next: Configure Security Group**. 

   ![000129-Microsoft Edge](assets/000129-Microsoft Edge.png)

8. In **Step 6: Configure Security Group**, review the contents of this page, ensure that **Assign a security group** is set to **Create a new security group**, and verify that the inbound rule being created has the following default values. 

   - **Type:** SSH 

   - **Protocol:** TCP 

   - **Port Range:** 22 

   - **Source:** Anywhere 0.0.0.0/0 

      ![img](assets/gs-review-security-group-600w.png) 

   Note

   You can configure the EFS file system to mount on your EC2 instance automatically. For more information, see [Configuring an EFS File System to Mount Automatically at EC2 Instance Launch](https://docs.aws.amazon.com/efs/latest/ug/mount-fs-auto-mount-onreboot.html#mount-fs-auto-mount-on-creation). 

9. Choose **Review and Launch**. 

10. Choose **Launch**. 

    ![000130-Microsoft Edge](assets/000130-Microsoft Edge.png)

11. Select the check box for the key pair that you created, and then choose **Launch Instances**. 

    ![000131-Microsoft Edge](assets/000131-Microsoft Edge.png)

12. Choose **View Instances**. 

    ![000132-Microsoft Edge](assets/000132-Microsoft Edge.png)

13. Choose the name of the instance you just created from the list, and then choose **Actions**. 

    1. From the menu that opens, choose **Networking** and then choose **Change Security Groups**. 

        ![img](assets/gs-change-security-group-600w.png) 

    2. Select the check box next to the security group with the description **default VPC security group**. 

    3. Choose **Assign Security Groups**. 

        ![img](assets/gs-assign-security-group-600w.png) 

    Note

    In this step, you assign your VPC's default security group to the Amazon EC2 instance. Doing this ensures that the instance is a member of the security group that the Amazon EFS file system mount target authorizes for connection in [Step 2: Create Your Amazon EFS File System](https://docs.aws.amazon.com/efs/latest/ug/gs-step-two-create-efs-resources.html). 

    By using your VPC's default security group, with its default inbound and outbound rules, you are potentially opening up this instance and this file system to potential threats from within your VPC. Make sure that you follow [Step 5: Clean Up Resources and Protect Your AWS Account](https://docs.aws.amazon.com/efs/latest/ug/gs-step-four-cleanup.html) at the end of this Getting Started exercise to remove resources exposed to your VPC's default security group for this example. For more information, see [Security Groups for Amazon EC2 Instances and Mount Targets](https://docs.aws.amazon.com/efs/latest/ug/security-considerations.html#network-access). 

14. Choose your instance from the list.

15. On the **Description** tab, make sure that you have two entries listed next to **security groups**—one for the default VPC security group and one for the security group that you created when you launched the instance. 

16. Make a note of the values listed next to **VPC ID** and **Public DNS**. You need those values later in this exercise. 

# Installing the AWS Command Line Interface

The primary distribution method for the AWS CLI on Linux, Windows, and macOS is `pip`, a package manager for Python that provides an easy way to install, upgrade, and remove Python packages and their dependencies. 

Current AWS CLI Version

The AWS CLI is updated frequently with support for new services and commands. To see if you have the latest version, see the [releases page on GitHub](https://github.com/aws/aws-cli/releases). 

**Requirements**

- Python 2 version 2.6.5+ or Python 3 version 3.3+
- Windows, Linux, macOS, or Unix

Note

Older versions of Python may not work with all AWS services. If you see `InsecurePlatformWarning` or deprecation notices when you install or use the AWS CLI, update to a recent version. 

If you already have `pip` and a supported version of Python, you can install the AWS CLI with the following command: 

```
$ pip install awscli --upgrade --user
```

The `--upgrade` option tells `pip` to upgrade any requirements that are already installed. The `--user` option tells `pip` to install the program to a subdirectory of your user directory to avoid modifying libraries used by your operating system. 

If you encounter issues when you attempt to install the AWS CLI with `pip`, you can [install the AWS CLI in a virtual environment](https://docs.aws.amazon.com/cli/latest/userguide/awscli-install-virtualenv.html) to isolate the tool and its dependencies, or use a different version of Python than you normally do. 

Standalone Installers

For offline or automated installations on Linux, macOS, or Unix, try the [bundled installer](https://docs.aws.amazon.com/cli/latest/userguide/awscli-install-bundle.html). The bundled installer includes the AWS CLI, its dependencies, and a shell script that performs the installation for you. 

On Windows, you can also use the [MSI installer](https://docs.aws.amazon.com/cli/latest/userguide/awscli-install-windows.html#install-msi-on-windows). Both of these methods simplify the initial installation, with the tradeoff of being more difficult to upgrade when a new version of the AWS CLI is released. 

After you install the AWS CLI, you may need to add the path to the executable file to your PATH variable. For platform specific instructions, see the following topics: 

- **Linux** – [Adding the AWS CLI Executable to your Command Line Path](https://docs.aws.amazon.com/cli/latest/userguide/awscli-install-linux.html#awscli-install-linux-path)
- **Windows** – [Adding the AWS CLI Executable to your Command Line Path](https://docs.aws.amazon.com/cli/latest/userguide/awscli-install-windows.html#awscli-install-windows-path)
- **macOS** – [Adding the AWS CLI Executable to your Command Line Path](https://docs.aws.amazon.com/cli/latest/userguide/cli-install-macos.html#awscli-install-osx-path)

Verify that the AWS CLI installed correctly by running `aws --version`. 

```
$ aws --version
aws-cli/1.11.84 Python/3.6.2 Linux/4.4.0-59-generic botocore/1.5.47
```

The AWS CLI is updated regularly to add support for new services and commands. To update to the latest version of the AWS CLI, run the installation command again. 

```
$ pip install awscli --upgrade --user
```

If you need to uninstall the AWS CLI, use `pip uninstall`. 

```
$ pip uninstall awscli
```

# Access the ec2 and execute command: "aws s3 ls". And fix error using Access Key ID.

In **EC2 Dashboard** -> **Actions** -> **Connect**

![000133-Microsoft Edge](assets/000133-Microsoft Edge.png)

![000137-Microsoft Edge](assets/000137-Microsoft Edge.png)

Try connect 

![000136-Cmder](assets/000136-Cmder.png)

execute command: `aws s3 ls`

![000138-Cmder](assets/000138-Cmder.png)

fix error using Access Key ID

![000139-Cmder](assets/000139-Cmder.png)

# IAM Role

Use the above ec2. Create a text file named Hello.txt with content "Hello" by execute this command:

```
echo "Hello" > Hello.txt
```

![000140-Cmder](assets/000140-Cmder.png)

Remove folder ~/.aws:

![000141-Cmder](assets/000141-Cmder.png)

Execute command: "aws s3 ls"

![000142-Cmder](assets/000142-Cmder.png)

Create an IAM Role that can be used by a EC2, and add policy **AmazonEC2FullAccess** and **AmazonS3FullAccess**

![000143-IAM Management Console and 1 more page InPrivate Microsoft Edge](assets/000143-IAM Management Console and 1 more page InPrivate Microsoft Edge.png)

Create role, Select **EC2** and click **Next**



![000144-Microsoft Edge](assets/000144-Microsoft Edge.png)

Attach EC2 

![000145-Microsoft Edge](assets/000145-Microsoft Edge.png)

Attach S3

![000146-Microsoft Edge](assets/000146-Microsoft Edge.png)

Review role

![000147-Microsoft Edge](assets/000147-Microsoft Edge.png)

Done,

![000148-Microsoft Edge](assets/000148-Microsoft Edge.png)

Attach this role to the above Ec2

1. Open the Amazon EC2 console at https://console.aws.amazon.com/ec2/.

2. In the navigation pane, choose Instances.

3. Select the instance, choose Actions, Attach/Replace IAM role.

   ![000149-Microsoft Edge](assets/000149-Microsoft Edge.png)

4. Select the IAM role to attach to your instance, and choose Apply.

![000150-Microsoft Edge](assets/000150-Microsoft Edge.png)

Success,

![000151-Microsoft Edge](assets/000151-Microsoft Edge.png)

Try `aws s3 ls` again

![000152-Cmder](assets/000152-Cmder.png)

Create a bucket named "lphieu-exercise-004" on Region Asia Pacific (Singapore) (code: ap-southeast-1)

```bash
aws s3api create-bucket --bucket lphieu-exercise-004 --region ap-southeast-1 --create-bu cket-configuration LocationConstraint=ap-southeast-1
```

Then upload the file Hello.txt to the bucket "lphieu-exercise-004"

```
aws s3 cp Hello.txt s3://lphieu-exercise-004/
```

Check it by using AWS Management Console. Then download the file via browser and download it via shared link

![000155-Cmder](assets/000155-Cmder.png)

![000156-Microsoft Edge](assets/000156-Microsoft Edge.png)

I was “Make Public” file Hello.txt. Link: https://s3-ap-southeast-1.amazonaws.com/lphieu-exercise-004/Hello.txt

# Regional product services

## Americas

| **Services Offered:**                                        | **Northern Virginia** | **Ohio** | **Oregon** | **Northern California** | **Montreal** | **São Paulo** | **GovCloud** |
| ------------------------------------------------------------ | --------------------- | -------- | ---------- | ----------------------- | ------------ | ------------- | ------------ |
| [Alexa for Business](https://aws.amazon.com/alexaforbusiness/) | ✓                     |          |            |                         |              |               |              |
| [Amazon API Gateway](https://aws.amazon.com/api-gateway/)    | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             | ✓            |
| [Amazon AppStream 2.0](https://aws.amazon.com/appstream2/)   | ✓                     |          | ✓          |                         |              |               |              |
| [Amazon Athena](https://aws.amazon.com/athena/)              | ✓                     | ✓        | ✓          |                         |              |               |              |
| [Amazon Aurora - MySQL-compatible](https://aws.amazon.com/rds/aurora/) | ✓                     | ✓        | ✓          | ✓                       | ✓            |               | ✓            |
| [Amazon Aurora - PostgreSQL-compatible](https://aws.amazon.com/rds/aurora/) | ✓                     | ✓        | ✓          |                         | ✓            |               |              |
| [Amazon Chime](https://chime.aws/)                           | ✓                     |          |            |                         |              |               |              |
| [Amazon Cloud Directory](https://aws.amazon.com/cloud-directory/) | ✓                     | ✓        | ✓          |                         |              |               |              |
| [Amazon CloudSearch](https://aws.amazon.com/cloudsearch/)    | ✓                     |          | ✓          | ✓                       |              | ✓             |              |
| [Amazon CloudWatch](https://aws.amazon.com/cloudwatch/)      | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             | ✓            |
| [Amazon CloudWatch Events](https://aws.amazon.com/cloudwatch/) | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             | ✓            |
| [Amazon CloudWatch Logs](https://docs.aws.amazon.com/AmazonCloudWatch/latest/DeveloperGuide/CWL_GettingStarted.html) | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             | ✓            |
| [Amazon Cognito](https://aws.amazon.com/cognito/)            | ✓                     | ✓        | ✓          |                         |              |               |              |
| [Amazon Comprehend](https://aws.amazon.com/comprehend/)      | ✓                     | ✓        | ✓          |                         |              |               |              |
| [Amazon Connect](https://aws.amazon.com/connect/)            | ✓                     |          | ✓          |                         |              |               |              |
| [Amazon DeepLens](https://aws.amazon.com/deeplens/)          | ✓                     | ✓        | ✓          | ✓                       |              |               |              |
| [Amazon DynamoDB](https://aws.amazon.com/dynamodb/)          | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             | ✓            |
| [Amazon DynamoDB Accelerator (DAX)](https://aws.amazon.com/dynamodb/dax/) | ✓                     | ✓        | ✓          | ✓                       |              | ✓             |              |
| [Amazon Elastic Container Registry (ECR)](https://aws.amazon.com/ecr/) | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             | ✓            |
| [Amazon Elastic Container Service (ECS)](https://aws.amazon.com/ecs/) | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             | ✓            |
| [Amazon Elastic Container Service for Kubernetes (EKS)](https://aws.amazon.com/eks) | ✓                     |          | ✓          |                         |              |               |              |
| [Amazon ElastiCache](https://aws.amazon.com/elasticache/)    | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             | ✓            |
| [Amazon Elastic Block Store (EBS)](https://aws.amazon.com/ebs/) | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             | ✓            |
| [Amazon Elastic Compute Cloud (EC2)](https://aws.amazon.com/ec2/) | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             | ✓            |
| [Amazon EC2 Auto Scaling ](https://aws.amazon.com/ec2/autoscaling/) | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             | ✓            |
| [AWS Auto Scaling](https://aws.amazon.com/autoscaling/)      | ✓                     | ✓        | ✓          |                         |              |               |              |
| [Amazon Elastic File System (EFS)](https://aws.amazon.com/efs/) | ✓                     | ✓        | ✓          | ✓                       |              |               |              |
| [Amazon Elastic MapReduce](https://aws.amazon.com/emr/)      | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             | ✓            |
| [Amazon Elasticsearch Service](https://aws.amazon.com/elasticsearch-service/) | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             | ✓            |
| [Amazon Elastic Transcoder](https://aws.amazon.com/elastictranscoder/) | ✓                     |          | ✓          | ✓                       |              |               |              |
| [Amazon FreeRTOS](https://aws.amazon.com/freertos/)          | ✓                     | ✓        | ✓          |                         |              |               |              |
| [Amazon GameLift](https://aws.amazon.com/gamelift/)          | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             |              |
| [Amazon Glacier](https://aws.amazon.com/glacier1/)           | ✓                     | ✓        | ✓          | ✓                       | ✓            |               | ✓            |
| [Amazon GuardDuty](https://aws.amazon.com/guardduty/)        | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             |              |
| [Amazon Inspector](https://aws.amazon.com/inspector/)        | ✓                     | ✓        | ✓          | ✓                       |              |               |              |
| [Amazon Kinesis Data Analytics](https://aws.amazon.com/kinesis/analytics/) | ✓                     |          | ✓          |                         |              |               |              |
| [Amazon Kinesis Data Firehose](https://aws.amazon.com/kinesis/firehose/) | ✓                     | ✓        | ✓          | ✓                       |              |               |              |
| [Amazon Kinesis Data Streams  ](https://aws.amazon.com/kinesis/streams/) | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             | ✓            |
| [Amazon Kinesis Video Streams  ](https://aws.amazon.com/kinesis/video-streams/) | ✓                     |          | ✓          |                         |              |               |              |
| [Amazon Lex  ](https://aws.amazon.com/lex/)                  | ✓                     |          | ✓          |                         |              |               |              |
| [Amazon Lightsail](https://amazonlightsail.com/)             | ✓                     | ✓        | ✓          |                         | ✓            |               |              |
| [Amazon Machine Learning](https://aws.amazon.com/machine-learning/) | ✓                     |          |            |                         |              |               |              |
| [Amazon Macie](https://aws.amazon.com/macie/)                | ✓                     |          | ✓          |                         |              |               |              |
| [Amazon Mobile Analytics](https://aws.amazon.com/mobileanalytics/) | ✓                     |          |            |                         |              |               |              |
| [Amazon MQ](https://aws.amazon.com/amazon-mq/)               | ✓                     | ✓        | ✓          |                         |              |               |              |
| [Amazon Neptune](https://aws.amazon.com/neptune/)            | ✓                     | ✓        | ✓          |                         |              |               |              |
| [Amazon Pinpoint](https://aws.amazon.com/pinpoint/)          | ✓                     |          |            |                         |              |               |              |
| [Amazon Polly](https://aws.amazon.com/polly/)                | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             | ✓            |
| [Amazon QuickSight](https://quicksight.aws/)                 | ✓                     | ✓        | ✓          |                         |              |               |              |
| [Amazon Redshift](https://aws.amazon.com/redshift/)          | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             | ✓            |
| [Amazon Rekognition Image](https://aws.amazon.com/rekognition/) | ✓                     | ✓        | ✓          |                         |              |               | ✓            |
| [Amazon Rekognition Video](https://aws.amazon.com/rekognition/video-features/) | ✓                     | ✓        | ✓          |                         |              |               |              |
| [Amazon Relational Database Service (RDS)](https://aws.amazon.com/rds/) | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             | ✓            |
| [Amazon Route 53 Auto Naming  ](https://aws.amazon.com/route53/) | ✓                     | ✓        | ✓          |                         |              |               |              |
| [Amazon Sagemaker](https://aws.amazon.com/sagemaker/)        | ✓                     | ✓        | ✓          |                         |              |               |              |
| [Amazon SimpleDB](https://aws.amazon.com/simpledb/)          | ✓                     |          | ✓          | ✓                       |              | ✓             |              |
| [Amazon Simple Email Service (SES)](https://aws.amazon.com/ses/) | ✓                     |          | ✓          |                         |              |               |              |
| [Amazon Simple Notification Service (SNS)](https://aws.amazon.com/sns/) | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             | ✓            |
| [Amazon Simple Queue Service (SQS)](https://aws.amazon.com/sqs/) | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             | ✓            |
| [Amazon Simple Storage Service (S3)](https://aws.amazon.com/s3/) | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             | ✓            |
| [Amazon Simple Workflow Service (SWF)](https://aws.amazon.com/swf/) | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             | ✓            |
| [Amazon Sumerian](https://aws.amazon.com/sumerian/)          | ✓                     | ✓        | ✓          | ✓                       |              | ✓             |              |
| [Amazon Transcribe](https://aws.amazon.com/transcribe/)      | ✓                     | ✓        | ✓          |                         |              |               |              |
| [Amazon Translate](https://aws.amazon.com/neptune/)          | ✓                     | ✓        | ✓          |                         |              |               |              |
| [Amazon Virtual Private Cloud (VPC)](https://aws.amazon.com/vpc/) | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             | ✓            |
| [Amazon WorkDocs](https://aws.amazon.com/workdocs/)          | ✓                     |          | ✓          |                         |              |               |              |
| [Amazon WorkMail](https://aws.amazon.com/workmail/)          | ✓                     |          | ✓          |                         |              |               |              |
| [Amazon WorkSpaces](https://aws.amazon.com/workspaces/)      | ✓                     |          | ✓          |                         | ✓            | ✓             |              |
| [Amazon WorkSpaces Application Manager](https://aws.amazon.com/workspaces/applicationmanager/) | ✓                     |          | ✓          |                         |              |               |              |
| [AWS Application Discovery Service](https://aws.amazon.com/application-discovery/) |                       |          | ✓          |                         |              |               |              |
| [AWS AppSync](https://aws.amazon.com/appsync/)               | ✓                     | ✓        | ✓          |                         |              |               |              |
| [AWS Batch](https://aws.amazon.com/batch/)                   | ✓                     | ✓        | ✓          | ✓                       | ✓            |               |              |
| [AWS Certificate Manager](https://aws.amazon.com/certificate-manager/) | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             | ✓            |
| [AWS CloudFormation](https://aws.amazon.com/cloudformation/) | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             | ✓            |
| [AWS Cloud9](https://aws.amazon.com/cloud9/)                 | ✓                     | ✓        | ✓          |                         |              |               |              |
| [AWS CloudHSM](https://aws.amazon.com/cloudhsm/)             | ✓                     | ✓        | ✓          | ✓                       | ✓            |               | ✓            |
| [AWS CloudHSM Classic](https://aws.amazon.com/cloudhsm/)     | ✓                     | ✓        | ✓          | ✓                       | ✓            |               | ✓            |
| [AWS CloudTrail](https://aws.amazon.com/cloudtrail/)         | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             | ✓            |
| [AWS CodeBuild](https://aws.amazon.com/codebuild/)           | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             |              |
| [AWS CodeCommit](https://aws.amazon.com/codecommit/)         | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             |              |
| [AWS CodeDeploy](https://aws.amazon.com/codedeploy/)         | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             | ✓            |
| [AWS CodePipeline](https://aws.amazon.com/codepipeline/)     | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             |              |
| [AWS CodeStar](https://aws.amazon.com/codestar/)             | ✓                     | ✓        | ✓          | ✓                       | ✓            |               |              |
| [AWS Config](https://aws.amazon.com/config/)                 | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             | ✓            |
| [AWS Database Migration Service](https://aws.amazon.com/dms/) | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             | ✓            |
| [AWS Data Pipeline](https://aws.amazon.com/datapipeline/)    | ✓                     |          | ✓          |                         |              |               |              |
| [AWS Device Farm](https://aws.amazon.com/device-farm/)       |                       |          | ✓          |                         |              |               |              |
| [AWS Direct Connect](https://aws.amazon.com/directconnect/)  | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             | ✓            |
| [AWS Directory Service](https://aws.amazon.com/directoryservice/) | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             |              |
| [AWS Elastic Beanstalk](https://aws.amazon.com/elasticbeanstalk/) | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             | ✓            |
| [AWS Elemental MediaConvert](https://aws.amazon.com/mediaconvert/) | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             |              |
| [AWS Elemental MediaLive](https://aws.amazon.com/medialive/) | ✓                     |          | ✓          |                         |              |               |              |
| [AWS Elemental MediaPackage](https://aws.amazon.com/mediapackage/) | ✓                     |          | ✓          |                         |              | ✓             |              |
| [AWS Elemental MediaStore](https://aws.amazon.com/mediastore/) | ✓                     |          | ✓          |                         |              |               |              |
| [AWS Elemental MediaTailor](https://aws.amazon.com/mediatailor/) | ✓                     |          |            |                         |              |               |              |
| [AWS Fargate](https://aws.amazon.com/fargate/)               | ✓                     | ✓        | ✓          |                         |              |               |              |
| [AWS Firewall Manager](https://aws.amazon.com/firewall-manager/) | ✓                     |          | ✓          |                         |              |               |              |
| [AWS Glue](https://aws.amazon.com/glue/)                     | ✓                     | ✓        | ✓          |                         |              |               |              |
| [AWS Greengrass](https://aws.amazon.com/greengrass/)         | ✓                     |          | ✓          |                         |              |               |              |
| [AWS IoT Core](https://aws.amazon.com/iot/)                  | ✓                     | ✓        | ✓          |                         |              |               |              |
| [AWS IoT 1-Click](https://aws.amazon.com/ot-1-click/)        | ✓                     | ✓        | ✓          |                         |              |               |              |
| [AWS IoT Analytics](https://aws.amazon.com/iot-analytics/)   | ✓                     | ✓        | ✓          |                         |              |               |              |
| [AWS IoT Device Management](https://aws.amazon.com/iot/)     | ✓                     | ✓        | ✓          |                         |              |               |              |
| [AWS Key Management Service](https://aws.amazon.com/kms/)    | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             | ✓            |
| [AWS Lambda](https://aws.amazon.com/lambda/)                 | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             | ✓            |
| [AWS Managed Services](https://aws.amazon.com/managed-services/) | ✓                     |          | ✓          |                         |              |               |              |
| [AWS Marketplace](https://aws.amazon.com/mp/)                | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             | ✓            |
| [AWS Migration Hub](https://aws.amazon.com/migration-hub/)   |                       |          | ✓          |                         |              |               |              |
| [AWS Mobile Hub](https://aws.amazon.com/mobile/)             | ✓                     | ✓        | ✓          | ✓                       |              | ✓             |              |
| [AWS OpsWorks Stacks](https://aws.amazon.com/opsworks/stacks/) | ✓                     | ✓        | ✓          | ✓                       |              | ✓             |              |
| [AWS OpsWorks for Chef Automate](https://aws.amazon.com/opsworks/chefautomate/) | ✓                     | ✓        | ✓          | ✓                       |              |               |              |
| [AWS OpsWorks for Puppet Enterprise](https://aws.amazon.com/opsworks/puppetenterprise/) | ✓                     | ✓        | ✓          | ✓                       |              |               |              |
| [AWS Personal Health Dashboard](https://aws.amazon.com/premiumsupport/phd/) | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             |              |
| [AWS Secrets Manager](https://aws.amazon.com/secrets-manager/) | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             |              |
| [AWS Server Migration Service](https://aws.amazon.com/server-migration-service/) | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             | ✓            |
| [AWS Service Catalog](https://aws.amazon.com/servicecatalog/) | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             |              |
| [AWS Shield Standard](https://aws.amazon.com/shield/)        | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             | ✓            |
| [AWS Shield Advanced](https://aws.amazon.com/shield/)        | ✓                     | ✓        | ✓          | ✓                       |              |               |              |
| [AWS Single Sign-On](https://aws.amazon.com/single-sign-on/) | ✓                     |          |            |                         |              |               |              |
| [AWS Snowball](https://aws.amazon.com/snowball/)             | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             | ✓            |
| [AWS Snowball Edge](https://aws.amazon.com/snowball-edge/)   | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             | ✓            |
| [AWS Snowmobile](https://aws.amazon.com/snowmobile/)         | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             | ✓            |
| [AWS Step Functions](https://aws.amazon.com/step-functions/details/) | ✓                     | ✓        | ✓          | ✓                       | ✓            |               |              |
| [AWS Storage Gateway](https://aws.amazon.com/storagegateway/) | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             | ✓            |
| [AWS Support](https://aws.amazon.com/premiumsupport/)        | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             | ✓            |
| [AWS Systems Manager](https://aws.amazon.com/systems-manager/) | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             |              |
| [AWS Trusted Advisor](https://aws.amazon.com/premiumsupport/trustedadvisor/) | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             | ✓            |
| [AWS WAF](https://aws.amazon.com/waf/)                       | ✓                     | ✓        | ✓          | ✓                       |              |               |              |
| [AWS X-Ray](https://aws.amazon.com/xray/)                    | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             |              |
| [Elastic Load Balancing](https://aws.amazon.com/elasticloadbalancing/) | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             | ✓            |
| [VM Import/Export](https://aws.amazon.com/ec2/vm-import/)    | ✓                     | ✓        | ✓          | ✓                       | ✓            | ✓             | ✓            |

## Europe / Middle East / Africa

| **Services Offered:**                                        | **Ireland** | **Frankfurt** | **London** | **Paris** |
| ------------------------------------------------------------ | ----------- | ------------- | ---------- | --------- |
| [Alexa for Business](https://aws.amazon.com/alexaforbusiness/) |             |               |            |           |
| [Amazon API Gateway](https://aws.amazon.com/api-gateway/)    | ✓           | ✓             | ✓          | ✓         |
| [Amazon AppStream 2.0](https://aws.amazon.com/appstream2/)   | ✓           | ✓             |            |           |
| [Amazon Athena](https://aws.amazon.com/athena/)              | ✓           | ✓             | ✓          |           |
| [Amazon Aurora - MySQL-compatible](https://aws.amazon.com/rds/aurora/) | ✓           | ✓             | ✓          | ✓         |
| [Amazon Aurora - PostgreSQL-compatible](https://aws.amazon.com/rds/aurora/) | ✓           | ✓             | ✓          | ✓         |
| [Amazon Chime](https://chime.aws/)                           |             |               |            |           |
| [Amazon Cloud Directory](https://aws.amazon.com/cloud-directory/) | ✓           |               | ✓          | ✓         |
| [Amazon CloudSearch](https://aws.amazon.com/cloudsearch/)    | ✓           | ✓             |            |           |
| [Amazon CloudWatch](https://aws.amazon.com/cloudwatch/)      | ✓           | ✓             | ✓          | ✓         |
| [Amazon CloudWatch Events](https://aws.amazon.com/cloudwatch/) | ✓           | ✓             | ✓          | ✓         |
| [Amazon CloudWatch Logs](https://docs.aws.amazon.com/AmazonCloudWatch/latest/DeveloperGuide/CWL_GettingStarted.html) | ✓           | ✓             | ✓          | ✓         |
| [Amazon Cognito](https://aws.amazon.com/cognito/)            | ✓           | ✓             | ✓          |           |
| [Amazon Comprehend](https://aws.amazon.com/comprehend/)      | ✓           |               |            |           |
| [Amazon Connect](https://aws.amazon.com/connect/)            |             | ✓             |            |           |
| [Amazon DeepLens](https://aws.amazon.com/connect/)           | ✓           | ✓             | ✓          |           |
| [Amazon DynamoDB](https://aws.amazon.com/dynamodb/)          | ✓           | ✓             | ✓          | ✓         |
| [Amazon DynamoDB Accelerator (DAX)](https://aws.amazon.com/dynamodb/dax/) | ✓           |               |            |           |
| [Amazon Elastic Container Registry (ECR)](https://aws.amazon.com/ecr/) | ✓           | ✓             | ✓          | ✓         |
| [Amazon Elastic Container Service (ECS)](https://aws.amazon.com/ecs/) | ✓           | ✓             | ✓          | ✓         |
| [Amazon Elastic Container Service for Kubernetes (EKS)](https://aws.amazon.com/eks) |             |               |            |           |
| [Amazon ElastiCache](https://aws.amazon.com/elasticache/)    | ✓           | ✓             | ✓          | ✓         |
| [Amazon Elastic Block Store (EBS)](https://aws.amazon.com/ebs/) | ✓           | ✓             | ✓          | ✓         |
| [Amazon Elastic Compute Cloud (EC2)](https://aws.amazon.com/ec2/) | ✓           | ✓             | ✓          | ✓         |
| [Amazon EC2 Auto Scaling ](https://aws.amazon.com/ec2/autoscaling/) | ✓           | ✓             | ✓          | ✓         |
| [AWS Auto Scaling](https://aws.amazon.com/autoscaling/)      | ✓           |               |            |           |
| [Amazon Elastic File System (EFS)](https://aws.amazon.com/efs/) | ✓           | ✓             |            |           |
| [Amazon Elastic MapReduce](https://aws.amazon.com/emr/)      | ✓           | ✓             | ✓          | ✓         |
| [Amazon Elasticsearch Service](https://aws.amazon.com/elasticsearch-service/) | ✓           | ✓             | ✓          | ✓         |
| [Amazon Elastic Transcoder](https://aws.amazon.com/elastictranscoder/) | ✓           |               |            |           |
| [Amazon FreeRTOS](https://aws.amazon.com/freertos/)          |             |               |            |           |
| [Amazon GameLift](https://aws.amazon.com/gamelift/)          | ✓           | ✓             | ✓          |           |
| [Amazon Glacier](https://aws.amazon.com/glacier1/)           | ✓           | ✓             | ✓          | ✓         |
| [Amazon GuardDuty](https://aws.amazon.com/guardduty/)        | ✓           | ✓             | ✓          | ✓         |
| [Amazon Inspector](https://aws.amazon.com/inspector/)        | ✓           | ✓             |            |           |
| [Amazon Kinesis Data Analytics](https://aws.amazon.com/kinesis/analytics/) | ✓           |               |            |           |
| [Amazon Kinesis Data Firehose](https://aws.amazon.com/kinesis/firehose/) | ✓           | ✓             |            |           |
| [Amazon Kinesis Data Streams  ](https://aws.amazon.com/kinesis/streams/) | ✓           | ✓             | ✓          | ✓         |
| [Amazon Kinesis Video Streams  ](https://aws.amazon.com/kinesis/video-streams/) | ✓           | ✓             |            |           |
| [Amazon Lex  ](https://aws.amazon.com/lex/)                  | ✓           |               |            |           |
| [Amazon Lightsail](https://amazonlightsail.com/)             | ✓           | ✓             | ✓          | ✓         |
| [Amazon Machine Learning](https://aws.amazon.com/machine-learning/) | ✓           |               |            |           |
| [Amazon Macie](https://aws.amazon.com/macie/)                |             |               |            |           |
| [Amazon Mobile Analytics](https://aws.amazon.com/mobileanalytics/) |             |               |            |           |
| [Amazon MQ](https://aws.amazon.com/amazon-mq/)               | ✓           | ✓             |            |           |
| [Amazon Neptune](https://aws.amazon.com/neptune/)            | ✓           |               |            |           |
| [Amazon Pinpoint](https://aws.amazon.com/pinpoint/)          |             |               |            |           |
| [Amazon Polly](https://aws.amazon.com/polly/)                | ✓           | ✓             | ✓          | ✓         |
| [Amazon QuickSight](https://quicksight.aws/)                 | ✓           |               |            |           |
| [Amazon Redshift](https://aws.amazon.com/redshift/)          | ✓           | ✓             | ✓          | ✓         |
| [Amazon Rekognition Image](https://aws.amazon.com/rekognition/) | ✓           |               |            |           |
| [Amazon Rekognition Video](https://aws.amazon.com/rekognition/video-features/) | ✓           |               |            |           |
| [Amazon Relational Database Service (RDS)](https://aws.amazon.com/rds/) | ✓           | ✓             | ✓          | ✓         |
| [Amazon Route 53 Auto Naming  ](https://aws.amazon.com/route53/) | ✓           |               |            |           |
| [Amazon Sagemaker](https://aws.amazon.com/sagemaker/)        | ✓           |               |            |           |
| [Amazon SimpleDB](https://aws.amazon.com/simpledb/)          | ✓           |               |            |           |
| [Amazon Simple Email Service (SES)](https://aws.amazon.com/ses/) | ✓           |               |            |           |
| [Amazon Simple Notification Service (SNS)](https://aws.amazon.com/sns/) | ✓           | ✓             | ✓          | ✓         |
| [Amazon Simple Queue Service (SQS)](https://aws.amazon.com/sqs/) | ✓           | ✓             | ✓          | ✓         |
| [Amazon Simple Storage Service (S3)](https://aws.amazon.com/s3/) | ✓           | ✓             | ✓          | ✓         |
| [Amazon Simple Workflow Service (SWF)](https://aws.amazon.com/swf/) | ✓           | ✓             | ✓          | ✓         |
| [Amazon Sumerian](https://aws.amazon.com/sumerian/)          | ✓           | ✓             | ✓          |           |
| [Amazon Transcribe](https://aws.amazon.com/transcribe/)      | ✓           |               |            |           |
| [Amazon Translate](https://aws.amazon.com/translate/)        | ✓           |               |            |           |
| [Amazon Virtual Private Cloud (VPC)](https://aws.amazon.com/vpc/) | ✓           | ✓             | ✓          | ✓         |
| [Amazon WorkDocs](https://aws.amazon.com/workdocs/)          | ✓           |               |            |           |
| [Amazon WorkMail](https://aws.amazon.com/workmail/)          | ✓           |               |            |           |
| [Amazon WorkSpaces](https://aws.amazon.com/workspaces/)      | ✓           | ✓             | ✓          |           |
| [Amazon WorkSpaces Application Manager](https://aws.amazon.com/workspaces/applicationmanager/) | ✓           |               |            |           |
| [AWS Application Discovery Service](https://aws.amazon.com/application-discovery/) |             |               |            |           |
| [AWS AppSync](https://aws.amazon.com/appsync/)               | ✓           | ✓             |            |           |
| [AWS Batch](https://aws.amazon.com/batch/)                   | ✓           | ✓             | ✓          |           |
| [AWS Certificate Manager](https://aws.amazon.com/certificate-manager/) | ✓           | ✓             | ✓          | ✓         |
| [AWS Cloud9](https://aws.amazon.com/cloud9/)                 | ✓           |               |            |           |
| [AWS CloudFormation](https://aws.amazon.com/cloudformation/) | ✓           | ✓             | ✓          | ✓         |
| [AWS CloudHSM ](https://aws.amazon.com/cloudhsm/)            | ✓           | ✓             |            |           |
| [AWS CloudHSM Classic](https://aws.amazon.com/cloudhsm/)     | ✓           | ✓             |            |           |
| [AWS CloudTrail](https://aws.amazon.com/cloudtrail/)         | ✓           | ✓             | ✓          | ✓         |
| [AWS CodeBuild](https://aws.amazon.com/codebuild/)           | ✓           | ✓             | ✓          | ✓         |
| [AWS CodeCommit](https://aws.amazon.com/codecommit/)         | ✓           | ✓             | ✓          | ✓         |
| [AWS CodeDeploy](https://aws.amazon.com/codedeploy/)         | ✓           | ✓             | ✓          | ✓         |
| [AWS CodePipeline](https://aws.amazon.com/codepipeline/)     | ✓           | ✓             | ✓          | ✓         |
| [AWS CodeStar](https://aws.amazon.com/codestar/)             | ✓           | ✓             | ✓          |           |
| [AWS Config](https://aws.amazon.com/config/)                 | ✓           | ✓             | ✓          | ✓         |
| [AWS Database Migration Service](https://aws.amazon.com/dms/) | ✓           | ✓             | ✓          | ✓         |
| [AWS Data Pipeline](https://aws.amazon.com/datapipeline/)    | ✓           |               |            |           |
| [AWS Device Farm](https://aws.amazon.com/device-farm/)       |             |               |            |           |
| [AWS Direct Connect](https://aws.amazon.com/directconnect/)  | ✓           | ✓             | ✓          | ✓         |
| [AWS Directory Service](https://aws.amazon.com/directoryservice/) | ✓           | ✓             | ✓          |           |
| [AWS Elastic Beanstalk](https://aws.amazon.com/elasticbeanstalk/) | ✓           | ✓             | ✓          | ✓         |
| [AWS Elemental MediaConvert](https://aws.amazon.com/mediaconvert/) | ✓           | ✓             | ✓          |           |
| [AWS Elemental MediaLive](https://aws.amazon.com/medialive/) | ✓           |               |            |           |
| [AWS Elemental MediaPackage](https://aws.amazon.com/mediapackage/) | ✓           | ✓             |            | ✓         |
| [AWS Elemental MediaStore](https://aws.amazon.com/mediastore/) | ✓           | ✓             |            |           |
| [AWS Elemental MediaTailor](https://aws.amazon.com/mediatailor/) | ✓           |               |            |           |
| [AWS Fargate](https://aws.amazon.com/fargate/)               | ✓           |               |            |           |
| [AWS Glue](https://aws.amazon.com/glue/)                     | ✓           | ✓             | ✓          |           |
| [AWS Greengrass](https://aws.amazon.com/greengrass/)         |             | ✓             |            |           |
| [AWS IoT Core](https://aws.amazon.com/iot/)                  | ✓           | ✓             | ✓          |           |
| [AWS 1-Click](https://aws.amazon.com/iot-1-click/)           | ✓           | ✓             | ✓          |           |
| [AWS IoT Analytics](https://aws.amazon.com/iot-analytics/)   | ✓           |               |            |           |
| [AWS IoT Device Management](https://aws.amazon.com/iot/)     | ✓           | ✓             | ✓          |           |
| [AWS Key Management Service](https://aws.amazon.com/kms/)    | ✓           | ✓             | ✓          | ✓         |
| [AWS Lambda](https://aws.amazon.com/lambda/)                 | ✓           | ✓             | ✓          | ✓         |
| [AWS Managed Services](https://aws.amazon.com/managed-services/) | ✓           |               | ✓          |           |
| [AWS Marketplace](https://aws.amazon.com/mp/)                | ✓           | ✓             | ✓          | ✓         |
| [AWS Migration Hub](https://aws.amazon.com/migration-hub/)   |             |               |            |           |
| [AWS Mobile Hub](https://aws.amazon.com/mobile/)             | ✓           | ✓             | ✓          |           |
| [AWS OpsWorks Stacks](https://aws.amazon.com/opsworks/stacks/) | ✓           | ✓             | ✓          | ✓         |
| [AWS OpsWorks for Chef Automate](https://aws.amazon.com/opsworks/chefautomate/) | ✓           | ✓             |            |           |
| [AWS OpsWorks for Puppet Enterprise](https://aws.amazon.com/opsworks/puppetenterprise/) | ✓           | ✓             |            |           |
| [AWS Personal Health Dashboard](https://aws.amazon.com/premiumsupport/phd/) | ✓           | ✓             | ✓          | ✓         |
| [AWS Secrets Manager  ](https://aws.amazon.com/secrets-manager/) | ✓           | ✓             | ✓          |           |
| [AWS Server Migration Service](https://aws.amazon.com/server-migration-service/) | ✓           | ✓             | ✓          | ✓         |
| [AWS Service Catalog](https://aws.amazon.com/servicecatalog/) | ✓           | ✓             | ✓          | ✓         |
| [AWS Shield Standard](https://aws.amazon.com/shield/)        | ✓           | ✓             | ✓          | ✓         |
| [AWS Shield Advanced](https://aws.amazon.com/shield/)        | ✓           | ✓             |            |           |
| [AWS Snowball](https://aws.amazon.com/snowball/)             | ✓           | ✓             | ✓          | ✓         |
| [AWS Snowball Edge](https://aws.amazon.com/snowball-edge/)   | ✓           | ✓             | ✓          | ✓         |
| [AWS Snowmobile](https://aws.amazon.com/snowmobile/)         | ✓           | ✓             | ✓          | ✓         |
| [AWS Step Functions](https://aws.amazon.com/step-functions/details/) | ✓           | ✓             | ✓          |           |
| [AWS Storage Gateway](https://aws.amazon.com/storagegateway/) | ✓           | ✓             | ✓          | ✓         |
| [AWS Support](https://aws.amazon.com/premiumsupport/)        | ✓           | ✓             | ✓          | ✓         |
| [AWS Trusted Advisor](https://aws.amazon.com/premiumsupport/trustedadvisor/) | ✓           | ✓             | ✓          | ✓         |
| [AWS WAF](https://aws.amazon.com/waf/)                       | ✓           | ✓             |            |           |
| [AWS Systems Manager](https://aws.amazon.com/systems-manager/) | ✓           | ✓             | ✓          |           |
| [AWS X-Ray](https://aws.amazon.com/xray/)                    | ✓           | ✓             | ✓          |           |
| [Elastic Load Balancing](https://aws.amazon.com/elasticloadbalancing/) | ✓           | ✓             | ✓          | ✓         |
| [VM Import/Export](https://aws.amazon.com/ec2/vm-import/)    | ✓           | ✓             | ✓          | ✓         |

## Asia Pacific

| **Services Offered:**                                        | **Singapore** | **Tokyo** | **Osaka\**** | **Sydney** | **Seoul** | **Mumbai** | **Beijing\*** | **Ningxia\*** |
| ------------------------------------------------------------ | ------------- | --------- | ------------ | ---------- | --------- | ---------- | ------------- | ------------- |
| [Alexa for Business](https://aws.amazon.com/alexaforbusiness/) |               |           |              |            |           |            |               |               |
| [Amazon API Gateway](https://aws.amazon.com/api-gateway/)    | ✓             | ✓         |              | ✓          | ✓         | ✓          | ✓             |               |
| [Amazon AppStream 2.0](https://aws.amazon.com/appstream2/)   | ✓             | ✓         |              | ✓          |           |            |               |               |
| [Amazon Athena](https://aws.amazon.com/athena/)              | ✓             | ✓         |              | ✓          | ✓         | ✓          |               |               |
| [Amazon Aurora - MySQL-compatible](https://aws.amazon.com/rds/aurora/) | ✓             | ✓         |              | ✓          | ✓         | ✓          |               |               |
| [Amazon Aurora - PostgreSQL-compatible](https://aws.amazon.com/rds/aurora/) | ✓             | ✓         |              | ✓          | ✓         | ✓          |               |               |
| [Amazon Chime](https://chime.aws)                            |               |           |              |            |           |            |               |               |
| [Amazon Cloud Directory](https://aws.amazon.com/cloud-directory/) | ✓             |           |              | ✓          |           |            |               |               |
| [Amazon CloudSearch](https://aws.amazon.com/cloudsearch/)    | ✓             | ✓         |              | ✓          | ✓         |            |               |               |
| [Amazon CloudWatch](https://aws.amazon.com/cloudwatch/)      | ✓             | ✓         | ✓            | ✓          | ✓         | ✓          | ✓             | ✓             |
| [Amazon CloudWatch Events](https://aws.amazon.com/cloudwatch/) | ✓             | ✓         | ✓            | ✓          | ✓         | ✓          | ✓             | ✓             |
| [Amazon CloudWatch Logs](https://docs.aws.amazon.com/AmazonCloudWatch/latest/DeveloperGuide/CWL_GettingStarted.html) | ✓             | ✓         | ✓            | ✓          | ✓         | ✓          | ✓             | ✓             |
| [Amazon Cognito](https://aws.amazon.com/cognito/)            | ✓             | ✓         |              | ✓          | ✓         | ✓          | ✓             |               |
| [Amazon Comprehend](https://aws.amazon.com/comprehend/)      |               |           |              |            |           |            |               |               |
| [Amazon Connect](https://aws.amazon.com/connect/)            |               |           |              | ✓          |           |            |               |               |
| [Amazon DeepLens](https://aws.amazon.com/deeplens/)          |               |           |              |            |           |            |               |               |
| [Amazon DynamoDB](https://aws.amazon.com/dynamodb/)          | ✓             | ✓         | ✓            | ✓          | ✓         | ✓          | ✓             | ✓             |
| [Amazon DynamoDB Accelerator (DAX)](https://aws.amazon.com/dynamodb/dax/) | ✓             | ✓         |              | ✓          |           | ✓          |               |               |
| [Amazon Elastic Container Registry (ECR)](https://aws.amazon.com/ecr/) | ✓             | ✓         |              | ✓          | ✓         | ✓          | ✓             | ✓             |
| [Amazon Elastic Container Service (ECS)](https://aws.amazon.com/ecs/) | ✓             | ✓         |              | ✓          | ✓         | ✓          | ✓             | ✓             |
| [Amazon Elastic Container Service for Kubernetes (EKS)](https://aws.amazon.com/eks) |               |           |              |            |           |            |               |               |
| [Amazon ElastiCache](https://aws.amazon.com/elasticache/)    | ✓             | ✓         | ✓            | ✓          | ✓         | ✓          | ✓             | ✓             |
| [Amazon Elastic Block Store (EBS)](https://aws.amazon.com/ebs/) | ✓             | ✓         | ✓            | ✓          | ✓         | ✓          | ✓             | ✓             |
| [Amazon Elastic Compute Cloud (EC2)](https://aws.amazon.com/ec2/) | ✓             | ✓         | ✓            | ✓          | ✓         | ✓          | ✓             | ✓             |
| [Amazon EC2 Auto Scaling ](https://aws.amazon.com/ec2/autoscaling/) | ✓             | ✓         | ✓            | ✓          | ✓         | ✓          | ✓             | ✓             |
| [AWS Auto Scaling](https://aws.amazon.com/autoscaling/)      | ✓             |           | ✓            |            |           |            |               |               |
| [Amazon Elastic File System (EFS)](https://aws.amazon.com/efs/) |               |           |              | ✓          | ✓         |            |               |               |
| [Amazon Elastic MapReduce](https://aws.amazon.com/emr/)      | ✓             | ✓         | ✓            | ✓          | ✓         | ✓          | ✓             | ✓             |
| [Amazon Elasticsearch Service](https://aws.amazon.com/elasticsearch-service/) | ✓             | ✓         |              | ✓          | ✓         | ✓          |               | ✓             |
| [Amazon Elastic Transcoder](https://aws.amazon.com/elastictranscoder/) | ✓             | ✓         |              | ✓          |           | ✓          |               |               |
| [Amazon FreeRTOS](https://aws.amazon.com/freertos/)          | ✓             | ✓         |              | ✓          | ✓         |            |               |               |
| [Amazon GameLift](https://aws.amazon.com/gamelift/)          | ✓             | ✓         |              | ✓          | ✓         | ✓          |               |               |
| [Amazon Glacier](https://aws.amazon.com/glacier1/)           | ✓             | ✓         | ✓            | ✓          | ✓         | ✓          | ✓             | ✓             |
| [Amazon GuardDuty](https://aws.amazon.com/guardduty/)        | ✓             | ✓         |              | ✓          | ✓         | ✓          |               |               |
| [Amazon Inspector](https://aws.amazon.com/inspector/)        |               | ✓         |              | ✓          | ✓         | ✓          |               |               |
| [Amazon Kinesis Data Analytics](https://aws.amazon.com/kinesis/analytics/) |               |           |              |            |           |            |               |               |
| [Amazon Kinesis Data Firehose](https://aws.amazon.com/kinesis/firehose/) | ✓             | ✓         |              | ✓          |           |            |               |               |
| [Amazon Kinesis Data Streams  ](https://aws.amazon.com/kinesis/streams/) | ✓             | ✓         | ✓            | ✓          | ✓         | ✓          | ✓             | ✓             |
| [Amazon Kinesis Video Streams  ](https://aws.amazon.com/kinesis/video-streams/) |               | ✓         |              |            |           |            |               |               |
| [Amazon Lex](https://aws.amazon.com/lex/)                    |               |           |              |            |           |            |               |               |
| [Amazon Lightsail](https://amazonlightsail.com/)             | ✓             | ✓         |              | ✓          | ✓         | ✓          |               |               |
| [Amazon Machine Learning](https://aws.amazon.com/machine-learning/) |               |           |              |            |           |            |               |               |
| [Amazon Macie](https://aws.amazon.com/macie/)                |               |           |              |            |           |            |               |               |
| [Amazon Mobile Analytics](https://aws.amazon.com/mobileanalytics/) |               |           |              |            |           |            |               |               |
| [Amazon MQ](https://aws.amazon.com/amazon-mq/)               |               |           |              | ✓          |           |            |               |               |
| [Amazon Neptune](https://aws.amazon.com/neptune/)            |               |           |              |            |           |            |               |               |
| [Amazon Pinpoint](https://aws.amazon.com/pinpoint/)          |               |           |              |            |           |            |               |               |
| [Amazon Polly](https://aws.amazon.com/polly/)                | ✓             | ✓         |              | ✓          | ✓         | ✓          |               |               |
| [Amazon QuickSight](https://quicksight.aws)                  | ✓             |           |              | ✓          |           |            |               |               |
| [Amazon Redshift](https://aws.amazon.com/redshift/)          | ✓             | ✓         | ✓            | ✓          | ✓         | ✓          | ✓             | ✓             |
| [Amazon Rekognition Image](https://aws.amazon.com/rekognition/) |               | ✓         |              | ✓          |           |            |               |               |
| [Amazon Rekognition Video](https://aws.amazon.com/rekognition/video-features/) |               | ✓         |              | ✓          |           |            |               |               |
| [Amazon Relational Database Service (RDS)](https://aws.amazon.com/rds/) | ✓             | ✓         | ✓            | ✓          | ✓         | ✓          | ✓             | ✓             |
| [Amazon Route 53 Auto Naming  ](https://aws.amazon.com/route53/) |               |           |              |            |           |            |               |               |
| [Amazon Sagemaker](https://aws.amazon.com/sagemaker/)        |               | ✓         |              |            |           |            |               |               |
| [Amazon SimpleDB](https://aws.amazon.com/simpledb/)          | ✓             | ✓         |              | ✓          |           |            |               |               |
| [Amazon Simple Email Service (SES)](https://aws.amazon.com/ses/) |               |           |              |            |           |            |               |               |
| [Amazon Simple Notification Service (SNS)](https://aws.amazon.com/sns/) | ✓             | ✓         | ✓            | ✓          | ✓         | ✓          | ✓             | ✓             |
| [Amazon Simple Queue Service (SQS)](https://aws.amazon.com/sqs/) | ✓             | ✓         | ✓            | ✓          | ✓         | ✓          | ✓             | ✓             |
| [Amazon Simple Storage Service (S3)](https://aws.amazon.com/s3/) | ✓             | ✓         | ✓            | ✓          | ✓         | ✓          | ✓             | ✓             |
| [Amazon Simple Workflow Service (SWF)](https://aws.amazon.com/swf/) | ✓             | ✓         | ✓            | ✓          | ✓         | ✓          | ✓             | ✓             |
| [AWS Sumerian](https://aws.amazon.com/sumerian/)             |               |           |              | ✓          |           |            |               |               |
| [Amazon Transcribe](https://aws.amazon.com/transcribe/)      |               |           |              |            |           |            |               |               |
| [Amazon Translate](https://aws.amazon.com/translate/)        |               |           |              |            |           |            |               |               |
| [Amazon Virtual Private Cloud (VPC)](https://aws.amazon.com/vpc/) | ✓             | ✓         | ✓            | ✓          | ✓         | ✓          | ✓             | ✓             |
| [Amazon WorkDocs](https://aws.amazon.com/workdocs/)          | ✓             | ✓         |              | ✓          |           |            |               |               |
| [Amazon WorkMail](https://aws.amazon.com/workmail/)          |               |           |              |            |           |            |               |               |
| [Amazon WorkSpaces](https://aws.amazon.com/workspaces/)      | ✓             | ✓         |              | ✓          | ✓         |            |               |               |
| [Amazon WorkSpaces Application Manager](https://aws.amazon.com/workspaces/applicationmanager/) | ✓             |           |              | ✓          |           |            |               |               |
| [AWS Application Discovery Service](https://aws.amazon.com/application-discovery/) |               |           |              |            |           |            |               |               |
| [AWS AppSync](https://aws.amazon.com/appsync/)               | ✓             | ✓         |              | ✓          |           | ✓          |               |               |
| [AWS Batch](https://aws.amazon.com/batch/)                   | ✓             | ✓         |              | ✓          | ✓         | ✓          |               |               |
| [AWS Certificate Manager](https://aws.amazon.com/certificate-manager/) | ✓             | ✓         | ✓            | ✓          | ✓         | ✓          |               |               |
| [AWS Cloud9](https://aws.amazon.com/cloud9/)                 | ✓             |           |              |            |           |            |               |               |
| [AWS CloudFormation](https://aws.amazon.com/cloudformation/) | ✓             | ✓         | ✓            | ✓          | ✓         | ✓          | ✓             | ✓             |
| [AWS CloudHSM](https://aws.amazon.com/cloudhsm/)             | ✓             | ✓         |              | ✓          |           | ✓          |               |               |
| [AWS CloudHSM Classic](https://aws.amazon.com/cloudhsm/)     | ✓             | ✓         |              | ✓          |           |            |               |               |
| [AWS CloudTrail](https://aws.amazon.com/cloudtrail/)         | ✓             | ✓         | ✓            | ✓          | ✓         | ✓          | ✓             | ✓             |
| [AWS CodeBuild](https://aws.amazon.com/codebuild/)           | ✓             | ✓         |              | ✓          | ✓         | ✓          |               |               |
| [AWS CodeCommit](https://aws.amazon.com/codecommit/)         | ✓             | ✓         |              | ✓          | ✓         | ✓          |               |               |
| [AWS CodeDeploy](https://aws.amazon.com/codedeploy/)         | ✓             | ✓         | ✓            | ✓          | ✓         | ✓          | ✓             | ✓             |
| [AWS CodePipeline](https://aws.amazon.com/codepipeline/)     | ✓             | ✓         |              | ✓          | ✓         | ✓          |               |               |
| [AWS CodeStar](https://aws.amazon.com/codestar/)             | ✓             | ✓         |              | ✓          | ✓         |            |               |               |
| [AWS Config](https://aws.amazon.com/config/)                 | ✓             | ✓         |              | ✓          | ✓         | ✓          | ✓             | ✓             |
| [AWS Database Migration Service](https://aws.amazon.com/dms/) | ✓             | ✓         |              | ✓          | ✓         | ✓          |               |               |
| [AWS Data Pipeline](https://aws.amazon.com/datapipeline/)    |               | ✓         |              |            |           |            |               |               |
| [AWS Device Farm](https://aws.amazon.com/device-farm/)       |               |           |              |            |           |            |               |               |
| [AWS Direct Connect](https://aws.amazon.com/directconnect/)  | ✓             | ✓         | ✓            | ✓          | ✓         | ✓          | ✓             | ✓             |
| [AWS Directory Service](https://aws.amazon.com/directoryservice/) | ✓             | ✓         |              | ✓          | ✓         | ✓          |               |               |
| [AWS Elastic Beanstalk](https://aws.amazon.com/elasticbeanstalk/) | ✓             | ✓         | ✓            | ✓          | ✓         | ✓          | ✓             | ✓             |
| [AWS Elemental MediaConvert](https://aws.amazon.com/mediaconvert/) | ✓             | ✓         |              | ✓          | ✓         | ✓          |               |               |
| [AWS Elemental MediaLive](https://aws.amazon.com/medialive/) | ✓             | ✓         |              | ✓          | ✓         |            |               |               |
| [AWS Elemental MediaPackage](https://aws.amazon.com/mediapackage/) | ✓             | ✓         |              | ✓          | ✓         |            |               |               |
| [AWS Elemental MediaStore](https://aws.amazon.com/mediastore/) |               | ✓         |              | ✓          |           |            |               |               |
| [AWS Elemental MediaTailor](https://aws.amazon.com/mediatailor/) | ✓             | ✓         |              | ✓          |           |            |               |               |
| [AWS Fargate](https://aws.amazon.com/fargate/)               |               |           |              |            |           |            |               |               |
| [AWS Glue](https://aws.amazon.com/glue/)                     | ✓             | ✓         |              | ✓          | ✓         | ✓          |               |               |
| [AWS Greengrass](https://aws.amazon.com/greengrass/)         |               | ✓         |              | ✓          |           |            |               |               |
| [AWS IoT Core](https://aws.amazon.com/iot/)                  | ✓             | ✓         |              | ✓          | ✓         | ✓          | ✓             |               |
| [AWS IoT 1-Click](https://aws.amazon.com/iot-1-click/)       |               | ✓         |              |            |           |            |               |               |
| [AWS IoT Analytics](https://aws.amazon.com/iot-analytics/)   |               |           |              |            |           |            |               |               |
| [AWS IoT Device Management](https://aws.amazon.com/iot/)     | ✓             | ✓         |              | ✓          | ✓         |            |               |               |
| [AWS Key Management Service](https://aws.amazon.com/kms/)    | ✓             | ✓         | ✓            | ✓          | ✓         | ✓          |               |               |
| [AWS Lambda](https://aws.amazon.com/lambda/)                 | ✓             | ✓         |              | ✓          | ✓         | ✓          | ✓             |               |
| [AWS Managed Services](https://aws.amazon.com/managed-services/) |               |           |              | ✓          |           |            |               |               |
| [AWS Marketplace](https://aws.amazon.com/mp/)                | ✓             | ✓         |              | ✓          | ✓         | ✓          |               |               |
| [AWS Migration Hub](https://aws.amazon.com/migration-hub/)   |               |           |              |            |           |            |               |               |
| [AWS Mobile Hub](https://aws.amazon.com/mobile/)             | ✓             | ✓         |              | ✓          | ✓         | ✓          |               |               |
| [AWS OpsWorks Stacks](https://aws.amazon.com/opsworks/stacks/) | ✓             | ✓         |              | ✓          | ✓         | ✓          |               |               |
| [AWS OpsWorks for Chef Automate](https://aws.amazon.com/opsworks/chefautomate/) | ✓             | ✓         |              | ✓          |           |            |               |               |
| [AWS OpsWorks for Puppet Enterprise](https://aws.amazon.com/opsworks/chefautomate/puppetenterprise/) | ✓             | ✓         |              | ✓          |           |            |               |               |
| [AWS Personal Health Dashboard](https://aws.amazon.com/premiumsupport/phd/) | ✓             | ✓         | ✓            | ✓          | ✓         | ✓          |               |               |
| [AWS Secrets Manager](https://aws.amazon.com/secrets-manager/) | ✓             | ✓         |              | ✓          | ✓         | ✓          |               |               |
| [AWS Server Migration Service](https://aws.amazon.com/server-migration-service/) | ✓             | ✓         |              | ✓          | ✓         | ✓          | ✓             | ✓             |
| [AWS Service Catalog](https://aws.amazon.com/servicecatalog/) | ✓             | ✓         |              | ✓          | ✓         | ✓          |               |               |
| [AWS Shield Standard](https://aws.amazon.com/shield/)        | ✓             | ✓         |              | ✓          | ✓         | ✓          |               |               |
| [AWS Shield Advanced](https://aws.amazon.com/shield/)        |               | ✓         |              | ✓          |           |            |               |               |
| [AWS Snowball](https://aws.amazon.com/snowball/)             | ✓             | ✓         |              | ✓          |           | ✓          |               |               |
| [AWS Snowball Edge](https://aws.amazon.com/snowball-edge/)   | ✓             | ✓         |              | ✓          |           |            |               |               |
| [AWS Snowmobile](https://aws.amazon.com/snowmobile/)         | ✓             | ✓         |              | ✓          | ✓         | ✓          | ✓             |               |
| [AWS Step Functions](https://aws.amazon.com/step-functions/details/) | ✓             | ✓         |              | ✓          | ✓         |            |               |               |
| [AWS Storage Gateway](https://aws.amazon.com/storagegateway/) | ✓             | ✓         |              | ✓          | ✓         | ✓          | ✓             |               |
| [AWS Support](https://aws.amazon.com/premiumsupport/)        | ✓             | ✓         | ✓            | ✓          | ✓         | ✓          | ✓             | ✓             |
| [AWS Systems Manager](https://aws.amazon.com/systems-manager/) | ✓             | ✓         |              | ✓          | ✓         | ✓          |               |               |
| [AWS Trusted Advisor](https://aws.amazon.com/premiumsupport/trustedadvisor/) | ✓             | ✓         |              | ✓          | ✓         | ✓          | ✓             | ✓             |
| [AWS WAF](https://aws.amazon.com/waf/)                       |               | ✓         |              | ✓          |           |            |               |               |
| [AWS X-Ray](https://aws.amazon.com/xray/)                    | ✓             | ✓         |              | ✓          | ✓         | ✓          |               |               |
| [Elastic Load Balancing](https://aws.amazon.com/elasticloadbalancing/) | ✓             | ✓         | ✓            | ✓          | ✓         | ✓          | ✓             | ✓             |
| [VM Import/Export](https://aws.amazon.com/ec2/vm-import/)    | ✓             | ✓         |              | ✓          | ✓         | ✓          | ✓             | ✓             |

## Global

| [Amazon CloudWatch](https://aws.amazon.com/cloudwatch/)      |
| ------------------------------------------------------------ |
| [Amazon CloudWatch Events](https://aws.amazon.com/cloudwatch/) |
| [Amazon CloudWatch Logs](https://docs.aws.amazon.com/AmazonCloudWatch/latest/DeveloperGuide/CWL_GettingStarted.html) |
| [Amazon DynamoDB](https://aws.amazon.com/dynamodb/)          |
| [Amazon ElastiCache](https://aws.amazon.com/elasticache/)    |
| [Amazon Elastic Block Store (EBS)](https://aws.amazon.com/ebs/) |
| [Amazon Elastic Compute Cloud (EC2)](https://aws.amazon.com/ec2/) |
| [Amazon EC2 Auto Scaling ](https://aws.amazon.com/ec2/autoscaling/) |
| [Amazon Elastic MapReduce](https://aws.amazon.com/emr/)      |
| [Amazon Kinesis Data Streams](https://aws.amazon.com/kinesis/streams/) |
| [Amazon Redshift](https://aws.amazon.com/redshift/)          |
| [Amazon Relational Database Service (RDS)](https://aws.amazon.com/rds/) |
| [Amazon Simple Notification Service (SNS)](https://aws.amazon.com/sns/) |
| [Amazon Simple Queue Service (SQS)](https://aws.amazon.com/sqs/) |
| [Amazon Simple Storage Service (S3)](https://aws.amazon.com/s3/) |
| [Amazon Simple Workflow Service (SWF)](https://aws.amazon.com/swf/) |
| [Amazon Virtual Private Cloud (VPC)](https://aws.amazon.com/vpc/) |
| [AWS CloudTrail](https://aws.amazon.com/cloudtrail/)         |
| [AWS CodeDeploy](https://aws.amazon.com/codedeploy/)         |
| [AWS Direct Connect](https://aws.amazon.com/directconnect/)  |
| [AWS Elastic Beanstalk](https://aws.amazon.com/elasticbeanstalk/) |
| [AWS Support](https://aws.amazon.com/premiumsupport/)        |
| [Elastic Load Balancing](https://aws.amazon.com/elasticloadbalancing/) |

## Amazon S3 Storage Classes

Amazon S3 offers four different storage classes designed for different use case depending on [durability](https://en.wikipedia.org/wiki/Durability), [availability](https://en.wikipedia.org/wiki/Availability) and performance requirements[[24\]](https://en.wikipedia.org/wiki/Amazon_S3#cite_note-24). Amazon S3 Standard and [Reduced Redundancy Storage](https://en.wikipedia.org/w/index.php?title=Reduced_Redundancy_Storage&action=edit&redlink=1) (RRS)[[25\]](https://en.wikipedia.org/wiki/Amazon_S3#cite_note-25) , Amazon S3 Standard Infrequent Access (IA), Amazon S3 One Zone Infrequent Access and [Amazon Glacier](https://en.wikipedia.org/wiki/Amazon_Glacier) which is designed for data archiving.

- Amazon S3 Standard is the default class.
- Amazon S3 Standard Infrequent Access (IA) is used for less  frequently accessed data. Typical uses case are backups and disaster  recovery solutions. Cost are lower that Amazon S3 Standard but applies  additional fees per gigabyte of data retrieved.
- Amazon S3 [Reduced Redundancy Storage](https://en.wikipedia.org/w/index.php?title=Reduced_Redundancy_Storage&action=edit&redlink=1)  (RRS) designed for noncritical, reproducible data at lower levels of  redundancy it reduces costs storing data in a less fault tolerance  manner, it supports one facility fault in stead of two of Amazon S3  Standard[[26\]](https://en.wikipedia.org/wiki/Amazon_S3#cite_note-26). Typical uses case can be data that can be recreated in case of data loss. [Durability](https://en.wikipedia.org/wiki/Durability) is 99.99% in comparison with 99,999999999% of standard class.

- [Amazon Glacier](https://en.wikipedia.org/wiki/Amazon_Glacier)  designed for long-term storage of data that is infrequently accessed  and for which retrieval latency of minutes or hours are acceptable.

# Bucket Restrictions and Limitations

 A bucket is owned by the AWS account that created it. By default, you can create up to 100 buckets in each of your AWS accounts. If you need additional buckets, you can increase your bucket limit by submitting a service limit increase. For information about how to increase your bucket limit, see [AWS Service Limits](http://docs.aws.amazon.com/general/latest/gr/aws_service_limits.html) in the *AWS General Reference*.         

 Bucket ownership is not transferable; however, if a bucket is empty, you can delete it. After a bucket is deleted, the name becomes available to reuse, but the name might not be available for you to reuse for various reasons. For example, some other account could create a bucket with that name. Note, too, that it might take some time before the name can be reused. So if you want to use the same bucket name, don't delete the bucket.         

 There is no limit to the number of objects that can be stored in a bucket and no difference in performance whether you use many buckets or just a few. You can store all of your objects in a single bucket, or you can organize them across several buckets.        

If you explicitly specify an AWS Region in your create bucket request that is different from the Region that you specifed when you created the client, you might get an error.        

You cannot create a bucket within another bucket.

The high-availability engineering of Amazon S3 is focused on get, put, list, and delete operations. Because bucket operations work against a centralized, global resource space, it is not appropriate to create or delete buckets on the high-availability code path of your application. It is better to create or delete buckets in a separate initialization or setup routine that you run less often.         

> Note
>
>  If your application automatically creates buckets, choose a bucket naming scheme that is unlikely to cause naming conflicts. Ensure that your application logic  will choose a different bucket name if a bucket name is already taken. 
>












# Reference

- https://aws.amazon.com/premiumsupport/knowledge-center/create-and-activate-aws-account/
- https://docs.aws.amazon.com/IAM/latest/UserGuide/introduction.html
- https://docs.aws.amazon.com/IAM/latest/UserGuide/id_root-user.html
- https://docs.aws.amazon.com/general/latest/gr/root-vs-iam.html
- https://docs.aws.amazon.com/general/latest/gr/aws_tasks-that-require-root.html
- https://docs.aws.amazon.com/IAM/latest/UserGuide/best-practices.html#create-iam-users
- https://aws.amazon.com/iam/details/manage-permissions/
- https://docs.aws.amazon.com/IAM/latest/UserGuide/introduction_access-management.html
- https://docs.aws.amazon.com/IAM/latest/UserGuide/access_controlling.html
- https://docs.aws.amazon.com/AmazonS3/latest/dev/Welcome.html
- https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/concepts.html
- https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Welcome.html
- https://acloud.guru/forums/aws-certified-sysops-administrator-associate/discussion/-KfDwy_ivfsSbrlEdETk/can_you_assign_a_role_to_an_al
- https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_use_switch-role-ec2.html
- https://github.com/aws/aws-cli/issues/589
- https://www.forbes.com/forbes/welcome/?toURL=https://www.forbes.com/sites/benkepes/2014/11/25/scale-beyond-comprehension-some-aws-numbers/&refURL=&referrer=#226333143af6
- https://aws.amazon.com/about-aws/global-infrastructure/regional-product-services/

