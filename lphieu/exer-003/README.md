# Docker overview

Docker is an open platform for developing, shipping, and running applications. Docker enables you to separate your applications from your infrastructure so you can deliver software quickly. With Docker, you can manage your infrastructure in the same ways you manage your applications. By taking advantage of Docker’s methodologies for shipping, testing, and deploying code quickly, you can significantly reduce the delay between writing code and running it in production.

## The Docker platform

Docker provides the ability to package and run an application in a loosely isolated environment called a container. The isolation and security allow you to run many containers simultaneously on a given host. Containers are lightweight because they don’t need the extra load of a hypervisor, but run directly within the host machine’s kernel. This means you can run more containers on a given hardware combination than if you were using virtual machines. You can even run Docker containers within host machines that are actually virtual machines!

Docker provides tooling and a platform to manage the lifecycle of your containers:

- Develop your application and its supporting components using containers.
- The container becomes the unit for distributing and testing your application.
- When you’re ready, deploy your application into your production environment, as a container or an orchestrated service. This works the same whether your production environment is a local data center, a cloud provider, or a hybrid of the two.

## Docker Engine

*Docker Engine* is a client-server application with these major components:

- A server which is a type of long-running program called a daemon process (the `dockerd` command).
- A REST API which specifies interfaces that programs can use to talk to the daemon and instruct it what to do.
- A command line interface (CLI) client (the `docker` command).

![Docker Engine Components Flow](assets/engine-components-flow.png)

The CLI uses the Docker REST API to control or interact with the Docker daemon through scripting or direct CLI commands. Many other Docker applications use the underlying API and CLI.

The daemon creates and manages Docker *objects*, such as images, containers, networks, and volumes.

## Docker Images vs. Containers

In Dockerland, there are **images** and there are **containers**. The two are closely related, but distinct. For me, grasping this dichotomy has clarified Docker immensely.

### What's an Image?

An image is an inert, immutable, file that's essentially a snapshot of a container. Images are created with the [build](https://docs.docker.com/engine/reference/commandline/build/) command, and they'll produce a container when started with [run](https://docs.docker.com/engine/reference/commandline/run/). Images are stored in a Docker registry such as [registry.hub.docker.com](https://registry.hub.docker.com/).  Because they can become quite large, images are designed to be composed  of layers of other images, allowing a miminal amount of data to be sent  when transferring images over the network.

Local images can be listed by running `docker images`:

```
REPOSITORY                TAG                 IMAGE ID            CREATED             VIRTUAL SIZE
ubuntu                    13.10               5e019ab7bf6d        2 months ago        180 MB
ubuntu                    14.04               99ec81b80c55        2 months ago        266 MB
ubuntu                    latest              99ec81b80c55        2 months ago        266 MB
ubuntu                    trusty              99ec81b80c55        2 months ago        266 MB
<none>                    <none>              4ab0d9120985        3 months ago        486.5 MB
```

**Some things to note:**

1. IMAGE ID is the first 12 characters of the true identifier for an  image. You can create many tags of a given image, but their IDs will all  be the same (as above).
2. VIRTUAL SIZE is *virtual* because it's adding up the sizes of  all the distinct underlying layers. This means that the sum of all the  values in that column is probably much larger than the disk space used  by all of those images.
3. The value in the REPOSITORY column comes from the `-t` flag of the `docker build` command, or from `docker tag`-ing  an existing image. You're free to tag images using a nomenclature that  makes sense to you, but know that docker will use the tag as the  registry location in a `docker push` or `docker pull`.
4. The full form of a tag is `[REGISTRYHOST/][USERNAME/]NAME[:TAG]`. For `ubuntu` above, REGISTRYHOST is inferred to be `registry.hub.docker.com`. So if you plan on storing your image called `my-application` in a registry at `docker.example.com`, you should tag that image `docker.example.com/my-application`.
5. The TAG column is just the [:TAG] part of the *full* tag. This is unfortunate terminology.
6. The `latest` tag is not magical, it's simply the default tag when you don't specify a tag. 
7. You can have untagged images only identifiable by their IMAGE IDs. These will get the `<none>` TAG and REPOSITORY. It's easy to forget about them.

More info on images is available from the [Docker docs](https://docs.docker.com/engine/reference/commandline/images/) and [glossary](https://docs.docker.com/engine/reference/glossary/#image).

### What's a container?

To use a programming metaphor, if an image is a class, then a  container is an instance of a class—a runtime object. Containers are  hopefully why you're using Docker; they're lightweight and portable  encapsulations of an environment in which to run applications.

View local running containers with `docker ps`:

```
CONTAINER ID        IMAGE                               COMMAND                CREATED             STATUS              PORTS                    NAMES
f2ff1af05450        samalba/docker-registry:latest      /bin/sh -c 'exec doc   4 months ago        Up 12 weeks         0.0.0.0:5000->5000/tcp   docker-registry
```

Here I'm running a dockerized version of the docker registry, so that  I have a private place to store my images. Again, some things to note:

1. Like IMAGE ID, CONTAINER ID is the true identifier for the  container. It has the same form, but it identifies a different kind of  object.
2. `docker ps` only outputs *running* containers. You can view all containers (*running* or *stopped*) with `docker ps -a`.
3. NAMES can be used to identify a started container via the `--name` flag.

### How to avoid image and container buildup?

One of my early frustrations with Docker was the **seemingly constant buildup of untagged images and stopped containers**.  On a handful of occassions this buildup resulted in maxed out hard  drives slowing down my laptop or halting my automated build pipeline.  Talk about "containers everywhere"!

We can remove all untagged images by combining `docker rmi` with the recent `dangling=true` query:

`docker images -q --filter "dangling=true" | xargs docker rmi`

Docker won't be able to remove images that are behind existing containers, so you may have to remove stopped containers with `docker rm` first:

```
docker rm `docker ps --no-trunc -aq`
```

These are [known pain points](https://github.com/docker/docker/issues/928)  with Docker, and may be addressed in future releases. However, with a  clear understanding of images and containers, these situations can be  avoided with a couple of practices:

1. Always remove a useless, stopped container with `docker rm [CONTAINER_ID]`.
2. Always remove the image behind a useless, stopped container with `docker rmi [IMAGE_ID]`.

## Research docker client command and argument 

### ps (with or without -a)

`docker ps` shows running . 

`docker ps -a` shows running and stopped containers. 

### run (-d, -p, -v, -it, -rm) 

```
docker run [OPTIONS] IMAGE [COMMAND] [ARG...]
```

|Name, shorthand 	   | 	Description|
| ---------------- | -------------------------------------------------- |
| `--detach , -d`  | Run container in background and print container ID |
| `--publish , -p` | Publish a container’s port(s) to the host          |
| `--volume , -v` | Bind mount a volume |
| `--name, -it` | Assign name and allocate pseudo-TTY |
| `--rm` | Automatically remove the container when it exits |

`docker run` with `-it` :

![docker-run-with-it](assets/000027-192.168.88.112 (root).png)

`docker run` without `-it` :

![docker-run-without-it](assets/000026-192.168.88.112 (root).png)



### exec (with or without -it) 

```
docker exec [OPTIONS] CONTAINER COMMAND [ARG...]
```

| Name, shorthand      | Default | Description                                                  |
| -------------------- | ------- | ------------------------------------------------------------ |
| `--detach , -d`      |         | Detached mode: run command in the background                 |
| `--detach-keys`      |         | Override the key sequence for detaching a container          |
| `--env , -e`         |         | [API 1.25+](https://docs.docker.com/engine/api/v1.25/) Set environment variables |
| `--interactive , -i` |         | Keep STDIN open even if not attached                         |
| `--privileged`       |         | Give extended privileges to the command                      |
| `--tty , -t`         |         | Allocate a pseudo-TTY                                        |
| `--user , -u`        |         | Username or UID (format: <name\|uid>[:<group\|gid>])         |
| `--workdir , -w`     |         | [API 1.35+](https://docs.docker.com/engine/api/v1.35/) Working directory inside the container |



## Install Docker 

### Install using the repository

Before you install Docker CE for the first time on a new host machine, you need to set up the Docker repository. Afterward, you can install and update Docker from the repository.

#### Set up the repository

1. Install required packages. `yum-utils` provides the `yum-config-manager` utility, and `device-mapper-persistent-data` and `lvm2` are required by the `devicemapper` storage driver.

   ```
   $ sudo yum install -y yum-utils \
     device-mapper-persistent-data \
     lvm2
   ```

2. Use the following command to set up the **stable** repository. You always need the **stable** repository, even if you want to install builds from the **edge** or **test** repositories as well.

   ```
   $ sudo yum-config-manager \
       --add-repo \
       https://download.docker.com/linux/centos/docker-ce.repo
   ```

3. **Optional**: Enable the **edge** and **test** repositories. These repositories are included in the `docker.repo` file above but are disabled by default. You can enable them alongside the stable repository.

   ```
   $ sudo yum-config-manager --enable docker-ce-edge
   ```

   ```
   $ sudo yum-config-manager --enable docker-ce-test
   ```

   You can disable the **edge** or **test** repository by running the `yum-config-manager` command with the `--disable` flag. To re-enable it, use the `--enable` flag. The following command disables the **edge** repository.

   ```
   $ sudo yum-config-manager --disable docker-ce-edge
   ```

   > **Note**: Starting with Docker 17.06, stable releases are also pushed to the **edge** and **test** repositories.

   [Learn about **stable** and **edge** builds](https://docs.docker.com/install/).

#### Install Docker CE

1. Install the *latest version* of Docker CE, or go to the next step to install a specific version:

   ```
   $ sudo yum install docker-ce
   ```

   If prompted to accept the GPG key, verify that the fingerprint matches `060A 61C5 1B55 8A7F 742B 77AA C52F EB6B 621E 9F35`, and if so, accept it.

   > Got multiple Docker repositories?
   >
   > If you have multiple Docker repositories enabled, installing or updating without specifying a version in the `yum install` or `yum update` command always installs the highest possible version, which may not be appropriate for your stability needs.

   Docker is installed but not started. The `docker` group is created, but no users are added to the group.

   ![install-docker-success](assets/000011-192.168.88.112 (root).png)

   ![add-docker-key](assets/000012-192.168.88.112 (root).png)

2. To install a *specific version* of Docker CE, list the available versions in the repo, then select and install:

   a. List and sort the versions available in your repo. This example sorts    results by version number, highest to lowest, and is truncated:

   ```
   $ yum list docker-ce --showduplicates | sort -r
   
   docker-ce.x86_64            18.03.0.ce-1.el7.centos             docker-ce-stable
   ```

   The list returned depends on which repositories are enabled, and is specific to your version of CentOS (indicated by the `.el7` suffix in this example).

   b. Install a specific version by its fully qualified package name, which is    the package name (`docker-ce`) plus the version string (2nd column) up to    the first hyphen, separated by a hyphen (`-`), for example,    `docker-ce-18.03.0.ce`.

   ```
   $ sudo yum install docker-ce-<VERSION STRING>
   ```

   Docker is installed but not started. The `docker` group is created, but no users are added to the group.

3. Start Docker.

   ```
   $ sudo systemctl start docker
   ```

4. Verify that `docker` is installed correctly by running the `hello-world` image.

   ```
   $ sudo docker run hello-world
   ```

   This command downloads a test image and runs it in a container. When the container runs, it prints an informational message and exits.

Docker CE is installed and running. You need to use `sudo` to run Docker commands. Continue to [Linux postinstall](https://docs.docker.com/install/linux/linux-postinstall/) to allow non-privileged users to run Docker commands and for other optional configuration steps.

#### Upgrade Docker CE

To upgrade Docker CE, follow the [installation instructions](https://docs.docker.com/install/linux/docker-ce/centos/#install-docker), choosing the new version you want to install.

### Install from a package

If you cannot use Docker’s repository to install Docker, you can download the `.rpm` file for your release and install it manually. You need to download a new file each time you want to upgrade Docker.

1. Go to <https://download.docker.com/linux/centos/7/x86_64/stable/Packages/> and download the `.rpm` file for the Docker version you want to install.

   > **Note**: To install an **edge**  package, change the word `stable` in the above URL to `edge`. [Learn about **stable** and **edge** channels](https://docs.docker.com/install/).

2. Install Docker CE, changing the path below to the path where you downloaded the Docker package.

   ```
   $ sudo yum install /path/to/package.rpm
   ```

   Docker is installed but not started. The `docker` group is created, but no users are added to the group.

3. Start Docker.

   ```
   $ sudo systemctl start docker
   ```

4. Verify that `docker` is installed correctly by running the `hello-world` image.

   ```
   $ sudo docker run hello-world
   ```

   This command downloads a test image and runs it in a container. When the container runs, it prints an informational message and exits.

Docker CE is installed and running. You need to use `sudo` to run Docker commands. Continue to [Post-installation steps for Linux](https://docs.docker.com/install/linux/linux-postinstall/) to allow non-privileged users to run Docker commands and for other optional configuration steps.

#### Upgrade Docker CE

To upgrade Docker CE, download the newer package file and repeat the [installation procedure](https://docs.docker.com/install/linux/docker-ce/centos/#install-from-a-package), using `yum -y upgrade` instead of `yum -y install`, and pointing to the new file.

### Install using the convenience script

Docker provides convenience scripts at [get.docker.com](https://get.docker.com/) and [test.docker.com](https://test.docker.com/) for installing edge and testing versions of Docker CE into development environments quickly and non-interactively. The source code for the scripts is in the [`docker-install` repository](https://github.com/docker/docker-install). **Using these scripts is not recommended for production environments**, and you should understand the potential risks before you use them:

- The scripts require `root` or `sudo` privileges to run. Therefore, you should carefully examine and audit the scripts before running them.
- The scripts attempt to detect your Linux distribution and version and configure your package management system for you. In addition, the scripts do not allow you to customize any installation parameters. This may lead to an unsupported configuration, either from Docker’s point of view or from your own organization’s guidelines and standards.
- The scripts install all dependencies and recommendations of the package manager without asking for confirmation. This may install a large number of packages, depending on the current configuration of your host machine.
- The script does not provide options to specify which version of Docker to install, and installs the latest version that is released in the “edge” channel.
- Do not use the convenience script if Docker has already been installed on the host machine using another mechanism.

This example uses the script at [get.docker.com](https://get.docker.com/) to install the latest release of Docker CE on Linux. To install the latest testing version, use [test.docker.com](https://test.docker.com/) instead. In each of the commands below, replace each occurrence of `get` with `test`.

> **Warning**:
>
> Always examine scripts downloaded from the internet before running them locally.

```
$ curl -fsSL get.docker.com -o get-docker.sh
$ sudo sh get-docker.sh

<output truncated>

If you would like to use Docker as a non-root user, you should now consider
adding your user to the "docker" group with something like:

  sudo usermod -aG docker your-user

Remember to log out and back in for this to take effect!

WARNING: Adding a user to the "docker" group grants the ability to run
         containers which can be used to obtain root privileges on the
         docker host.
         Refer to https://docs.docker.com/engine/security/security/#docker-daemon-attack-surface
         for more information.
```

Docker CE is installed. It starts automatically on `DEB`-based distributions. On `RPM`-based distributions, you need to start it manually using the appropriate `systemctl` or `service` command. As the message indicates, non-root users can’t run Docker commands by default.

#### Upgrade Docker after using the convenience script

If you installed Docker using the convenience script, you should upgrade Docker using your package manager directly. There is no advantage to re-running the convenience script, and it can cause issues if it attempts to re-add repositories which have already been added to the host machine.

## What is Docker file?

When you run the Docker run command and specify WordPress, Docker  uses this file to build the image itself. The Dockerfile is essentially  the build instructions to build the image.

The advantage of a  Dockerfile over just storing the binary image (or a snapshot/template in  other virtualization systems) is that the automatic builds will ensure  you have the latest version available. This is a good thing from a  security perspective, as you want to ensure you’re not installing any  vulnerable software.

## Docker file components, syntax, cmd vs entrypoint 

Docker can build images automatically by reading the instructions from a `Dockerfile`. A `Dockerfile` is a text document that contains all the commands a user could call on the command line to assemble an image. Using `docker build` users can create an automated build that executes several command-line instructions in succession.

This page describes the commands you can use in a `Dockerfile`. When you are done reading this page, refer to the [`Dockerfile` Best Practices](https://docs.docker.com/engine/userguide/eng-image/dockerfile_best-practices/) for a tip-oriented guide.

### Usage

The [`docker build`](https://docs.docker.com/engine/reference/commandline/build/) command builds an image from a `Dockerfile` and a *context*. The build’s context is the set of files at a specified location `PATH` or `URL`. The `PATH` is a directory on your local filesystem. The `URL` is a Git repository location.

A context is processed recursively. So, a `PATH` includes any subdirectories and the `URL` includes the repository and its submodules. This example shows a build command that uses the current directory as context:

```
$ docker build .
Sending build context to Docker daemon  6.51 MB
...
```

The build is run by the Docker daemon, not by the CLI. The first thing a build process does is send the entire context (recursively) to the daemon.  In most cases, it’s best to start with an empty directory as context and keep your Dockerfile in that directory. Add only the files needed for building the Dockerfile.

> **Warning**: Do not use your root directory, `/`, as the `PATH` as it causes the build to transfer the entire contents of your hard drive to the Docker daemon.

To use a file in the build context, the `Dockerfile` refers to the file specified in an instruction, for example,  a `COPY` instruction. To increase the build’s performance, exclude files and directories by adding a `.dockerignore` file to the context directory.  For information about how to [create a `.dockerignore` file](https://docs.docker.com/engine/reference/builder/#dockerignore-file) see the documentation on this page.

Traditionally, the `Dockerfile` is called `Dockerfile` and located in the root of the context. You use the `-f` flag with `docker build` to point to a Dockerfile anywhere in your file system.

```
$ docker build -f /path/to/a/Dockerfile .
```

You can specify a repository and tag at which to save the new image if the build succeeds:

```
$ docker build -t shykes/myapp .
```

To tag the image into multiple repositories after the build, add multiple `-t` parameters when you run the `build` command:

```
$ docker build -t shykes/myapp:1.0.2 -t shykes/myapp:latest .
```

Before the Docker daemon runs the instructions in the `Dockerfile`, it performs a preliminary validation of the `Dockerfile` and returns an error if the syntax is incorrect:

```
$ docker build -t test/myapp .
Sending build context to Docker daemon 2.048 kB
Error response from daemon: Unknown instruction: RUNCMD
```

The Docker daemon runs the instructions in the `Dockerfile` one-by-one, committing the result of each instruction to a new image if necessary, before finally outputting the ID of your new image. The Docker daemon will automatically clean up the context you sent.

Note that each instruction is run independently, and causes a new image to be created - so `RUN cd /tmp` will not have any effect on the next instructions.

Whenever possible, Docker will re-use the intermediate images (cache), to accelerate the `docker build` process significantly. This is indicated by the `Using cache` message in the console output. (For more information, see the [Build cache section](https://docs.docker.com/engine/userguide/eng-image/dockerfile_best-practices/#build-cache) in the `Dockerfile` best practices guide):

```
$ docker build -t svendowideit/ambassador .
Sending build context to Docker daemon 15.36 kB
Step 1/4 : FROM alpine:3.2
 ---> 31f630c65071
Step 2/4 : MAINTAINER SvenDowideit@home.org.au
 ---> Using cache
 ---> 2a1c91448f5f
Step 3/4 : RUN apk update &&      apk add socat &&        rm -r /var/cache/
 ---> Using cache
 ---> 21ed6e7fbb73
Step 4/4 : CMD env | grep _TCP= | (sed 's/.*_PORT_\([0-9]*\)_TCP=tcp:\/\/\(.*\):\(.*\)/socat -t 100000000 TCP4-LISTEN:\1,fork,reuseaddr TCP4:\2:\3 \&/' && echo wait) | sh
 ---> Using cache
 ---> 7ea8aef582cc
Successfully built 7ea8aef582cc
```

Build cache is only used from images that have a local parent chain. This means that these images were created by previous builds or the whole chain of images was loaded with `docker load`. If you wish to use build cache of a specific image you can specify it with `--cache-from` option. Images specified with `--cache-from` do not need to have a parent chain and may be pulled from other registries.

When you’re done with your build, you’re ready to look into [*Pushing a repository to its registry*](https://docs.docker.com/engine/tutorials/dockerrepos/#/contributing-to-docker-hub).

### Format

Here is the format of the `Dockerfile`:

```
# Comment
INSTRUCTION arguments
```

The instruction is not case-sensitive. However, convention is for them to be UPPERCASE to distinguish them from arguments more easily.

Docker runs instructions in a `Dockerfile` in order. A `Dockerfile` **must start with a `FROM` instruction**. The `FROM` instruction specifies the [*Base Image*](https://docs.docker.com/engine/reference/glossary/#base-image) from which you are building. `FROM` may only be preceded by one or more `ARG` instructions, which declare arguments that are used in `FROM` lines in the `Dockerfile`.

Docker treats lines that *begin* with `#` as a comment, unless the line is a valid [parser directive](https://docs.docker.com/engine/reference/builder/#parser-directives). A `#` marker anywhere else in a line is treated as an argument. This allows statements like:

```
# Comment
RUN echo 'we are running some # of cool things'
```

Line continuation characters are not supported in comments.

### Environment replacement

Environment variables (declared with [the `ENV` statement](https://docs.docker.com/engine/reference/builder/#env)) can also be used in certain instructions as variables to be interpreted by the `Dockerfile`. Escapes are also handled for including variable-like syntax into a statement literally.

Environment variables are notated in the `Dockerfile` either with `$variable_name` or `${variable_name}`. They are treated equivalently and the brace syntax is typically used to address issues with variable names with no whitespace, like `${foo}_bar`.

The `${variable_name}` syntax also supports a few of the standard `bash` modifiers as specified below:

- `${variable:-word}` indicates that if `variable` is set then the result will be that value. If `variable` is not set then `word` will be the result.
- `${variable:+word}` indicates that if `variable` is set then `word` will be the result, otherwise the result is the empty string.

In all cases, `word` can be any string, including additional environment variables.

Escaping is possible by adding a `\` before the variable: `\$foo` or `\${foo}`, for example, will translate to `$foo` and `${foo}` literals respectively.

Example (parsed representation is displayed after the `#`):

```
FROM busybox
ENV foo /bar
WORKDIR ${foo}   # WORKDIR /bar
ADD . $foo       # ADD . /bar
COPY \$foo /quux # COPY $foo /quux
```

Environment variables are supported by the following list of instructions in the `Dockerfile`:

- `ADD`
- `COPY`
- `ENV`
- `EXPOSE`
- `FROM`
- `LABEL`
- `STOPSIGNAL`
- `USER`
- `VOLUME`
- `WORKDIR`

as well as:

- `ONBUILD` (when combined with one of the supported instructions above)

> **Note**: prior to 1.4, `ONBUILD` instructions did **NOT** support environment variable, even when combined with any of the instructions listed above.

Environment variable substitution will use the same value for each variable throughout the entire instruction. In other words, in this example:

```
ENV abc=hello
ENV abc=bye def=$abc
ENV ghi=$abc
```

will result in `def` having a value of `hello`, not `bye`. However, `ghi` will have a value of `bye` because it is not part of the same instruction that set `abc` to `bye`.

### ENV

```
ENV <key> <value>
ENV <key>=<value> ...
```

The `ENV` instruction sets the environment variable `<key>` to the value `<value>`. This value will be in the environment for all subsequent instructions in the build stage and can be [replaced inline](https://docs.docker.com/engine/reference/builder/#environment-replacement) in many as well.

The `ENV` instruction has two forms. The first form, `ENV <key> <value>`, will set a single variable to a value. The entire string after the first space will be treated as the `<value>` - including whitespace characters. The value will be interpreted for other environment variables, so quote characters will be removed if they are not escaped.

The second form, `ENV <key>=<value> ...`, allows for multiple variables to be set at one time. Notice that the second form uses the equals sign (=) in the syntax, while the first form does not. Like command line parsing, quotes and backslashes can be used to include spaces within values.

For example:

```
ENV myName="John Doe" myDog=Rex\ The\ Dog \
    myCat=fluffy
```

and

```
ENV myName John Doe
ENV myDog Rex The Dog
ENV myCat fluffy
```

will yield the same net results in the final image.

The environment variables set using `ENV` will persist when a container is run from the resulting image. You can view the values using `docker inspect`, and change them using `docker run --env <key>=<value>`.

> **Note**: Environment persistence can cause unexpected side effects. For example, setting `ENV DEBIAN_FRONTEND noninteractive` may confuse apt-get users on a Debian-based image. To set a value for a single command, use `RUN <key>=<value> <command>`.

### ADD

ADD has two forms:

- `ADD [--chown=<user>:<group>] <src>... <dest>`
- `ADD [--chown=<user>:<group>] ["<src>",... "<dest>"]` (this form is required for paths containing whitespace)

> **Note**: The `--chown` feature is only supported on Dockerfiles used to build Linux containers, and will not work on Windows containers. Since user and group ownership concepts do not translate between Linux and Windows, the use of `/etc/passwd` and `/etc/group` for translating user and group names to IDs restricts this feature to only be viable for Linux OS-based containers.

The `ADD` instruction copies new files, directories or remote file URLs from `<src>` and adds them to the filesystem of the image at the path `<dest>`.

Multiple `<src>` resources may be specified but if they are files or directories, their paths are interpreted as relative to the source of the context of the build.

Each `<src>` may contain wildcards and matching will be done using Go’s [filepath.Match](http://golang.org/pkg/path/filepath#Match) rules. For example:

```
ADD hom* /mydir/        # adds all files starting with "hom"
ADD hom?.txt /mydir/    # ? is replaced with any single character, e.g., "home.txt"
```

The `<dest>` is an absolute path, or a path relative to `WORKDIR`, into which the source will be copied inside the destination container.

```
ADD test relativeDir/          # adds "test" to `WORKDIR`/relativeDir/
ADD test /absoluteDir/         # adds "test" to /absoluteDir/
```

When adding files or directories that contain special characters (such as `[` and `]`), you need to escape those paths following the Golang rules to prevent them from being treated as a matching pattern. For example, to add a file named `arr[0].txt`, use the following;

```
ADD arr[[]0].txt /mydir/    # copy a file named "arr[0].txt" to /mydir/
```

All new files and directories are created with a UID and GID of 0, unless the optional `--chown` flag specifies a given username, groupname, or UID/GID combination to request specific ownership of the content added. The format of the `--chown` flag allows for either username and groupname strings or direct integer UID and GID in any combination. Providing a username without groupname or a UID without GID will use the same numeric UID as the GID. If a username or groupname is provided, the container’s root filesystem `/etc/passwd` and `/etc/group` files will be used to perform the translation from name to integer UID or GID respectively. The following examples show valid definitions for the `--chown` flag:

```
ADD --chown=55:mygroup files* /somedir/
ADD --chown=bin files* /somedir/
ADD --chown=1 files* /somedir/
ADD --chown=10:11 files* /somedir/
```

If the container root filesystem does not contain either `/etc/passwd` or `/etc/group` files and either user or group names are used in the `--chown` flag, the build will fail on the `ADD` operation. Using numeric IDs requires no lookup and will not depend on container root filesystem content.

In the case where `<src>` is a remote file URL, the destination will have permissions of 600. If the remote file being retrieved has an HTTP `Last-Modified` header, the timestamp from that header will be used to set the `mtime` on the destination file. However, like any other file processed during an `ADD`, `mtime` will not be included in the determination of whether or not the file has changed and the cache should be updated.

> **Note**: If you build by passing a `Dockerfile` through STDIN (`docker build - < somefile`), there is no build context, so the `Dockerfile` can only contain a URL based `ADD` instruction. You can also pass a compressed archive through STDIN: (`docker build - < archive.tar.gz`), the `Dockerfile` at the root of the archive and the rest of the archive will be used as the context of the build.

> **Note**: If your URL files are protected using authentication, you will need to use `RUN wget`, `RUN curl` or use another tool from within the container as the `ADD` instruction does not support authentication.

> **Note**: The first encountered `ADD` instruction will invalidate the cache for all following instructions from the Dockerfile if the contents of `<src>` have changed. This includes invalidating the cache for `RUN` instructions. See the [`Dockerfile` Best Practices guide](https://docs.docker.com/engine/userguide/eng-image/dockerfile_best-practices/#/build-cache) for more information.

`ADD` obeys the following rules:

- The `<src>` path must be inside the *context* of the build; you cannot `ADD ../something /something`, because the first step of a `docker build` is to send the context directory (and subdirectories) to the docker daemon.
- If `<src>` is a URL and `<dest>` does not end with a trailing slash, then a file is downloaded from the URL and copied to `<dest>`.
- If `<src>` is a URL and `<dest>` does end with a trailing slash, then the filename is inferred from the URL and the file is downloaded to `<dest>/<filename>`. For instance, `ADD http://example.com/foobar /` would create the file `/foobar`. The URL must have a nontrivial path so that an appropriate filename can be discovered in this case (`http://example.com` will not work).
- If `<src>` is a directory, the entire contents of the directory are copied, including filesystem metadata.

> **Note**: The directory itself is not copied, just its contents.

- If `<src>` is a *local* tar archive in a recognized compression format (identity, gzip, bzip2 or xz) then it is unpacked as a directory. Resources from *remote* URLs are **not** decompressed. When a directory is copied or unpacked, it has the same behavior as `tar -x`, the result is the union of:

  1. Whatever existed at the destination path and
  2. The contents of the source tree, with conflicts resolved in favor of “2.” on a file-by-file basis.

  > **Note**: Whether a file is identified as a recognized compression format or not is done solely based on the contents of the file, not the name of the file. For example, if an empty file happens to end with `.tar.gz` this will not be recognized as a compressed file and **will not** generate any kind of decompression error message, rather the file will simply be copied to the destination.

- If `<src>` is any other kind of file, it is copied individually along with its metadata. In this case, if `<dest>` ends with a trailing slash `/`, it will be considered a directory and the contents of `<src>` will be written at `<dest>/base(<src>)`.

- If multiple `<src>` resources are specified, either directly or due to the use of a wildcard, then `<dest>` must be a directory, and it must end with a slash `/`.

- If `<dest>` does not end with a trailing slash, it will be considered a regular file and the contents of `<src>` will be written at `<dest>`.

- If `<dest>` doesn’t exist, it is created along with all missing directories in its path.

### COPY

COPY has two forms:

- `COPY [--chown=<user>:<group>] <src>... <dest>`
- `COPY [--chown=<user>:<group>] ["<src>",... "<dest>"]` (this form is required for paths containing whitespace)

> **Note**: The `--chown` feature is only supported on Dockerfiles used to build Linux containers, and will not work on Windows containers. Since user and group ownership concepts do not translate between Linux and Windows, the use of `/etc/passwd` and `/etc/group` for translating user and group names to IDs restricts this feature to only be viable for Linux OS-based containers.

The `COPY` instruction copies new files or directories from `<src>` and adds them to the filesystem of the container at the path `<dest>`.

Multiple `<src>` resources may be specified but the paths of files and directories will be interpreted as relative to the source of the context of the build.

Each `<src>` may contain wildcards and matching will be done using Go’s [filepath.Match](http://golang.org/pkg/path/filepath#Match) rules. For example:

```
COPY hom* /mydir/        # adds all files starting with "hom"
COPY hom?.txt /mydir/    # ? is replaced with any single character, e.g., "home.txt"
```

The `<dest>` is an absolute path, or a path relative to `WORKDIR`, into which the source will be copied inside the destination container.

```
COPY test relativeDir/   # adds "test" to `WORKDIR`/relativeDir/
COPY test /absoluteDir/  # adds "test" to /absoluteDir/
```

When copying files or directories that contain special characters (such as `[` and `]`), you need to escape those paths following the Golang rules to prevent them from being treated as a matching pattern. For example, to copy a file named `arr[0].txt`, use the following;

```
COPY arr[[]0].txt /mydir/    # copy a file named "arr[0].txt" to /mydir/
```

All new files and directories are created with a UID and GID of 0, unless the optional `--chown` flag specifies a given username, groupname, or UID/GID combination to request specific ownership of the copied content. The format of the `--chown` flag allows for either username and groupname strings or direct integer UID and GID in any combination. Providing a username without groupname or a UID without GID will use the same numeric UID as the GID. If a username or groupname is provided, the container’s root filesystem `/etc/passwd` and `/etc/group` files will be used to perform the translation from name to integer UID or GID respectively. The following examples show valid definitions for the `--chown` flag:

```
COPY --chown=55:mygroup files* /somedir/
COPY --chown=bin files* /somedir/
COPY --chown=1 files* /somedir/
COPY --chown=10:11 files* /somedir/
```

If the container root filesystem does not contain either `/etc/passwd` or `/etc/group` files and either user or group names are used in the `--chown` flag, the build will fail on the `COPY` operation. Using numeric IDs requires no lookup and will not depend on container root filesystem content.

> **Note**: If you build using STDIN (`docker build - < somefile`), there is no build context, so `COPY` can’t be used.

Optionally `COPY` accepts a flag `--from=<name|index>` that can be used to set the source location to a previous build stage (created with `FROM .. AS <name>`) that will be used instead of a build context sent by the user. The flag also accepts a numeric index assigned for all previous build stages started with `FROM` instruction. In case a build stage with a specified name can’t be found an image with the same name is attempted to be used instead.

`COPY` obeys the following rules:

- The `<src>` path must be inside the *context* of the build; you cannot `COPY ../something /something`, because the first step of a `docker build` is to send the context directory (and subdirectories) to the docker daemon.
- If `<src>` is a directory, the entire contents of the directory are copied, including filesystem metadata.

> **Note**: The directory itself is not copied, just its contents.

- If `<src>` is any other kind of file, it is copied individually along with its metadata. In this case, if `<dest>` ends with a trailing slash `/`, it will be considered a directory and the contents of `<src>` will be written at `<dest>/base(<src>)`.
- If multiple `<src>` resources are specified, either directly or due to the use of a wildcard, then `<dest>` must be a directory, and it must end with a slash `/`.
- If `<dest>` does not end with a trailing slash, it will be considered a regular file and the contents of `<src>` will be written at `<dest>`.
- If `<dest>` doesn’t exist, it is created along with all missing directories in its path.

### ENTRYPOINT

ENTRYPOINT has two forms:

- `ENTRYPOINT ["executable", "param1", "param2"]` (*exec* form, preferred)
- `ENTRYPOINT command param1 param2` (*shell* form)

An `ENTRYPOINT` allows you to configure a container that will run as an executable.

For example, the following will start nginx with its default content, listening on port 80:

```
docker run -i -t --rm -p 80:80 nginx
```

Command line arguments to `docker run <image>` will be appended after all elements in an *exec* form `ENTRYPOINT`, and will override all elements specified using `CMD`. This allows arguments to be passed to the entry point, i.e., `docker run <image> -d` will pass the `-d` argument to the entry point. You can override the `ENTRYPOINT` instruction using the `docker run --entrypoint` flag.

The *shell* form prevents any `CMD` or `run` command line arguments from being used, but has the disadvantage that your `ENTRYPOINT` will be started as a subcommand of `/bin/sh -c`, which does not pass signals. This means that the executable will not be the container’s `PID 1` - and will *not* receive Unix signals - so your executable will not receive a `SIGTERM` from `docker stop <container>`.

Only the last `ENTRYPOINT` instruction in the `Dockerfile` will have an effect.

#### Exec form ENTRYPOINT example

You can use the *exec* form of `ENTRYPOINT` to set fairly stable default commands and arguments and then use either form of `CMD` to set additional defaults that are more likely to be changed.

```
FROM ubuntu
ENTRYPOINT ["top", "-b"]
CMD ["-c"]
```

When you run the container, you can see that `top` is the only process:

```
$ docker run -it --rm --name test  top -H
top - 08:25:00 up  7:27,  0 users,  load average: 0.00, 0.01, 0.05
Threads:   1 total,   1 running,   0 sleeping,   0 stopped,   0 zombie
%Cpu(s):  0.1 us,  0.1 sy,  0.0 ni, 99.7 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
KiB Mem:   2056668 total,  1616832 used,   439836 free,    99352 buffers
KiB Swap:  1441840 total,        0 used,  1441840 free.  1324440 cached Mem

  PID USER      PR  NI    VIRT    RES    SHR S %CPU %MEM     TIME+ COMMAND
    1 root      20   0   19744   2336   2080 R  0.0  0.1   0:00.04 top
```

To examine the result further, you can use `docker exec`:

```
$ docker exec -it test ps aux
USER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
root         1  2.6  0.1  19752  2352 ?        Ss+  08:24   0:00 top -b -H
root         7  0.0  0.1  15572  2164 ?        R+   08:25   0:00 ps aux
```

And you can gracefully request `top` to shut down using `docker stop test`.

The following `Dockerfile` shows using the `ENTRYPOINT` to run Apache in the foreground (i.e., as `PID 1`):

```
FROM debian:stable
RUN apt-get update && apt-get install -y --force-yes apache2
EXPOSE 80 443
VOLUME ["/var/www", "/var/log/apache2", "/etc/apache2"]
ENTRYPOINT ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
```

If you need to write a starter script for a single executable, you can ensure that the final executable receives the Unix signals by using `exec` and `gosu` commands:

```
#!/usr/bin/env bash
set -e

if [ "$1" = 'postgres' ]; then
    chown -R postgres "$PGDATA"

    if [ -z "$(ls -A "$PGDATA")" ]; then
        gosu postgres initdb
    fi

    exec gosu postgres "$@"
fi

exec "$@"
```

Lastly, if you need to do some extra cleanup (or communicate with other containers) on shutdown, or are co-ordinating more than one executable, you may need to ensure that the `ENTRYPOINT` script receives the Unix signals, passes them on, and then does some more work:

```
#!/bin/sh
# Note: I've written this using sh so it works in the busybox container too

# USE the trap if you need to also do manual cleanup after the service is stopped,
#     or need to start multiple services in the one container
trap "echo TRAPed signal" HUP INT QUIT TERM

# start service in background here
/usr/sbin/apachectl start

echo "[hit enter key to exit] or run 'docker stop <container>'"
read

# stop service and clean up here
echo "stopping apache"
/usr/sbin/apachectl stop

echo "exited $0"
```

If you run this image with `docker run -it --rm -p 80:80 --name test apache`, you can then examine the container’s processes with `docker exec`, or `docker top`, and then ask the script to stop Apache:

```
$ docker exec -it test ps aux
USER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
root         1  0.1  0.0   4448   692 ?        Ss+  00:42   0:00 /bin/sh /run.sh 123 cmd cmd2
root        19  0.0  0.2  71304  4440 ?        Ss   00:42   0:00 /usr/sbin/apache2 -k start
www-data    20  0.2  0.2 360468  6004 ?        Sl   00:42   0:00 /usr/sbin/apache2 -k start
www-data    21  0.2  0.2 360468  6000 ?        Sl   00:42   0:00 /usr/sbin/apache2 -k start
root        81  0.0  0.1  15572  2140 ?        R+   00:44   0:00 ps aux
$ docker top test
PID                 USER                COMMAND
10035               root                {run.sh} /bin/sh /run.sh 123 cmd cmd2
10054               root                /usr/sbin/apache2 -k start
10055               33                  /usr/sbin/apache2 -k start
10056               33                  /usr/sbin/apache2 -k start
$ /usr/bin/time docker stop test
test
real	0m 0.27s
user	0m 0.03s
sys	0m 0.03s
```

> **Note:** you can override the `ENTRYPOINT` setting using `--entrypoint`, but this can only set the binary to *exec* (no `sh -c` will be used).

> **Note**: The *exec* form is parsed as a JSON array, which means that you must use double-quotes (“) around words not single-quotes (‘).

> **Note**: Unlike the *shell* form, the *exec* form does not invoke a command shell. This means that normal shell processing does not happen. For example, `ENTRYPOINT [ "echo", "$HOME" ]` will not do variable substitution on `$HOME`. If you want shell processing then either use the *shell* form or execute a shell directly, for example: `ENTRYPOINT [ "sh", "-c", "echo $HOME" ]`. When using the exec form and executing a shell directly, as in the case for the shell form, it is the shell that is doing the environment variable expansion, not docker.

#### Shell form ENTRYPOINT example

You can specify a plain string for the `ENTRYPOINT` and it will execute in `/bin/sh -c`. This form will use shell processing to substitute shell environment variables, and will ignore any `CMD` or `docker run` command line arguments. To ensure that `docker stop` will signal any long running `ENTRYPOINT` executable correctly, you need to remember to start it with `exec`:

```
FROM ubuntu
ENTRYPOINT exec top -b
```

When you run this image, you’ll see the single `PID 1` process:

```
$ docker run -it --rm --name test top
Mem: 1704520K used, 352148K free, 0K shrd, 0K buff, 140368121167873K cached
CPU:   5% usr   0% sys   0% nic  94% idle   0% io   0% irq   0% sirq
Load average: 0.08 0.03 0.05 2/98 6
  PID  PPID USER     STAT   VSZ %VSZ %CPU COMMAND
    1     0 root     R     3164   0%   0% top -b
```

Which will exit cleanly on `docker stop`:

```
$ /usr/bin/time docker stop test
test
real	0m 0.20s
user	0m 0.02s
sys	0m 0.04s
```

If you forget to add `exec` to the beginning of your `ENTRYPOINT`:

```
FROM ubuntu
ENTRYPOINT top -b
CMD --ignored-param1
```

You can then run it (giving it a name for the next step):

```
$ docker run -it --name test top --ignored-param2
Mem: 1704184K used, 352484K free, 0K shrd, 0K buff, 140621524238337K cached
CPU:   9% usr   2% sys   0% nic  88% idle   0% io   0% irq   0% sirq
Load average: 0.01 0.02 0.05 2/101 7
  PID  PPID USER     STAT   VSZ %VSZ %CPU COMMAND
    1     0 root     S     3168   0%   0% /bin/sh -c top -b cmd cmd2
    7     1 root     R     3164   0%   0% top -b
```

You can see from the output of `top` that the specified `ENTRYPOINT` is not `PID 1`.

If you then run `docker stop test`, the container will not exit cleanly - the `stop` command will be forced to send a `SIGKILL` after the timeout:

```
$ docker exec -it test ps aux
PID   USER     COMMAND
    1 root     /bin/sh -c top -b cmd cmd2
    7 root     top -b
    8 root     ps aux
$ /usr/bin/time docker stop test
test
real	0m 10.19s
user	0m 0.04s
sys	0m 0.03s
```

#### Understand how CMD and ENTRYPOINT interact

Both `CMD` and `ENTRYPOINT` instructions define what command gets executed when running a container. There are few rules that describe their co-operation.

1. Dockerfile should specify at least one of `CMD` or `ENTRYPOINT` commands.
2. `ENTRYPOINT` should be defined when using the container as an executable.
3. `CMD` should be used as a way of defining default arguments for an `ENTRYPOINT` command or for executing an ad-hoc command in a container.
4. `CMD` will be overridden when running the container with alternative arguments.

The table below shows what command is executed for different `ENTRYPOINT` / `CMD` combinations:

|                                | No ENTRYPOINT              | ENTRYPOINT exec_entry p1_entry | ENTRYPOINT [“exec_entry”, “p1_entry”]          |
| ------------------------------ | -------------------------- | ------------------------------ | ---------------------------------------------- |
| **No CMD**                     | *error, not allowed*       | /bin/sh -c exec_entry p1_entry | exec_entry p1_entry                            |
| **CMD [“exec_cmd”, “p1_cmd”]** | exec_cmd p1_cmd            | /bin/sh -c exec_entry p1_entry | exec_entry p1_entry exec_cmd p1_cmd            |
| **CMD [“p1_cmd”, “p2_cmd”]**   | p1_cmd p2_cmd              | /bin/sh -c exec_entry p1_entry | exec_entry p1_entry p1_cmd p2_cmd              |
| **CMD exec_cmd p1_cmd**        | /bin/sh -c exec_cmd p1_cmd | /bin/sh -c exec_entry p1_entry | exec_entry p1_entry /bin/sh -c exec_cmd p1_cmd |

### WORKDIR

```
WORKDIR /path/to/workdir
```

The `WORKDIR` instruction sets the working directory for any `RUN`, `CMD`, `ENTRYPOINT`, `COPY` and `ADD` instructions that follow it in the `Dockerfile`. If the `WORKDIR` doesn’t exist, it will be created even if it’s not used in any subsequent `Dockerfile` instruction.

The `WORKDIR` instruction can be used multiple times in a `Dockerfile`. If a relative path is provided, it will be relative to the path of the previous `WORKDIR` instruction. For example:

```
WORKDIR /a
WORKDIR b
WORKDIR c
RUN pwd
```

The output of the final `pwd` command in this `Dockerfile` would be `/a/b/c`.

The `WORKDIR` instruction can resolve environment variables previously set using `ENV`. You can only use environment variables explicitly set in the `Dockerfile`. For example:

```
ENV DIRPATH /path
WORKDIR $DIRPATH/$DIRNAME
RUN pwd
```

The output of the final `pwd` command in this `Dockerfile` would be `/path/$DIRNAME`

## Run a `Docker image` as a `container`

```
docker run -i -t ubuntu-tma:0.1 /bin/bash
```

## Compose file 

Please visit link: https://docs.docker.com/compose/compose-file/

## Write and build a simple docker file 

Create folder for your image docker. Ex: "~/training/docker-image/ubuntu"

Create file `Dockerfile` in `ubuntu` you just created:

```
FROM ubuntu:16.04

# Update Ubuntu Software repository
RUN apt-get update && apt-get -y upgrade
```

Save the file and use.

```
docker build -t ubuntu-tma:0.1 .
```

The result is

![error-with-proxy](assets/000021-192.168.88.112 (root).png)

It means we need configure proxy for `docker`.

##### Configure Proxy for the Docker client

On the Docker client, create or edit the file `~/.docker/config.json` in the home directory of the user which starts containers. Add JSON such as the following, substituting the type of proxy with `httpsProxy` or `ftpProxy` if necessary, and substituting the address and port of the proxy server. You can configure multiple proxy servers at the same time.

You can optionally exclude hosts or ranges from going through the proxy server by setting a `noProxy` key to one or more comma-separated IP addresses or hosts. Using the `*` character as a wildcard is supported, as shown in this example.

```json
{
 "proxies":
 {
   "default":
   {
     "httpProxy": "http://10.10.10.10:8080",
     "noProxy": "*.tma.com.vn"
   }
 }
}
```

Save the file and try again

![the-result](assets/000025-192.168.88.112 (root).png)

##### Configure Proxy for the image can access the Internet

Edit file `Dockerfile`:

```ini
FROM ubuntu:16.04
# to know "Who am I?"
RUN whoami

# for access output of shell
ENV TERM dumb

#for access internet behide proxy
ENV http_proxy "http://10.10.10.10:8080"
```

Build the image again with command:

```bash
docker build -t ubuntu-tma:0.1 ./
```

Result

![result-rebuild-docker-image](assets/000030-192.168.88.112 (root).png)

##### Run update and upgrade the system  

```bash
docker run -it 7e01b14f7ab8 apt update && apt upgrade -y
OR
docker run -it ubuntu-tma:0.1 apt upgrade -y
```

Result is same

![docker-run](assets/000031-192.168.88.112 (root).png)

Edit `Dockerfile` and add `apt update && apt upgrade -y`:

```
FROM ubuntu:16.04
# Update Ubuntu Software repository
RUN whoami

ENV TERM dumb
ENV http_proxy "http://10.10.10.10:8080"

RUN apt update && apt upgrade -y
```

Rebuild for test.

![success-build](assets/000033-192.168.88.112 (root).png)

##### Create working dir `/workdir`  and Add `/workdir` to `HOME` environment:

```
FROM ubuntu:16.04
# Update Ubuntu Software repository
RUN whoami

ENV TERM dumb
ENV http_proxy "http://10.10.10.10:8080"

RUN apt update && apt upgrade -y

RUN mkdir /workdir
ENV HOME /workdir
RUN env
```

Result

![Success-build-with-environment](assets/000034-192.168.88.112 (root).png)

##### `print_uname.sh` in local machine

Run 

```bash
touch print_uname.sh
echo "uname -a && echo \$HOME" >> print_uname.sh
chmod +x print_uname.sh
./print_uname.sh
```

Result 

![Success-print_uname-localhost](assets/000035-192.168.88.112 (root).png)

##### Copy `print_uname.sh` in local machine to `docker` use `Dockerfile` and execute the `print_uname.sh` when starting the container 

```bash
FROM ubuntu:16.04
# Update Ubuntu Software repository
RUN whoami

ENV TERM dumb
ENV http_proxy "http://10.10.10.10:8080"

RUN apt update && apt upgrade -y

RUN mkdir /workdir
ENV HOME /workdir/
WORKDIR /workdir/
USER root
RUN env

COPY ./print_uname.sh /workdir/
ENTRYPOINT /workdir/print_uname.sh
```

Build and test

![build-test](assets/000037-192.168.88.112 (root).png)



# `Docker registry `
##### Create a `docker` registry with authentication and push your above image to it. 

Use a command like the following to start the registry container:

```
docker run -d -p 5000:5000 --restart=always --name registry registry:2
```

The registry is now ready to use.

Tag the image as `localhost:5000/ubuntu-tma`. This creates an additional tag for the existing image. When the first part of the tag is a `hostname` and `port`, `Docker` interprets this as the location of a registry, when pushing.

```
docker tag ubuntu-tma localhost:5000/ubuntu-tma
```

Push the image to the local registry running at `localhost:5000`:

```
docker push localhost:5000/ubuntu-tma
```

![push-localhost-ubuntu](assets/000046-192.168.88.112 (root).png)

##### On your machine, login to the registry and download the pushed image. 

Remove the locally-cached `ubuntu-tma` and `localhost:5000/ubuntu-tma` images, so that you can test pulling the image from your registry. This does not remove the `localhost:5000/ubuntu-tma` image from your registry.

```
docker image remove ubuntu-tma
docker image remove localhost:5000/ubuntu-tma
```

![docker-remove-image](assets/000047-192.168.88.112 (root).png)

Pull the `localhost:5000/ubuntu-tma` image from your local registry.

```
docker pull localhost:5000/ubuntu-tma
```

![docker-pull-ubuntu-tma](assets/000048-192.168.88.112 (root).png)

## Create a authentication for `Docker Registry`. 

Generate your own certificate:

```bash
mkdir -p /certs
openssl req \
  -newkey rsa:4096 -nodes -sha256 -keyout /certs/domain.key \
  -x509 -days 365 -out /certs/domain.crt
```
Remove old registry
```
docker stop $(docker ps -a -q) 
docker update --restart=no $(docker ps -a -q) 
docker rm -v $(docker ps -a -q -f status=exited)
docker rmi d1fd7d86a825
systemctl restart docker
```

Create a folder registry from in which I wanted to work:

```
mkdir registry
cd registry/
```

Now I create my folder in which I wil store my credentials

```
mkdir auth
```

Now I will create a htpasswd file with the help of a docker container. This htpasswd file will contain my credentials and my encrypted passwd.

```
docker run --entrypoint htpasswd registry:2 -Bbn lphieu 12345678x@X > auth/htpasswd
```

To verify

```
cat auth/htpasswd
```

Credentials are fine. Now I have to add my credentials to my  registry. Here for I will mount my auth directory inside my container:

```
docker run -d -p 5000:5000 \
--restart=always \
--name registry_private \
-v `pwd`/auth:/auth \
-e "REGISTRY_AUTH=htpasswd" \
-e "REGISTRY_AUTH_HTPASSWD_REALM=Registry Realm" \
-e "REGISTRY_AUTH_HTPASSWD_PATH=auth/htpasswd" \
registry:2
```

authenticate

```
docker login localhost:5000
```

![login-success](assets/000063-192.168.88.112 (root).png)

Retry the push

```
docker push localhost:5000/ubuntu-tma
```

![docker-push](assets/000064-192.168.88.112 (root).png)

Credentials are saved in `~/.docker/config.json`:

```
cat ~/.docker/config.json

{
    "auths": {
        "localhost:5000": {
            "auth": "bXl1c2VyOm15cGFzc3dvcmQ="
        }
    }
```

Don't forget it's recommended to use https when you use credentials.

##### On your Unix machine, pull the newly pushed image.

Remove all old image 

![remove-all-old](assets/000065-192.168.88.112 (root).png)

Login to `Docker Registry`:

![login-docker-registry](assets/000066-192.168.88.112 (root).png)

Pull from `Docker Registry`:

![](assets/000067-192.168.88.112 (root).png)



# Reference 

- https://docs.docker.com/engine/docker-overview/#the-docker-platform
- https://stackoverflow.com/questions/23735149/what-is-the-difference-between-a-docker-image-and-a-container
- http://paislee.io/how-to-automate-docker-deployments/
- https://github.com/wsargent/docker-cheat-sheet
- https://docs.docker.com/install/linux/docker-ce/centos/
- https://blog.codeship.com/what-is-a-dockerfile/
- https://www.tutorialspoint.com/docker/index.htm
- https://stackoverflow.com/questions/23111631/cannot-download-docker-images-behind-a-proxy
- https://blog.codeship.com/using-docker-behind-a-proxy/
- https://github.com/dockerfile/mariadb/issues/3
- https://github.com/moby/moby/issues/1493
- https://docs.docker.com/engine/reference/builder/
- https://docs.docker.com/compose/compose-file/#network_mode
- https://stackoverflow.com/questions/30455036/how-to-copy-file-from-host-to-container-using-dockerfile
- https://docs.docker.com/engine/reference/commandline/push/#examples
- https://docs.docker.com/engine/reference/commandline/login/#parent-command
- https://docs.docker.com/docker-cloud/builds/push-images/
- http://www.johnzaccone.io/entrypoint-vs-cmd-back-to-basics/
- https://stackoverflow.com/questions/21553353/what-is-the-difference-between-cmd-and-entrypoint-in-a-dockerfile
- https://www.digitalocean.com/community/tutorials/how-to-remove-docker-images-containers-and-volumes
- https://www.trainingdevops.com/insights-and-tutorials/deploying-docker-registry-with-let-s-encrypt-ssl-tls-certs
- https://medium.com/@cnadeau_/private-docker-registry-part-4-lets-secure-the-registry-250c3cef237
- https://stackoverflow.com/questions/38247362/how-i-can-use-docker-registry-with-login-password
- https://forums.docker.com/t/private-docker-registry-authenticated-required-after-successful-login/28069

