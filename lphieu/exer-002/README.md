# Exer-002 Setup Jenkins master-slave and build a project with `Git-Gradle`

## Add node (this also means creating a slave) Centos to your Windows Jenkins with label “Centos7 java gradle” 

First, install plugin follow this image and restart Jenkins:

![Install-Plugins](assets/2018-05-28 14_30_37-Update Center [Jenkins].png)

On your master machine go to **Manage Jenkins** > **Manage Nodes** > **New Node**

![initNode](assets/2018-05-28 09_18_54-Jenkins.png)

Click Ok.

Config new node follows this image:

![config-New-Node](assets/000004-Centos7 java gradle Configuration [Jenkins].png)

with Credentials. Click **Add** -> **Jenkins** .You should configure follow the image below:

![CredentialsPasswordRoot](assets/2018-05-28 15_14_21-Centos7 java gradle Configuration [Jenkins].png)

Click **Add** and **Save**.

In MobaXterm run follow command in local machine:

```
ssh-keygen -t rsa -b 4096 -C "lphieu@tma.com.vn"
```

run 

```
ssh-copy-id root@192.168.88.112
```

 to add ssh key to 192.168.88.112 server. Results:

![ssh-copy-id](assets/2018-05-28 14_08_12-MobaXterm.png)

![resultsSSHcopy](assets/2018-05-28 13_31_52-MobaXterm.png)

login don't need the password.

Back to the terminal of Linux (Slave Machine):

**Create** new a folder for **Master** can run code in **Slave node**:

```
/opt/jenkins/jenkinsW
```

go to **~/.ssh/** with **root** user, you should see 3 or 4 files like this:

```
id_rsa  id_rsa.pub  known_hosts
```

run 

```
useradd -d /var/lib/jenkins jenkins
cd /var/lib/jenkins/
cp -a ~/.ssh ./
chmod 600 ~/.ssh/authorized_keys
```

Back to Jenkins GUI Master machine:

Try to run node by click button **Launch agent**:

![run-LaunchAgent](assets/2018-05-28 15_31_15-Centos7 java gradle [Jenkins].png)

You should see the result:

![begin-terminal-node](assets/2018-05-28 15_17_57-Centos7 java gradle log [Jenkins].png)

...

![End-terminal-node](assets/2018-05-28 15_18_14-Centos7 java gradle log [Jenkins].png)

Success to add node Centos to run code from windows machine

![test-unix](assets/2018-05-28 15_49_02-test-unix-fromNode #4 Console [Jenkins].png)

## Add node (this also means creating a slave) Windows to your Centos Jenkins with label “Centos7 java gradle” 

Warning: to use `Launch agent via Java Web Start` you need to `enable JNLP agents` in `Manage Jenkins` --> `Configure Global Security` --> TCP port for JNLP agents  choose 1 port `Fixed` or `Random`. 

Like create Linux slave in below, configure `Remote root directory` similar.

- `Launch method` :  choose `Launch agent via Java Web Start` for windows.
- `Labels` :  windows_slave

![add-Windows-Slave](assets/000005-Centos7 java gradle Configuration [Jenkins].png)

Click **Save**, We will see this page:

![Laucher-WebJava](assets/2018-05-28 16_40_39-Centos7 java gradle [Jenkins].png)

Click **Launch**,

![run-Terminal](assets/2018-05-28 16_45_46-MobaXterm.png)

This is an error when you not set JAVA_HOME. So I set that:

![Set-Java_home](assets/2018-05-28 16_48_33-Environment Variables.png)

But till not work. So I found this Error by proxy in the company. so I configure that by following steps:

**Step 1**: Search `Configure Java` in start menu

![Start-Search-Java-config](assets/2018-05-28 16_53_23-Microsoft Edge.png)

**Step 2**: `Choose Network Settings...` from `Java Control Panel`

![Java-Control](assets/2018-05-28 16_56_37-Java Control Panel.png)

**Step 3**: Choose `Direct connection` from `Network Settings`

![No-Proxy](assets/2018-05-28 16_56_43-Network Settings.png)

**Step 4**: Try again and it will work. Check to Checkbox `I accept` and Click **Run** and wait

![It-work](assets/2018-05-28 16_57_08-Security Warning.png)

It's done!

![It.done](assets/2018-05-28 17_03_13-Jenkins agent.png)

![Build-2-platform](assets/2018-05-28 17_06_58-Dashboard [Jenkins].png)

## Create a `JenkinsFile`

```
pipeline {
    agent any

    stages {
        stage ('Compile Stage') {

            steps {
                sh '''
                    export JAVA_HOME=/opt/jdk1.8.0_162
                    # export PATH=$JAVA_HOME/bin:/opt/gradle/gradle-3.4.1/bin/:$PATH
                    export PATH=$JAVA_HOME/bin:$PATH
                    pwd
                    gradle clean build test
                '''
                sentEmail()
            }
        }
    }
}
def sentEmail(){
    emailext subject: '$DEFAULT_SUBJECT',
    body: 'Failed code Sync from CC to GIT .$DEFAULT_CONTENT',
    recipientProviders: [
        [$class: 'CulpritsRecipientProvider'],
        [$class: 'DevelopersRecipientProvider'],
        [$class: 'RequesterRecipientProvider']
    ],
    attachLog: true,
    replyTo: '$DEFAULT_REPLYTO',
    to: '$DEFAULT_RECIPIENTS'
}
```



## Create a new project and push the code to <http://192.168.88.134> gitlab server 

**Create** a new project in the **gitlab**:

![create-gitlab-project](assets/2018-05-28 18_01_58-New Project · GitLab.png)

**Copy** all file and folder from **Exercise-002/files** to new folder. The new folder should be like this:

```bash
[root@localhost ex02]# ls
Jenkinsfile  Jenkinsfile.declarative  src
```

Run this code will upload the project to Gitlab server:

```bash
git init
git remote add origin git@192.168.88.134:lphieu/pj_exercise-002.git
git add .
git commit -m "Initial commit"
git push -u origin master
```

**Create** new **pipeline job** follows below:

![pipelineScript](assets/2018-05-28 18_26_09-ex2-test Config [Jenkins].png)

Edit file **JenkinsFile** (in the Repository) follow below:

```groovy
pipeline {
        agent none
        stages {
            stage ('Compile Stage') {
                steps {
                    node("Centos7 java gradle") {
                        sh '''
                            export JAVA_HOME=/usr/lib/jvm/java-1.8.0
                            # export PATH=$JAVA_HOME/bin:$PATH
                            export PATH=$JAVA_HOME/bin:$PATH
                            pwd
                            gradle clean build test
                        '''
                        sentEmail()
                    }
                }
            }
        }
}
def sentEmail(){
    emailext subject: '$DEFAULT_SUBJECT',
    body: 'Failed code Sync from CC to GIT .$DEFAULT_CONTENT',
    recipientProviders: [
        [$class: 'CulpritsRecipientProvider'],
        [$class: 'DevelopersRecipientProvider'],
        [$class: 'RequesterRecipientProvider']
    ],
    attachLog: true,
    replyTo: '$DEFAULT_REPLYTO',
    to: '$DEFAULT_RECIPIENTS'
}

```

Build and I get an error:

![error-build-1](assets/error-build-1.png)

I think this error reason is gradle was not installed. So I install gradle :

First, I will need to install SDKMAN so I run this command:

```bash
curl -s "https://get.sdkman.io" | bash
source "$HOME/.sdkman/bin/sdkman-init.sh"
sdk version
```

And install gradle:

```bash
sdk install gradle 4.7
```

Install done, but still not work because `gradle` much be configure with `build.gradle` so I try to make one in root of project:

```bash
[root@localhost ex02]# touch build.gradle
[root@localhost ex02]# vi build.gradle
```

`build.gradle`with simple content:

```ini
apply plugin: 'java'
```

and try build again:![build-failed-with-gradle](assets/2018-05-29 08_57_55-192.168.88.112 (root).png)

I found `gradle` use `gradle.properties` to define environment so I try to make one in root of the project: 

```ini
org.gradle.java.home = /usr/lib/jvm/java-1.8.0-openjdk-1.8.0.171-8.b10.el7_5.x86_64
```

But I was wrong, that file can init with command: 

```bash
gradle init
```

Redefine and upload to gitlab server:

```bash
git add .
git commit -m "Upload gradle configure file"
git push
```

I change work dir to my project:

```groovy
pipeline {
        agent none
        stages {
            stage ('Compile Stage') {
                steps {
                    node("Centos7 java gradle") {
                        sh '''
                            export JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.171-8.b10.el7_5.x86_64
                            # export PATH=$JAVA_HOME/bin:/opt/gradle/gradle-3.4.1/bin/:$PATH
                            export PATH=$JAVA_HOME/bin:$PATH
                            pwd
                            cd /root/training/ex02
                            pwd
                            gradle clean build test
                        '''
                        sentEmail()
                    }
                }
            }
        }
}
def sentEmail(){
    emailext subject: '$DEFAULT_SUBJECT',
    body: 'Failed code Sync from CC to GIT .$DEFAULT_CONTENT',
    recipientProviders: [
        [$class: 'CulpritsRecipientProvider'],
        [$class: 'DevelopersRecipientProvider'],
        [$class: 'RequesterRecipientProvider']
    ],
    attachLog: true,
    replyTo: '$DEFAULT_REPLYTO',
    to: '$DEFAULT_RECIPIENTS'
}

```

Try to build and get an error:

![build-error](assets/2018-05-29 10_15_51-ex2-test 17 Console [Jenkins].png)

I think this is a bug by developer so I will try to fix it by add `;`to end of line 8 and upload code to server:

```
git add .
git commit -m "Fix bug FizzBuzz:8"
git push
```

Try build again and I get this error:

![Error-FizzBuzz](assets/2018-05-29 10_21_24-ex2-test 18 Console [Jenkins].png)

`package org.junit.jupiter.api does not exist` mean I need to add this package. So I add `JUnit` to `gradle` by add `build.gradle `:

```
...
repositories {
    mavenCentral()
}
 
dependencies {
    testImplementation(
            'org.junit.jupiter:junit-jupiter-api:5.1.0'
    )
    testRuntimeOnly(
            'org.junit.jupiter:junit-jupiter-engine:5.1.0'
    )
}
```

Try build again:

![error-build](assets/2018-05-29 11_18_14-ex2-test 19 Console [Jenkins].png)

This error mean it can not download lib `JUnit` maybe I need add proxy for `gradle` so I comeback to `gradle.properties` and add follow:

```ini
systemProp.http.proxyHost=10.10.10.10
systemProp.http.proxyPort=8080
systemProp.https.proxyHost=10.10.10.10
systemProp.https.proxyPort=8080
```

and It works:

![itwork-gradle-proxy](assets/2018-05-29 11_37_35-192.168.88.112 (root).png)

Now, try build with Jenkins

![Success-build](assets/2018-05-29 11_41_16-ex2-test 20 Console [Jenkins].png)

It looks good. 

## Configure so that we can trigger this job on remote script

Go to **Jenkins** > **job name** > **Configure** and set follow this image:

![remote-build-1](assets/000006-ex2-test Config [Jenkins].png)

string `ex2-test` is a `TOKEN` to remote build `ex2-test` job

This is a example:

```
http://192.168.88.111:8080/job/ex2-test/build?token=ex2-test
```

run like this:

![curl-runner](assets/000007-Cmder.png)

you can call it from cronjob or your code.

## Create a pipeline project that uses this `JenkinsFile` (incomplete)

First, Install `gitlab hook` plugin. (It need `ruby-runtime ` so you need install `ruby-runtime` too )

Second, Configure `Build Triggers` follow this image:

![build-triggers](assets/2018-05-29 14_07_44-ex2-test Config [Jenkins].png)

but still not work. So I try to install `Ruby` (from https://rubyinstaller.org/downloads/) but still not work. 

I move `Jenkins` folder from `C:\Program Files (x86)\Jenkins` to `C:\Jenkins` and I can install `ruby-runtime` 







## Reference

1. <https://wiki.jenkins.io/display/JENKINS/Step+by+step+guide+to+set+up+master+and+slave+machines+on+Windows>
2. <https://embeddedartistry.com/blog/2017/12/22/jenkins-configuring-a-linux-slave-node>
3. <https://viblo.asia/p/part-4-jenkins-ci-cau-hinh-slave-node-machine-va-tich-hop-he-thong-phan-tan-phan-cuoi-4P856aNLlY3>
4. https://viblo.asia/p/part-3-gerrit-code-review-with-jenkins-ci-tich-hop-ci-cd-eW65GYWOZDO
5. https://stackoverflow.com/questions/2643893/java-web-start-unable-to-load-resource
6. https://wiki.jenkins.io/display/JENKINS/Windows+slaves+fail+to+start+via+DCOM
7. https://stackoverflow.com/questions/44870978/how-to-run-multiple-stages-on-the-same-node-with-declarative-jenkins-pipeline
8. https://stackoverflow.com/questions/8523749/jenkins-slave-environment-variable-refresh
9. https://www.lynda.com/Java-tutorials/Download-install-JUnit/520534/548576-4.html
10. https://www.petrikainulainen.net/programming/testing/junit-5-tutorial-running-unit-tests-with-gradle/
11. https://stackoverflow.com/questions/5991194/gradle-proxy-configuration
12. http://www.andyfrench.info/2015/03/automatically-triggering-jenkins-build.html
13. https://stackoverflow.com/questions/44403642/jenkins-plugin-installation-failing

​    