# Exercise 6 - AWS part 2

# Hosting a Static Website on Amazon S3

You can host a static website on Amazon Simple Storage Service (Amazon S3). On a static website, individual webpages include static content. They might also contain client-side scripts. By contrast, a dynamic website relies on server-side processing, including server-side scripts such as PHP, JSP, or ASP.NET. Amazon S3 does not support server-side scripting. Amazon Web Services (AWS) also has resources for hosting dynamic websites. 

To host a static website, you configure an Amazon S3 bucket for website hosting, and then upload your website content to the bucket. This bucket must have public read access. It is intentional that everyone in the world will have read access to this bucket. The website is then available at the AWS Region-specific website endpoint of the bucket, which is in one of the following formats: 

```
<bucket-name>.s3-website-<AWS-region>.amazonaws.com
```

```
<bucket-name>.s3-website.<AWS-region>.amazonaws.com
```

For a list of AWS Region-specific website endpoints for Amazon S3, see [Website Endpoints](https://docs.aws.amazon.com/AmazonS3/latest/dev/WebsiteEndpoints.html). For example, suppose you create a bucket called `examplebucket` in the US West (Oregon) Region, and configure it as a website.  The following example URLs provide access to your website content: 

- This URL returns a default index document that you configured for the website. 

  ```
  http://examplebucket.s3-website-us-west-2.amazonaws.com/
  ```

- This URL requests the photo.jpg object, which is stored at the root level in the bucket. 

  ```
  http://examplebucket.s3-website-us-west-2.amazonaws.com/photo.jpg
  ```

- This URL requests the `docs/doc1.html` object in your bucket. 

  ```
  http://examplebucket.s3-website-us-west-2.amazonaws.com/docs/doc1.html
  ```

### Host a static website using s3

Access to S3, and upload static website downloaded from https://www.atlassian.com/time-wasting-at-work-infographic

![000164-S3 Management Console](assets/000164-S3 Management Console.png)

Set permissions 

![000165-S3 Management Console](assets/000165-S3 Management Console.png)

Set properties 

![000166-S3 Management Console](assets/000166-S3 Management Console.png)

Upload

![000167-S3 Management Console](assets/000167-S3 Management Console.png)

Completed

![000168-S3 Management Console](assets/000168-S3 Management Console.png)

Link: https://s3-ap-southeast-1.amazonaws.com/lphieu-exercise-004/static.html

## Create an API Gateway API for AWS Lambda Functions

## Set Up an IAM Role and Policy for an API to Invoke Lambda Functions 

 For API Gateway to invoke a Lambda function, the API must have a permission to call the Lambda's [InvokeFunction](http://docs.aws.amazon.com/lambda/latest/dg/API_Invoke.html) action. This means that, at minimum, you must attach the following IAM policy to an IAM role for API Gateway to assume the policy. 

```json
{
 "Version": "2012-10-17",
 "Statement": [
  {
 "Effect": "Allow",
 "Action": "lambda:InvokeFunction",
 "Resource": "*"
  }
 ]
} 
```

If you do not enact this policy, the API caller will receive a 500 Internal Server Error response. The response contains the "Invalid permissions on Lambda function" error message. For a complete list of error messages returned by Lambda, see the [Invoke](http://docs.aws.amazon.com/lambda/latest/dg/API_Invoke.html) topic.  

An API Gateway assumable role is an IAM role with the following trusted relationship:      

```
{
  "Version": "2012-10-17",
  "Statement": [
  {
  "Sid": "",
  "Effect": "Allow",
  "Principal": {
  "Service": "apigateway.amazonaws.com"
  },
  "Action": "sts:AssumeRole"
  }
  ]
} 
  
```

First, We need create new role:

![000173-IAM Management Console](assets/000173-IAM Management Console.png)

Copy and paste to JSON tab

![000174-IAM Management Console](assets/000174-IAM Management Console.png)

Review and create

![000175-IAM Management Console](assets/000175-IAM Management Console.png)

 If you do not enact this policy, the API caller will receive a 500 Internal Server Error response. The response contains the "Invalid permissions on Lambda function" error message. For a complete list of error messages returned by Lambda, see the [Invoke](http://docs.aws.amazon.com/lambda/latest/dg/API_Invoke.html) topic. 

 An API Gateway assumable role is an IAM role with the following trusted relationship: 

```
{
  "Version": "2012-10-17",
  "Statement": [
 {
 "Sid": "",
 "Effect": "Allow",
 "Principal": {
  "Service": "apigateway.amazonaws.com"
 },
 "Action": "sts:AssumeRole"
 }
  ]
} 
```

## Create a Lambda Function in the Backend 

The following procedure outlines the steps to create a Lambda function using the Lambda console. 

1. Go to the Lambda console.

2. Choose **Create a Lambda function**. 

3. Choose the **Blank Function** blueprint for the runtime of Node.js 4.3 or later. 

4. Follow the on-screen instructions to the **Lambda function code** editor. 

   ![1528689223919](assets/1528689223919.png)

5.  Copy the following Lambda function and paste it into the code editor in the Lambda console.  

   ```python
   def lambda_handler(event, context):
       rt = int(event["arg1"]) + int(event["arg2"])
       return rt
   ```

6. Choose an existing or create a new IAM role

7. Follow the on-screen instructions to finish creating the function.

 This function requires two operands (`arg1` and `arg2`) from the `event` input parameter. The input is a JSON object of the following format: 

```json
{
  "arg2": "2",
  "arg1": "3"
}
```

## Create API Resources for the Lambda Function

1. In the API Gateway console, create an API named  **apilambda**

2.  Create the **/apilambda** resource off the API's root. We will expose the GET and POST methods on this resource for the client to invoke the backend Lambda function. The caller must supply the required input as query string parameters (to be declared as `?operand1=...&operand2=...&operator=...`) in the GET request and as a JSON payload in the POST request, respectively. 

    We will also create the **/apilambda/{operand1}/{operand2}/{operator}** resource subtree to expose the GET method to invoke the Lambda function. The caller must supply the required input by specifying the three path parameters (**operand1**, **operand2**, and **operator**). 

   ![1528690811682](assets/1528690811682.png)



##  Create a GET Method with Query Parameters to Call the Lambda Function 

 By creating a GET method with query parameters to call the Lambda function, we can let the API user to do the calculations via any browser. This can be useful especially if the API allows open access. 

**To set up the GET method with query strings to invoke the Lambda function**

1.  In the API Gateway console, choose the API's **/add2numbers** resource under **Resources**. 

2. Choose **Create Method**, from the **Actions** drop-down menu, to create the GET method. 

3.  In the ensuing **Set up** pane, 

   1. Choose `AWS Service` for **Integration type**. 
   2. Choose the region (e.g., `us-west-2`) where you created the Lambda function for **AWS Region**. 
   3. Choose `Lambda` for **AWS Service**. 
   4. Leave **AWS Subdomain** blank because our Lambda function is not hosted on any of AWS subdomain. 
   5. Choose `POST` for **HTTP method**. Lambda requires that the `POST` request be used to invoke any Lambda function. This examples shows that the HTTP method in a frontend method request can be different from the integration request in the backend. 
   6. Choose `Use path override` for **Action Type**. This option allows us to specify the ARN of the [Invoke](http://docs.aws.amazon.com/lambda/latest/dg/API_Invoke.html) action to execute our `add2numbers` function. 
   7. Type `/2015-03-31/functions/arn:aws:lambda:*region*:*account-id*:function:Calc/invocations` in **Path override**. 
   8. Specify the ARN of an IAM role for **Execution role**. You can find the ARN of the role in the IAM console. The role must contain the necessary permissions for the caller to call the `add2numbers` function and for API Gateway to assume the role of the caller. 
   9. Leave the `Passthrough` as the default of **Content Handling**, because we will not deal with any binary data. 
   10. Choose the **Save** to finish setting up the `GET /add2numbers` method. 

   After the setup succeeds, the configuration should look as follows: 

   ![1528698967050](assets/1528698967050.png)

    You can also add, in **Integration Request**, the `X-Amz-Invocation-Type: Event | RequestReponse | DryRun` header to have the action invoked asynchronously, as request and response, or as a test run, respectively. If the header is not specified, the action will be invoked as request and response. 

4.  Go to **Method Request** to set up query parameters for the **GET** method on **/add2numbers** to receive input to the backend Lambda function. 

   1. Choose the pencil icon next to **Request Validator** to select `Validate query string parameters and headers`. This setting will cause an error message to return to state the required parameters are missing if the client does not specify them. You will not get charged for the call to the backend. 
   2. Expand the **URL Query String Parameters** section. Choose **Add query string** to add the **operand1**, **operand2**, and **operator** query string parameters. Check the **Required** option for each parameter to ensure that they are validated. 

   The configuration now looks as follows:

   ![1528698960015](assets/1528698960015.png)

5.  Go back to **Integration Request** to set up the mapping template to translate the client-supplied query strings to the integration request payload as required by the `add2numbers` function. 

   1. Expand the **Body Mapping Templates** section. 

   2. Choose `When no template matches the request Content-Type header` for **Request body passthrough**.  

   3. If `application/json` is not shown under **Content-Type**, choose **Add mapping template** to add it.  

   4. And then type and save the following mapping script in the mapping template editor: 

      ```
      {
       "arg1":  "$input.params('arg1')",
       "arg2" : "$input.params('arg2')"
      }
      ```

       This template maps the three query string parameters declared in **Method Request** into designated property values of the JSON object as the input to the backend Lambda function. The transformed JSON object will be included as the integration request payload.  

   The final settings of this step is shown as follows:

     ![1528699529873](assets/1528699529873.png)

6.  You can now choose **Test** to verify that the GET method on the **/add2numbers** resource has been properly set up to invoke the Lambda function. 

![1528701724210](assets/1528701724210.png)

## Deploy an API to a Stage

The API Gateway console lets you deploy an API by creating a deployment and associating it with a new or existing stage. 

> Note
>
> To associate a stage in API Gateway with a different deployment, see [Associate a Stage with a Different Deployment](https://docs.aws.amazon.com/apigateway/latest/developerguide/how-to-deploy-api-with-console.html#how-to-deploy-change-deployment-console) instead. 

1. Sign in to the API Gateway console at <https://console.aws.amazon.com/apigateway>.  
2. In the **APIs** navigation pane, choose the API you want to deploy. 
3. In the **Resources** navigation pane, choose **Actions**.  
4. From the **Actions** drop-down menu, choose **Deploy API**.  
5. In the **Deploy API** dialog, choose an entry from the **Deployment stage** dropdown list. 
6. If you choose **[New Stage]**, type a name in **Stage name** and optionally provide a description for the stage and deployment in **Stage description** and **Deployment description**. If you choose an existing stage, you may want to provide a description of the new deployment in **Deployment description**.  
7. Choose **Deploy** to deploy the API to the specified stage with default stage settings.  

![1528706505012](assets/1528706505012.png)

Link: https://1y1ftkxv0m.execute-api.us-east-2.amazonaws.com/2numbers/add2numbers?arg1=1&arg2=3































## Reference

- https://docs.aws.amazon.com/AmazonS3/latest/dev/WebsiteHosting.html
- https://docs.aws.amazon.com/apigateway/latest/developerguide/integrating-api-with-aws-services-lambda.html
- https://docs.aws.amazon.com/lambda/latest/dg/API_Invoke.html

