# Exercise 4 - docker registry and nginx

## Overview of Docker Compose

`Compose` is a tool for defining and running multi-container `Docker applications`. With `Compose`, you use a `YAML` file to configure your application’s services. Then, with a single command, you create and start all the services from your configuration. To learn more about all the features of Compose, see [the list of features](https://docs.docker.com/compose/overview/#features).

Compose works in all environments: production, staging, development, testing, as well as CI workflows. You can learn more about each case in [Common Use Cases](https://docs.docker.com/compose/overview/#common-use-cases).

Using `Compose` is basically a three-step process:

1. Define your app’s environment with a `Dockerfile` so it can be reproduced anywhere.
2. Define the services that make up your app in `docker-compose.yml` so they can be run together in an isolated environment.
3. Run `docker-compose up` and Compose starts and runs your entire app.

A `docker-compose.yml` looks like this:

```
version: '3'
services:
  web:
    build: .
    ports:
    - "5000:5000"
    volumes:
    - .:/code
    - logvolume01:/var/log
    links:
    - redis
  redis:
    image: redis
volumes:
  logvolume01: {}
```

For more information about the Compose file, see the [Compose file reference](https://docs.docker.com/compose/compose-file/).

Compose has commands for managing the whole lifecycle of your application:

- Start, stop, and rebuild services
- View the status of running services
- Stream the log output of running services
- Run a one-off command on a service

## Install Compose on Linux systems

On **Linux**, you can download the Docker Compose binary from the [Compose repository release page on GitHub](https://github.com/docker/compose/releases). Follow the instructions from the link, which involve running the `curl` command in your terminal to download the binaries. These step by step instructions are also included below.

1. Run this command to download the latest version of Docker Compose:

   ```
   sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
   ```

   > Use the latest Compose release number in the download command.
   >
   > The above command is an *example*, and it may become out-of-date. To ensure you have the latest version, check the [Compose repository release page on GitHub](https://github.com/docker/compose/releases).

   If you have problems installing with `curl`, see [Alternative Install Options](https://docs.docker.com/compose/install/#alternative-install-options) tab above.

2. Apply executable permissions to the binary:

   ```
   sudo chmod +x /usr/local/bin/docker-compose
   ```

3. Optionally, install [command completion](https://docs.docker.com/compose/completion/) for the `bash` and `zsh` shell.

4. Test the installation.

   ```bash
   docker-compose --version
   ```



## Get started with Docker Compose

   Estimated reading time:            10 minutes    

On this page you build a simple Python web application running on Docker Compose. The application uses the Flask framework and maintains a hit counter in Redis. While the sample uses Python, the concepts demonstrated here should be understandable even if you’re not familiar with it.

### Prerequisites

Make sure you have already installed both [Docker Engine](https://docs.docker.com/install/) and [Docker Compose](https://docs.docker.com/compose/install/). You don’t need to install Python or Redis, as both are provided by Docker images.

### Step 1: Setup

Define the application dependencies.

1. Create a directory for the project:

   ```
   $ mkdir composetest
   $ cd composetest
   ```

2. Create a file called `app.py` in your project directory and paste this in:

   ```
   import time
   
   import redis
   from flask import Flask
   
   
   app = Flask(__name__)
   cache = redis.Redis(host='redis', port=6379)
   
   
   def get_hit_count():
       retries = 5
       while True:
           try:
               return cache.incr('hits')
           except redis.exceptions.ConnectionError as exc:
               if retries == 0:
                   raise exc
               retries -= 1
               time.sleep(0.5)
   
   
   @app.route('/')
   def hello():
       count = get_hit_count()
       return 'Hello World! I have been seen {} times.\n'.format(count)
   
   if __name__ == "__main__":
       app.run(host="0.0.0.0", debug=True)
   ```

   In this example, `redis` is the hostname of the redis container on the   application’s network. We use the default port for Redis, `6379`.

   > Handling transient errors
   >
   > Note the way the `get_hit_count` function is written. This basic retry loop lets us attempt our request multiple times if the redis service is not available. This is useful at startup while the application comes online, but also makes our application more resilient if the Redis service needs to be restarted anytime during the app’s lifetime. In a cluster, this also helps handling momentary connection drops between nodes.

3. Create another file called `requirements.txt` in your project directory and paste this in:

   ```
   flask
   redis
   ```

### Step 2: Create a Dockerfile

In this step, you write a Dockerfile that builds a Docker image. The image contains all the dependencies the Python application requires, including Python itself.

In your project directory, create a file named `Dockerfile` and paste the following:

```
FROM python:3.4-alpine
ADD . /code
WORKDIR /code
ENV http_proxy "http://10.10.10.10:8080"
RUN pip install -r requirements.txt
CMD ["python", "app.py"]
```

This tells Docker to:

- Build an image starting with the Python 3.4 image.
- Add the current directory `.` into the path `/code` in the image.
- Set the working directory to `/code`.
- Install the Python dependencies.
- Set the default command for the container to `python app.py`.

For more information on how to write Dockerfiles, see the [Docker user guide](https://docs.docker.com/engine/tutorials/dockerimages/#building-an-image-from-a-dockerfile) and the [Dockerfile reference](https://docs.docker.com/engine/reference/builder/).

### Step 3: Define services in a Compose file

Create a file called `docker-compose.yml` in your project directory and paste the following:

```
version: '3'
services:
  web:
    build: .
    ports:
     - "5000:5000"
  redis:
    image: "redis:alpine"
```

This Compose file defines two services, `web` and `redis`. The web service:

- Uses an image that’s built from the `Dockerfile` in the current directory.
- Forwards the exposed port 5000 on the container to port 5000 on the host machine. We use the default port for the Flask web server, `5000`.

The `redis` service uses a public [Redis](https://registry.hub.docker.com/_/redis/) image pulled from the Docker Hub registry.

### Step 4: Build and run your app with Compose

**Prerequisites:** 

run `telnet telehack.com` and fill your TMA username and password for pass proxy server.



1. From your project directory, start up your application by running `docker-compose up`.

   ```
   $ docker-compose up
   Creating network "composetest_default" with the default driver
   Creating composetest_web_1 ...
   Creating composetest_redis_1 ...
   Creating composetest_web_1
   Creating composetest_redis_1 ... done
   Attaching to composetest_web_1, composetest_redis_1
   web_1    |  * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)
   redis_1  | 1:C 17 Aug 22:11:10.480 # oO0OoO0OoO0Oo Redis is starting oO0OoO0OoO0Oo
   redis_1  | 1:C 17 Aug 22:11:10.480 # Redis version=4.0.1, bits=64, commit=00000000, modified=0, pid=1, just started
   redis_1  | 1:C 17 Aug 22:11:10.480 # Warning: no config file specified, using the default config. In order to specify a config file use redis-server /path/to/redis.conf
   web_1    |  * Restarting with stat
   redis_1  | 1:M 17 Aug 22:11:10.483 * Running mode=standalone, port=6379.
   redis_1  | 1:M 17 Aug 22:11:10.483 # WARNING: The TCP backlog setting of 511 cannot be enforced because /proc/sys/net/core/somaxconn is set to the lower value of 128.
   web_1    |  * Debugger is active!
   redis_1  | 1:M 17 Aug 22:11:10.483 # Server initialized
   redis_1  | 1:M 17 Aug 22:11:10.483 # WARNING you have Transparent Huge Pages (THP) support enabled in your kernel. This will create latency and memory usage issues with Redis. To fix this issue run the command 'echo never > /sys/kernel/mm/transparent_hugepage/enabled' as root, and add it to your /etc/rc.local in order to retain the setting after a reboot. Redis must be restarted after THP is disabled.
   web_1    |  * Debugger PIN: 330-787-903
   redis_1  | 1:M 17 Aug 22:11:10.483 * Ready to accept connections
   ```

   Compose pulls a Redis image, builds an image for your code, and starts the services you defined. In this case, the code is statically copied into the image at build time.

2. Enter `http://0.0.0.0:5000/` in a browser to see the application running.

   If you’re using Docker natively on Linux, Docker for Mac, or Docker for Windows, then the web app should now be listening on port 5000 on your Docker daemon host. Point your web browser to `http://localhost:5000` to find the `Hello World` message. If this doesn’t resolve, you can also try `http://0.0.0.0:5000`.

   If you’re using Docker Machine on a Mac or Windows, use `docker-machine ip MACHINE_VM` to get the IP address of your Docker host. Then, open `http://MACHINE_VM_IP:5000` in a browser.

   You should see a message in your browser saying:

   ```
   Hello World! I have been seen 1 times.
   ```

   ![hello world in browser](assets/quick-hello-world-1.png)

3. Refresh the page.

   The number should increment.

   ```
   Hello World! I have been seen 2 times.
   ```

   ![hello world in browser](assets/quick-hello-world-2.png)

4. Switch to another terminal window, and type `docker image ls` to list local images.

   Listing images at this point should return `redis` and `web`.

   ```
   $ docker image ls
   REPOSITORY              TAG                 IMAGE ID            CREATED             SIZE
   composetest_web         latest              e2c21aa48cc1        4 minutes ago       93.8MB
   python                  3.4-alpine          84e6077c7ab6        7 days ago          82.5MB
   redis                   alpine              9d8fa9aa0e5b        3 weeks ago         27.5MB
   ```

   You can inspect images with `docker inspect <tag or id>`.

5. Stop the application, either by running `docker-compose down` from within your project directory in the second terminal, or by hitting CTRL+C in the original terminal where you started the app.

### Step 5: Edit the Compose file to add a bind mount

Edit `docker-compose.yml` in your project directory to add a [bind mount](https://docs.docker.com/engine/admin/volumes/bind-mounts/) for the `web` service:

```
version: '3'
services:
  web:
    build: .
    ports:
     - "5000:5000"
    volumes:
     - .:/code
  redis:
    image: "redis:alpine"
```

The new `volumes` key mounts the project directory (current directory) on the host to `/code` inside the container, allowing you to modify the code on the fly, without having to rebuild the image.

### Step 6: Re-build and run the app with Compose

From your project directory, type `docker-compose up` to build the app with the updated Compose file, and run it.

```
$ docker-compose up
Creating network "composetest_default" with the default driver
Creating composetest_web_1 ...
Creating composetest_redis_1 ...
Creating composetest_web_1
Creating composetest_redis_1 ... done
Attaching to composetest_web_1, composetest_redis_1
web_1    |  * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)
...
```

Check the `Hello World` message in a web browser again, and refresh to see the count increment.

> Shared folders, volumes, and bind mounts
>
> - If your project is outside of the `Users` directory (`cd ~`), then you need to share the drive or location of the Dockerfile and volume you are using. If you get runtime errors indicating an application file is not found, a volume mount is denied, or a service cannot start, try enabling file or drive sharing. Volume mounting requires shared drives for projects that live outside of `C:\Users` (Windows) or `/Users` (Mac), and is required for *any* project on Docker for Windows that uses [Linux containers](https://docs.docker.com/docker-for-windows/#switch-between-windows-and-linux-containers-beta-feature). For more information, see [Shared Drives](https://docs.docker.com/docker-for-windows/#shared-drives) on Docker for Windows, [File sharing](https://docs.docker.com/docker-for-mac/#file-sharing) on Docker for Mac, and the general examples on how to [Manage data in containers](https://docs.docker.com/engine/tutorials/dockervolumes/).
> - If you are using Oracle VirtualBox on an older Windows OS, you  might encounter an issue with shared folders as described in this [VB trouble ticket](https://www.virtualbox.org/ticket/14920). Newer Windows systems meet the requirements for [Docker for Windows](https://docs.docker.com/docker-for-windows/install/) and do not need VirtualBox.

### Step 7: Update the application

Because the application code is now mounted into the container using a volume, you can make changes to its code and see the changes instantly, without having to rebuild the image.

1. Change the greeting in `app.py` and save it. For example, change the `Hello World!` message to `Hello from Docker!`:

   ```
   return 'Hello from Docker! I have been seen {} times.\n'.format(count)
   ```

2. Refresh the app in your browser. The greeting should be updated, and the counter should still be incrementing.

   ![hello world in browser](assets/quick-hello-world-3.png)

### Step 8: Experiment with some other commands

If you want to run your services in the background, you can pass the `-d` flag (for “detached” mode) to `docker-compose up` and use `docker-compose ps` to see what is currently running:

```
$ docker-compose up -d
Starting composetest_redis_1...
Starting composetest_web_1...

$ docker-compose ps
Name                 Command            State       Ports
-------------------------------------------------------------------
composetest_redis_1   /usr/local/bin/run         Up
composetest_web_1     /bin/sh -c python app.py   Up      5000->5000/tcp
```

The `docker-compose run` command allows you to run one-off commands for your services. For example, to see what environment variables are available to the `web` service:

```
$ docker-compose run web env
```

See `docker-compose --help` to see other available commands. You can also install [command completion](https://docs.docker.com/compose/completion/) for the bash and zsh shell, which also shows you available commands.

If you started Compose with `docker-compose up -d`, stop your services once you’ve finished with them:

```
$ docker-compose stop
```

You can bring everything down, removing the containers entirely, with the `down` command. Pass `--volumes` to also remove the data volume used by the Redis container:

```
$ docker-compose down --volumes
```

## What is nginx ? What is it used for ?

**NGINX** is open source software for web serving,  reverse proxying, caching, load balancing, media streaming, and more. It  started out as a web server designed for maximum performance and  stability. In addition to its HTTP server capabilities, NGINX can also  function as a proxy server for email (IMAP, POP3, and SMTP) and a  reverse proxy and load balancer for HTTP, TCP, and UDP servers.

### Backstory

Igor Sysoev originally wrote NGINX to solve the [C10K problem](https://en.wikipedia.org/wiki/C10k_problem), a term coined in 1999 to describe the difficulty that existing web servers experienced in handling large numbers (the *10K*) of concurrent connections (the *C*). With its event-driven, asynchronous architecture, [NGINX](http://nginx.org/en) revolutionized how servers operate in [high-performance contexts](https://www.nginx.com/blog/inside-nginx-how-we-designed-for-performance-scale/) and became the fastest web server available.

After open sourcing the project in 2004 and watching its use grow  exponentially, Sysoev co-founded NGINX, Inc. to support continued  development of NGINX and to market [NGINX Plus](https://www.nginx.com/products/)  as a commercial product with additional features designed for  enterprise customers. Today, NGINX and NGINX Plus can handle hundreds of  thousands of concurrent connections, and power [more than 50% of the busiest sites](http://w3techs.com/technologies/cross/web_server/ranking) on the web.

### NGINX as a Web Server

The goal behind NGINX was to create the fastest web server around, and maintaining that excellence is still a [central goal of the project](https://www.nginx.com/blog/nginx-open-source-reflecting-back-and-looking-ahead/). NGINX consistently beats Apache and other servers in [benchmarks measuring web server performance](https://www.rootusers.com/linux-web-server-performance-benchmark-2016-results/). Since the original release of NGINX however, websites have expanded from simple HTML pages to dynamic, multifaceted content. [NGINX has grown along with it](https://www.nginx.com/blog/nginx-vs-apache-our-view/)  and now supports all the components of the modern Web, including  WebSocket, HTTP/2, and streaming of multiple video formats (HDS, HLS,  RTMP, and others).

### NGINX Beyond Web Serving

Though NGINX became famous as the fastest web server, the scalable underlying architecture has proved ideal for [many web tasks beyond serving content](https://www.nginx.com/products/). Because it can handle a high volume of connections, NGINX is commonly used as a reverse proxy and [load balancer](https://www.nginx.com/blog/five-reasons-use-software-load-balancer/)  to manage incoming traffic and distribute it to slower upstream  servers – anything from legacy database servers to microservices. 

NGINX also is frequently placed between clients and a second web  server, to serve as an SSL/TLS terminator or a web accelerator. Acting  as an intermediary, NGINX efficiently handles tasks that might slow down  your web server, such as negotiating SSL/TLS or compressing and caching  content to improve performance. Dynamic sites, built using anything  from Node.js to PHP, commonly deploy NGINX as a content cache and  reverse proxy to reduce load on application servers and make the [most effective use](https://www.nginx.com/blog/nginx-and-nginx-plus-everywhere/) of the underlying hardware. 

### What Can NGINX and NGINX Plus Do for You?

NGINX Plus and NGINX are the best-in-class web server and application  delivery solutions used by high‑traffic websites such as Dropbox,  Netflix, and Zynga. More than [409 million websites](https://news.netcraft.com/archives/2018/03/27/march-2018-web-server-survey.html) worldwide, including [the majority of the 100,000 busiest websites](http://w3techs.com/technologies/cross/web_server/ranking), rely on NGINX Plus and NGINX to deliver their content quickly, reliably, and securely.

- NGINX makes hardware load balancers obsolete. As a software-only open source load balancer, NGINX is [less expensive](https://www.nginx.com/blog/nginx-plus-vs-f5-big-ip-a-price-performance-comparison/) and more configurable than hardware load balancers, and is designed for modern cloud architectures. NGINX Plus supports [on-the-fly reconfiguration](https://www.nginx.com/products/on-the-fly-reconfiguration) and integrates with modern DevOps tools for easier monitoring.
- NGINX is a multifunction tool. With NGINX, you can use the same tool  as your load balancer, reverse proxy, content cache, and web server,  minimizing the amount of tooling and configuration your organization  needs to maintain. NGINX offers tutorials, webinars, and a wide array of  [documentation](https://www.nginx.com/resources/library/) to get you on your feet. NGINX Plus includes [rapid-response customer support](https://www.nginx.com/support/), so you can easily get help diagnosing any part of your stack that uses NGINX or NGINX Plus.
- NGINX keeps evolving. For the past decade NGINX has been at the  forefront of development of the modern Web, and has helped lead the way  on everything from HTTP/2 to microservices support. As development and  delivery of web applications continue to evolve, NGINX Plus keeps adding  features to enable flawless application delivery, from the recently  announced support for configuration using an implementation of [JavaScript customized for NGINX](https://www.nginx.com/blog/nginscript-why-our-own-javascript-implementation/), to support for [dynamic modules](https://www.nginx.com/blog/future-nginx-nginx-plus/). Using NGINX Plus ensures you’ll stay at the cutting edge of web performance.

## How To Install Nginx on CentOS 7     

## Install Apache

The Apache web server is currently the most popular web server in the  world, which makes it a great default choice for hosting a website.

We can install Apache easily using CentOS's package manager, `yum`.   A package manager allows us to install most software pain-free from a  repository maintained by CentOS.  You can learn more about [how to use `yum`](https://www.digitalocean.com/community/tutorials/how-to-set-up-and-use-yum-repositories-on-a-centos-6-vps) here.

For our purposes, we can get started by typing these commands:

```
sudo yum install httpd
```

Since we are using a `sudo` command, these operations get  executed with root privileges.  It will ask you for your regular user's  password to verify your intentions.

Afterwards, your web server is installed.

Once it installs, you can start Apache on your VPS:

```
sudo systemctl start httpd.service
```

You can do a spot check right away to verify that everything went as  planned by visiting your server's public IP address in your web browser  (see the note under the next heading to find out what your public IP  address is if you do not have this information already):

```
http://your_server_IP_address/
```

You will see the default CentOS 7 Apache web page, which is there for  informational and testing purposes.  It should look something like  this:

![CentOS 7 Apache default](assets/default_apache.png)

If you see this page, then your web server is now correctly installed.

**Change port apache from 80 to 81:**

In this file `/etc/httpd/conf/httpd.conf` there should be a line: ServerRoot `"/etc/httpd"` Below this line there is `Listen 80` Change this to `Listen 81` and restart apache

Restarting with command: `systemctl restart httpd`

The last thing you will want to do is enable Apache to start on boot. Use the following command to do so:

```
sudo systemctl enable httpd.service
```

now, you can visit to your wesite by:

```
http://your_server_IP_address:81/
```

### Installing MySQL (MariaDB)

Once  the web server is installed, we can proceed further towards MySQL  installation. MariaDB is a community fork of the old and well known  MySQL service. Since MariaDB comes with default CentOS repositories we  can run Yum to install it:

```
yum install mariadb-server mariadb -y
```

After finishing the installation, enable and start the service:

```
systemctl start mariadb
systemctl enable mariadb
```

Lastly, run initial setup script which will remove some of the default settings:

```
mysql_secure_installation
```

MariaDB  will ask you for the root password, however, since this is initial  installation, you don’t have any, so just press enter. Next prompt will  ask if you want to set a root password, enter **Y** and follow the instructions:

```
Enter current password for root (enter for none):
OK, successfully used password, moving on...

Setting the root password ensures that nobody can log into the MariaDB
root user without the proper authorization.

New password: password
Re-enter new password: password
Password updated successfully!
Reloading privilege tables..
 ... Success!
```

You can safely click **ENTER** key and accept default settings for all other questions. After you complete the setup, proceed further to **PHP** installation.

### Installing PHP v7.1.0

The first thing that we will do is install additional CentOS repo which contains required packages for PHP v7.1:

```
wget http://rpms.remirepo.net/enterprise/remi-release-7.rpm
rpm -Uvh remi-release-7.rpm
```

Enable `php71` repository which is disabled by default:

```
yum install yum-utils -y
yum-config-manager --enable remi-php71
```

Secondly, install PHP package:

```
yum --enablerepo=remi,remi-php71 install php-fpm php-common
```

Install common modules:

```
yum --enablerepo=remi,remi-php71 install php-opcache php-pecl-apcu php-cli php-pear php-pdo php-mysqlnd php-pgsql php-pecl-mongodb php-pecl-redis php-pecl-memcache php-pecl-memcached php-gd php-mbstring php-mcrypt php-xml
```

## Configuring nginx as a load balancer

### Prerequisites

The  steps in this tutorial require the user to have root privileges.  You can see how to set that up by following steps 3 and 4 in the [Initial Server Setup with CentOS 7](https://www.digitalocean.com/community/tutorials/initial-server-setup-with-centos-7) tutorial.

### Step One—Add Nginx Repository

To add the CentOS 7 EPEL repository, open terminal and use the following command:

```
sudo yum install epel-release
```

### Step Two—Install Nginx

Now that the Nginx repository is installed on your server, install Nginx using the following `yum` command:

```
sudo yum install nginx
```

After you answer yes to the prompt, Nginx will finish installing on your virtual private server (VPS).

### Step Three—Start Nginx

Nginx does not start on its own. To get Nginx running, type:

```
sudo systemctl start nginx
```

If you are running a firewall, run the following commands to allow HTTP and HTTPS traffic:

```
sudo firewall-cmd --permanent --zone=public --add-service=http 
sudo firewall-cmd --permanent --zone=public --add-service=https
sudo firewall-cmd --reload
```

You can do a spot check right away to verify that everything went as  planned by visiting your server's public IP address in your web browser  (see the note under the next heading to find out what your public IP  address is if you do not have this information already):

```
http://server_domain_name_or_IP/
```

You will see the default CentOS 7 Nginx web page, which is there for  informational and testing purposes.  It should look something like this:

![CentOS 7 Nginx Default](assets/nginx_default.png)

If you see this page, then your web server is now correctly installed.

Before continuing, you will probably want to enable Nginx to start  when your system boots. To do so, enter the following command:

```
sudo systemctl enable nginx
```

Congratulations! Nginx is now installed and running!

#### Configuring nginx as a load balancer

With nginx installed and tested you can start configuring it for load  balancing. In essence all you need to do is setup nginx with  instructions for which type of connections to listen to and where to  redirect them. To accomplish this, create a new configuration file using  which ever text editor you prefer, for example with *nano*:

```
mkdir -p /var/log/nginx/log/
sudo nano /etc/nginx/conf.d/default.conf
```

In the *default.conf* you’ll need to define the following two segments, *upstream* and *server*, see the examples below.

```j
upstream backend {
        server 192.168.88.112:81;
}

server {
    listen       80;
    server_name  192.168.88.112;

    #charset koi8-r;
    access_log  /var/log/nginx/log/host.access.log  main;
    error_log  /var/log/nginx/log/nginx_error.log  warn;

    location / {
                proxy_pass   http://backend;
    }

    #error_page  404              /404.html;

    # redirect server error pages to the static page /50x.html
    #
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }

    # proxy the PHP scripts to Apache listening on 127.0.0.1:80

    location ~ \.php$ {
        proxy_set_header X-Real-IP  $remote_addr;
        proxy_set_header X-Forwarded-For $remote_addr;
        proxy_set_header Host $host;
        proxy_pass http://127.0.0.1:81;
    }

    # pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
    #
    #location ~ \.php$ {
    #    root           html;
    #    fastcgi_pass   127.0.0.1:9000;
    #    fastcgi_index  index.php;
    #    fastcgi_param  SCRIPT_FILENAME  /scripts$fastcgi_script_name;
    #    include        fastcgi_params;
    #}

    # deny access to .htaccess files, if Apache's document root
    # concurs with nginx's one
    #
    location ~ /\.ht {
        deny  all;
    }
}

```

Then save the file and exit the editor.

Then use the following to restart nginx.

```
systemctl restart nginx
```

Check that nginx starts successfully. If the restart fails, take a look at the  */etc/nginx/conf.d/default.conf* you just created to make sure there are no mistypes or missing semicolons.

You should now be passed to one of your back-end servers when  entering the load balancer’s public IP address in your web browser.





## Learning How to Expose the Port

In this section we'll download the Nginx Docker image and show you  how to run the container so it's publicly accessible as a web server.

By default containers are not accessible from the Internet, so we need to map the container's *internal* port to the Droplet's port. That's what this section will teach you!

First, though, we'll get the Nginx image.

Run the following command to get the Nginx Docker image:

```
sudo docker pull nginx
```

This downloads all the necessary components for the container. Docker  will cache these, so when we run the container we don't need to  download the container image(s) each time.

Docker maintains a site called [Dockerhub](https://hub.docker.com/),  a public repository of Docker files (including both official and  user-submitted images). The image we downloaded is the official Nginx  one, which saves us from having to build our own image.

Let's start our Nginx Docker container with this command:

```
sudo docker run --name docker-nginx -p 80:80 nginx
```

- `run` is the command to create a new container
- The `--name` flag is how we specify the name of the container (if left blank one is assigned for us, like nostalgic_hopper from Step 2)
- `-p` specifies the port we are exposing in the format of `-p local-machine-port:internal-container-port`. In this case we are mapping Port 80 in the container to Port 80 on the server
- `nginx` is the name of the image on dockerhub (we  downloaded this before with the pull command, but Docker will do this  automatically if the image is missing)

That's all we need to get Nginx up! Paste the IP address of your  Droplet into a web browser and you should see Nginx's "Welcome to  nginx!" page.

You'll also notice in your shell session that the log for Nginx is  being updated when you make requests to your server, because we're  running our container interactively.

Let's hit the break shortcut `CTRL+C` to get back to our shell session.

If you try to load the page now, you'll get a "connection refused"  page. This is because we shut down our container. We can verify this  with this command:

```
sudo docker ps -a
```

You should see something similar to the output shown below.

```
OutputCONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                      PORTS               NAMES
05012ab02ca1        nginx               "nginx -g 'daemon off"   57 seconds ago      Exited (0) 47 seconds ago                       docker-nginx
```

We can see that our Docker container has exited.

Nginx isn't going to be very useful if we need to be attached to the  container image for it to work, so in the next step we'll show you how  to detach the container to allow it to run independently.

Remove the existing `docker-nginx` container with this command:

```
sudo docker rm docker-nginx
```

In the next step we'll show you how to run it in detached mode.

## Learning How to Run in Detached Mode

Create a new, detached Nginx container with this command:

```
sudo docker run --name docker-nginx -p 80:80 -d nginx
```

We added the `-d` flag to run this container in the background.

The output should simply be the new container's ID.

If we run the list command:

```
sudo docker ps
```

We're going to see a couple things in the output we haven't seen before.

```
OutputCONTAINER ID        IMAGE               COMMAND                  CREATED              STATUS              PORTS                         NAMES
b91f3ce26553        nginx               "nginx -g 'daemon off"   About a minute ago   Up About a minute   0.0.0.0:80->80/tcp, 443/tcp   docker-nginx
```

We can see that instead of `Exited (0) X minutes ago` we now have `Up About a minute`, and we can also see the port mapping.

If we go to our server's IP address again in our browser, we will be  able to see the "Welcome to nginx!" page again. This time it's running  in the background because we specified the `-d` flag, which tells Docker to run this container in detached mode.

Now we have a running instance of Nginx in a detached container!

It's not useful enough yet, though, because we can't edit the config  file, and the container has no access to any of our website files.

Stop the container by running the following command:

```
sudo docker stop docker-nginx
```

Now that the container is stopped (you can check with `sudo docker ps -a` if you want to be sure), we can remove it by running the following command;

```
sudo docker rm docker-nginx
```

Now we'll get to the final version of our container, with a quick stop to generate a custom website file.

## Building a Web Page to Serve on Nginx

In this step, we'll create a custom index page for our website. This  setup allows us to have persistent website content that's hosted outside  of the (transient) container.

Let's create a new directory for our website content within our home  directory, and move to it, by running the commands shown below.

```
mkdir -p ~/docker-nginx/html
cd ~/docker-nginx/html
```

Now let's create an HTML file (we show the commands for Vim, but you can use any text editor you like).

```
vim index.html
```

Enter insert mode by pressing `i`. Paste in the content shown below (or feel free to add your own HTML markup).

```
<html>
  <head>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-MfvZlkHCEqatNoGiOXveE8FIwMzZg4W85qfrfIFBfYc= sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
    <title>Docker nginx Tutorial</title>
  </head>
  <body>
    <div class="container">
      <h1>Hello Digital Ocean</h1>
      <p>This nginx page is brought to you by Docker and Digital Ocean</p>
    </div>
  </body>
</html>
```

If you're familiar with HTML, you'll see that this is a super basic web page. We've included a `<link>`  tag that is pointed to a CDN for Bootstrap (a CSS framework that gives  your web page a collection of responsive styles). You can read more  about [Bootstrap](http://getbootstrap.com/).

We can save this file now by pressing `ESC`, and then `:wq` and `ENTER`:

- write (`w`) tells Vim to write the changes to the file
- quit (`q`) tells Vim to exit

We've now got a simple index page to replace the default Nginx landing page.

## Linking the Container to the Local Filesystem

In this section, we'll put it all together. We'll start our Nginx  container so it's accessible on the Internet over Port 80, and we'll  connect it to our website content on the server.

**Background information about volumes; that is, linking to permanent server content from your container:**

Docker allows us to link directories from our virtual machine's local file system to our containers.

In our case, since we want to server web pages, we need to give our container the files to render.

We could copy the files into the container as part of a Dockerfile,  or copy them into the container after the fact, but both of these  methods leave our website in a static state inside the container. By  using Docker's data volumes feature, we can create a symbolic link  between the Droplet's filesystem and the container's filesystem. This  allows us to edit our existing web page files and add new ones into the  directory and our container will automatically access them. If you want  to read more about Docker and volumes check out the [data volumes documentation](https://docs.docker.com/userguide/dockervolumes/).

The Nginx container is set up by default to look for an index page at `/usr/share/nginx/html`, so in our new Docker container, we need to give it access to our files at that location.

**Making the link:**

To do this, we use the `-v` flag to map a folder from our local machine (`~/docker-nginx/html`) to a relative path in the container (`/usr/share/nginx/html`).

We can accomplish this by running the following command:

```
sudo docker run --name docker-nginx -p 80:80 -d -v ~/docker-nginx/html:/usr/share/nginx/html nginx
```

We can see that the new addition to the command `-v ~/docker-nginx/html:/usr/share/nginx/html` is our volume link.

- `-v` specifies that we're linking a volume
- the part to the left of the `:` is the location of our file/directory on our virtual machine (`~/docker-nginx/html`)
- the part to the right of the `:` is the location that we are linking to in our container (`/usr/share/nginx/html`)

After running that command, if you now point your browser to your  DigitalOcean Droplet's IP address, you should see the first heading of **Hello Digital Ocean** (or whatever web page you created in Step 5).

If you're happy with the other Nginx defaults, you're all set.

You can upload more content to the `~/docker-nginx/html/` directory, and it will be added to your live website.

![](assets/000073-Docker nginx Tutorial.png)

For example, if we modify our index file, and if we reload our  browser window, we will be able to see it update in realtime. We could  build a whole site out of flat HTML files this way if we wanted to. For  example, if we added an `about.html` page, we could access it at `http://your_server_ip/about.html` without needing to interact with the container.

## Using Your Own Nginx Configuration File

This section is for advanced users who want to use their own Nginx  config file with their Nginx container. Please skip this step if you  don't have a custom config file you want to use.

Let's go back a directory so we aren't writing to our public HTML directory:

```
cd ~/docker-nginx
```

If you'd like to take a look at the default config file, just copy it using the Docker copy command:

```
sudo docker cp docker-nginx:/etc/nginx/conf.d/default.conf default.conf
```

Since we're going to be using a custom `.conf` file for Nginx, we will need to rebuild the container.

First stop the container:

```
sudo docker stop docker-nginx
```

Remove it with:

```
sudo docker rm docker-nginx
```

Now you can edit the default file locally (to serve a new directory, or to use a `proxy_pass`  to forward the traffic to another app/container like you would with a  regular Nginx installation). You can read about Nginx's configuration  file in our [Nginx config file guide](https://www.digitalocean.com/community/tutorials/understanding-the-nginx-configuration-file-structure-and-configuration-contexts).

Once you've saved your custom config file, it's time to make the Nginx container. Simply add a second `-v` flag with the appropriate paths to give a fresh Nginx container the appropriate links to run from your own config file.

```
sudo docker run --name docker-nginx -p 80:80 -v ~/docker-nginx/html:/usr/share/nginx/html -v ~/docker-nginx/default.conf:/etc/nginx/conf.d/default.conf -d nginx
```

This command also still links in the custom website pages to the container.

Please note that you will need to restart the container using a `docker restart`  command if you make any changes to your config file after starting the  container, since Nginx does not hot reload if its config file is  changed:

```
sudo docker restart docker-nginx
```

## `Docker compose` and `nginx`

We'll setup a reverse proxy with NGINX, and will setup two applications (one on NGINX and another on apache). 

![nginx-reverse-proxy-pic.png](assets/nginx-reverse-proxy-pic.png) 

Dockerfile - building a reverse proxy image

Here is the **Dockerfile** which will be used to create the reverse proxy image. It will use the **nginx.conf** after copying it to the proxy container:

```
FROM nginx:alpine
 
COPY ./nginx.conf /etc/nginx/nginx.conf
```

 ![](assets/000074-192.168.88.112 (root).png)

Let's build **reverse proxy** image:

```
$ docker build -t reverseproxy .
Sending build context to Docker daemon 4.608 kB
Step 1/2 : FROM nginx:alpine
alpine: Pulling from library/nginx
627beaf3eaaf: Pull complete 
d7d23baed7e7: Pull complete 
7ded693c7a29: Pull complete 
6856facfdc93: Pull complete 
Digest: sha256:ebd2b85803e78100a582385f7eac56cd367561f0f2bab005f784cda95818d41f
Status: Downloaded newer image for nginx:alpine
 ---> 5a35015d93e9
Step 2/2 : COPY nginx.conf /etc/nginx/nginx.conf
 ---> 3e31efad8bda
Removing intermediate container edd9450463ad
Successfully built 3e31efad8bda
```

 We can check the create image:

```
$ docker images
REPOSITORY     TAG      IMAGE ID       CREATED              SIZE
reverseproxy   latest   3e31efad8bda   About a minute ago   15.5 MB
```

 Note that we copied the following **nginx.conf** to our reverse proxy container:

**nginx.conf**:

```
worker_processes 1;
 
events { worker_connections 1024; }
 
http {
 
    sendfile on;
 
    upstream docker-nginx {
        server nginx:80;
    }
 
    upstream docker-apache {
        server apache:80;
    }
 
    server {
        listen 8080;
 
        location / {
            proxy_pass         http://docker-nginx;
            proxy_redirect     off;
        }
    }
 
    server {
        listen 8081;

        location / {
            proxy_pass         http://docker-apache;
            proxy_redirect     off;
        }
    }
}
```

Here is our **docker-compose.yml**:

```
version: "3.3"
services:
    reverseproxy:
        image: reverseproxy
        ports:
            - "8080:8080"
            - "8081:8081"
        restart: always

    nginx:
        depends_on:
            - reverseproxy
        image: nginx:alpine
        restart: always

    apache:
        depends_on:
            - reverseproxy
        image: httpd:alpine
        restart: always
```

 

A **services** definition contains configuration which will be  applied to each container started for that service, much like passing  command-line parameters to **docker run**.

The **depends_on** expresses dependency between services and the **docker-compose up** will start services in dependency order.

 

Let's create and start containers in detached mode (run containers in the background).

```
$ docker-compose up -d
Creating network "proxy_default" with the default driver
Pulling apache (httpd:alpine)...
alpine: Pulling from library/httpd
627beaf3eaaf: Already exists
e225632b13fc: Pull complete
09d704230c42: Pull complete
a1f05d6d2879: Pull complete
f9e9b4770efc: Pull complete
Digest: sha256:2b943ffb79a69deb138af7358c37ceb21ab9e2919fa76f72158c541f17ad76d2
Status: Downloaded newer image for httpd:alpine
Creating proxy_reverseproxy_1
Creating proxy_nginx_1
Creating proxy_apache_1
```

![](assets/000076-192.168.88.112 (root).png)

When complete, we should have three containers deployed, two of which we cannot access directly: 

![](assets/000077-192.168.88.112 (root).png)

We can check our applications (one with NGINX and the other one with apache).

Navigate to http://192.168.88.112:8080/, and this will hit the NGINX reverse proxy which will in turn load the NGINX web application:

  ![](assets/000079-Welcome to nginx!.png)

Then, navigate to http://192.168.88.112:8081/ , the NGINX reverse proxy will be hit and the Apache web application will be loaded:

 ![](assets/000080-Photos.png)

![](assets/000081-192.168.88.112 (root).png)

Here is another example for a reverse proxy. We'll have two servers (site1 and site2) behind the proxy:

Source here: https://github.com/Einsteinish/Docker-compose-Nginx-Reverse-Proxy-II.git

 ![tree-reverse](assets/tree-reverse.png)

```
$ cd site1
~/site1$ docker-compose build
~/site1$ docker-compose up -d
Recreating site1_app_1

~/site1$ cd ../site2
~/site2$ docker-compose build
~/site2$ docker-compose up -d
Recreating site2_app_1

~/site2$ cd ../reverse-proxy/
~/reverse-proxy$ docker-compose build
Building proxy
Step 1/4 : FROM nginx:1.12
 ---> 92e8f1b61b09
Step 2/4 : COPY ./default.conf /etc/nginx/conf.d/default.conf
 ---> Using cache
 ---> 126ba809f42c
Step 3/4 : COPY ./backend-not-found.html /var/www/html/backend-not-found.html
 ---> Using cache
 ---> 3fd66191c774
Step 4/4 : COPY ./includes/ /etc/nginx/includes/
 ---> Using cache
 ---> d70a589d357b
Successfully built d70a589d357b

~/reverse-proxy$ docker-compose up -d
Creating reverseproxy_proxy_1

$ docker ps
CONTAINER ID   IMAGE               COMMAND                 CREATED            STATUS             PORTS                NAMES
09d9423c758b   reverseproxy_proxy  "nginx -g 'daemon ..."  5 seconds ago      Up 2 seconds       0.0.0.0:80->80/tcp   reverseproxy_proxy_1
68dc3ecf49ae   nginx:1.12          "nginx -g 'daemon ..."  49 seconds ago     Up 47 seconds      80/tcp               site2_app_1
b5c7e602f9ec   nginx:1.12          "nginx -g 'daemon ..."  About a minute ago Up About a minute  80/tcp               site1_app_1
```

 ![](assets/000082-192.168.88.112 (root).png)



![site1-example-com](assets/site1-example-com.png)

 

![site2-example-com](assets/site2-example-com.png)

  Note that we need to set the domains in /etc/hosts using ip of host machine:

```
192.168.88.112 site1.example.com
192.168.88.112 site2.example.com
```

 

The **docker-compose.yml** files used are:

**reverse-proxy/docker-compose.yml**:

```
FROM nginx:1.12

#  default conf for proxy service
COPY ./default.conf /etc/nginx/conf.d/default.conf

# NOT FOUND response
COPY ./backend-not-found.html /var/www/html/backend-not-found.html

# Proxy configurations
COPY ./includes/ /etc/nginx/includes/
```

 

**site1/docker-compose.yml**:

```
version: '3'
services:
  app:
    image: nginx:1.12
    volumes:
      - .:/usr/share/nginx/html/
    expose:
      - "80"
```

 

**site2/docker-compose.yml**:

```
version: '3'
services:
  app:
    image: nginx:1.12
    volumes:
      - .:/usr/share/nginx/html/
    expose:
      - "80"
```



## Reference

- https://www.nginx.com/resources/glossary/nginx/
- https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-centos-7
- https://www.digitalocean.com/community/tutorials/how-to-install-linux-nginx-mysql-php-lemp-stack-on-centos-7
- https://www.hostinger.com/tutorials/how-to-install-lemp-centos7#Step-2-Installing-MySQL-MariaDB
- https://thachpham.com/linux-webserver/nginx-reverse-proxy-cho-apache.html
- https://www.upcloud.com/support/how-to-set-up-load-balancing/
- https://stackoverflow.com/questions/1706111/where-can-i-find-the-error-logs-of-nginx-using-fastcgi-and-django
- https://www.nginx.com/products/nginx/load-balancing/#session-persistence
- https://www.techrepublic.com/article/how-to-run-nginx-as-a-docker-container/
- http://www.bogotobogo.com/DevOps/Docker/Docker-Compose-Nginx-Reverse-Proxy-Multiple-Containers.php

  