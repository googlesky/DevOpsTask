Install Jenkins on CentOS 7:
----------------------------

### Prerequisites

Set proxy in /etc/environment:

```
http_proxy="http://10.10.10.10:8080/"
https_proxy="https://10.10.10.10:8080/"
ftp_proxy="ftp://10.10.10.10:8080/"
```

Set proxy for yum in /etc/yum.conf:

```
proxy=http://10.10.10.10:8080
```



**Step 1: Update your CentOS 7 system**

One of the Linux system administrator's best practices is keeping a system up to date. Install the latest stable packages, then reboot.

```
sudo yum install epel-release
sudo yum update
sudo reboot
```

When the reboot finishes, login with the same `sudo` user.

**Step 2: Install Java**

Before you can install Jenkins, you need to setup a Java virtual machine on your system. Here, let's install the latest `OpenJDK Runtime` Environment 1.8.0 using YUM:

```
sudo yum install java-1.8.0-openjdk.x86_64

```

After the installation, you can confirm it by running the following command:

```
java -version
```

This command will tell you about the Java runtime environment that you have installed:
```
openjdk version "1.8.0_91"
OpenJDK Runtime Environment (build 1.8.0_91-b14)
OpenJDK 64-Bit Server VM (build 25.91-b14, mixed mode)
```
In order to help Java-based applications locate the Java virtual machine properly, you need to set two environment variables: `JAVA_HOME` and `JRE_HOME`.

```
sudo cp /etc/profile /etc/profile_backup
echo 'export JAVA_HOME=/usr/lib/jvm/jre-1.8.0-openjdk' | sudo tee -a /etc/profile
echo 'export JRE_HOME=/usr/lib/jvm/jre' | sudo tee -a /etc/profile
source /etc/profile
```
Finally, you can print them for review:

```
echo \$JAVA_HOME
echo \$JRE_HOME
```

**Step 3: Install Jenkins**

The most recent version of Jenkins at any given time is available on the [Jenkin's mirror](http://mirrors.jenkins-ci.org/war/latest/jenkins.war). You can use any tool you like to download this file. The following method employs a command line tool called wget:
```
wget http://mirrors.jenkins-ci.org/war/latest/jenkins.war
```
When you're ready, start Jenkins via Java:
```
java -jar jenkins.war
```
You should see output in the console indicating that Jenkins has started running:

```
...

INFO: Jenkins is fully up and running

...
```
**Step 4.1: Running Jenkins as a Service**

```
mkdir /opt/jenkins
sudo cp -R jenkins.war /opt/jenkins/
cd /etc/systemd/system/
sudo nano jenkins.service
```
Now, add the following lines to the new `jenkins.service` file. In a moment, we'll go over exactly what these lines accomplish.

```
[Unit]
Description=Jenkins Service
After=network.target

[Service]
Type=simple
User=root
Environment=JENKINS_HOME=/opt/jenkins/
ExecStart=/usr/bin/java -jar /opt/jenkins/jenkins.war
Restart=on-abort

[Install]
WantedBy=multi-user.target
```
Run
```bash
sudo systemctl daemon-reload
sudo systemctl start jenkins.service
```



**Step 4.1: Running Jenkins by Shell**

Write a `start_jenkins.sh` file follow:

```bash
#!/bin/bash

cd /opt/jenkins
export JENKINS_HOME=/opt/jenkins
nohup java -jar jenkins.war --httpPort=8888 2>&1 jenkins.log &
```

Allow `start_jenkins.sh` to run

```
chmod +x start_jenkins.sh
```

now you can run `start_jenkins.sh` normally

**Step 5: Change port Jenkins Server**

Now, change jenkins.service file.

```
[Unit]
Description=Jenkins Service
After=network.target

[Service]
Type=simple
User=root
Environment=JENKINS_HOME=/opt/jenkins/
ExecStart=/usr/bin/java -jar /opt/jenkins/jenkins.war --httpPort=8888
Restart=on-abort

[Install]
WantedBy=multi-user.target
```

Run

```
sudo systemctl daemon-reload
sudo systemctl restart jenkins.service
```



Install plugins.
----------------

Get password form `/var/lib/jenkins/secrets/initialAdminPassword`

![](./images/image1.png)

Go to `Admin CP`

![](./images/image2.png)

Set `proxy`.

Install Plugins for Jenkins by go to **Manage Jenkins** -\> **Plugin Manager** -\> **Available**

![](./images/image3.png)

Set http_proxy
---------------

Go to **Manage Jenkins** -\> **Plugin Manager** -\> **Advanced**
Set `Proxy` follow this image:
![](./images/image4.png)

Clone the code from https://github.com/TechPrimers/jenkins-example to your VM.
------------------------------------------------------------------------------

Install `git` by command:
```
yum install git
```
Clone <https://github.com/TechPrimers/jenkins-example> to VM:

![](./images/image5.png)

## **Create SSH Key and add to gitlab:**

Before generating a new SSH key pair check if your system already has one at the default location by opening a shell, or Command Prompt on Windows, and running the following command:

**Windows Command Prompt:**

```
type %userprofile%\.ssh\id_rsa.pub
```

**Git Bash on Windows / GNU/Linux / macOS / PowerShell:**

```
cat ~/.ssh/id_rsa.pub
```

If you see a string starting with `ssh-rsa` you already have an SSH key pair and you can skip the generate portion of the next section and skip to the copy to clipboard step. If you don't see the string or would like to generate a SSH key pair with a custom name continue onto the next step.

Note that Public SSH key may also be named as follows:

- `id_dsa.pub`
- `id_ecdsa.pub`
- `id_ed25519.pub`

To generate a new SSH key pair, use the following command:

**Git Bash on Windows / GNU/Linux / macOS:**

```
ssh-keygen -t rsa -C "your.email@example.com" -b 4096
```

**Windows:**

Alternatively on Windows you can download [PuttyGen](http://www.chiark.greenend.org.uk/%7Esgtatham/putty/download.html) and follow [this documentation article](https://the.earth.li/%7Esgtatham/putty/0.67/htmldoc/Chapter8.html#pubkey-puttygen) to generate a SSH key pair.

The next step is to copy the public SSH key as we will need it afterwards.

To copy your public SSH key to the clipboard, use the appropriate code below:

**macOS:**

```
pbcopy < ~/.ssh/id_rsa.pub
```

**GNU/Linux (requires the xclip package):**

```
xclip -sel clip < ~/.ssh/id_rsa.pub
```

**Windows Command Line:**

```
type %userprofile%\.ssh\id_rsa.pub | clip
```

**Git Bash on Windows / Windows PowerShell:**

```
cat ~/.ssh/id_rsa.pub | clip
```

The final step is to add your public SSH key to GitLab. 

Once you do that, login to GitLab with your credentials.

On the upper right corner, click on your avatar and go to your **Profile settings**.

[![Profile settings dropdown](assets/profile_settings.png)](https://docs.gitlab.com/ee/gitlab-basics/img/profile_settings.png)

Navigate to the **SSH keys** tab.

[![SSH Keys](assets/profile_settings_ssh_keys.png)](https://docs.gitlab.com/ee/gitlab-basics/img/profile_settings_ssh_keys.png)

Paste your **public** key that you generated in the first step in the 'Key' box.

[![Paste SSH public key](assets/profile_settings_ssh_keys_paste_pub.png)](https://docs.gitlab.com/ee/gitlab-basics/img/profile_settings_ssh_keys_paste_pub.png)

Optionally, give it a descriptive title so that you can recognize it in the event you add multiple keys.

[![SSH key title](assets/profile_settings_ssh_keys_title.png)](https://docs.gitlab.com/ee/gitlab-basics/img/profile_settings_ssh_keys_title.png)

Finally, click on **Add key** to add it to GitLab. You will be able to see its fingerprint, its title and creation date.

[![SSH key single page](assets/profile_settings_ssh_keys_single_key.png)](https://docs.gitlab.com/ee/gitlab-basics/img/profile_settings_ssh_keys_single_key.png)

## Push the code to http://192.168.88.134 gitlab server

Create new project from gitlab `192.168.88.134`

![](./images/image6.png)

cd to `jenkins-example`
Run `rm -rf .git` to remove github information.

Run follow step by step

```
git config --global user.name "Hieu Le Phuong"
git config --global user.email lphieu@tma.com.vn
git init
git remote add origin http://lphieu@192.168.88.134/lphieu/jenkins-example.git
git add .
git commit -m "Initial commit"
git push -u origin master
```




Edit JenkinsFile to remove stage ('Deployment Stage').
--------------------------------------------------------

Run 

```
nano Jenkinsfile
```

In `jenkins-example`

Edit to remove stage ('Deployment Stage')

![](./images/image8.png)

Submit code again.
------------------

Run 

```
git add .
```

Run 

```
git commit -m "Edit Jenkinsfile 23.05.2018 - Remove 'Deployment Stage'"
```

![](./images/image9.png)

Submit code success.

Create a pipeline project that use this JenkinsFile.
----------------------------------------------------

Create pipeline

![](./images/image10.png)

Config follow this image:

![](./images/image11.png)

Click Add to add more Credentials

![](./images/image12.png)

Add ssh key:

![](./images/image13.png)

It will work.

With JenkinsFile, create a function to send email, and call it when build failed.
---------------------------------------------------------------------------------

Config `smtp` server from `config system`

![](./images/image14.png)

I use follow pipeline:

```groovy
emailNotifications = 'lphieu@tma.com.vn'
notificationSent    = false

def sendNotification(buildChanged)
{
    if (notificationSent)
    {
        return
    }
    notificationSent = true

    if (currentBuild.currentResult == 'SUCCESS')
    {
        // notify users when the build is back to normal
        mail to: emailNotifications,
            subject: "Build fixed: ${currentBuild.fullDisplayName}",
            body: "The build is back to normal ${env.BUILD_URL}"
    }
    else if ((currentBuild.currentResult == 'FAILURE') && buildChanged)
    {
        // notify users when the Pipeline first fails
        mail to: emailNotifications,
            subject: "Build failed: ${currentBuild.fullDisplayName}",
            body: "Something went wrong with ${env.BUILD_URL}"
    }
    else if ((currentBuild.currentResult == 'FAILURE'))
    {
        // notify users when they check into a broken build
        mail to: emailNotifications,
            subject: "Build failed (again): ${currentBuild.fullDisplayName}",
            body: "Something is still wrong with ${env.BUILD_URL}"
    }
}

pipeline {
    agent any
    stages {
        stage ('Compile Stage') {
            steps {
                withMaven(maven : 'maven_3_5_3') {
                    sh 'mvn clean compile'
                }
            }
        }
        stage ('Testing Stage') {

            steps {
                withMaven(maven : 'maven_3_5_3') {
                    sh 'mvn test'
                }
            }
        }
    }

    post {
        changed {
            sendNotification buildChanged:true
        }
        failure {
            sendNotification buildChanged:false
        }
    }
}
```

Result

![](./images/image15.png)

But I got a error:

![](./images/image16.png)

I think I must do something with proxy. Ex: add proxy to maven

![](./images/image17.png)

I was write a `setting.xml` and use by command:

```bash
mvn clean install -s settings.xml
```

but still not work. I try to fix problem with this tut

<https://techjourney.net/update-add-ca-certificates-bundle-in-redhat-centos/>

but still not work.

But I found this tutorial:

<https://maven.apache.org/guides/mini/guide-proxies.html>

And it work.

![](./images/image18.png)

![](./images/image19.png)

But I got other error:

![](./images/image20.png)

Maven thinks we don't have Environment. So I set it:

```bash
export JAVA_HOME=/usr/bin/java
```

test it but I think I need to restart `Jenkins` but not work. I will try
to install JDK because I try `javac` but not work.

So I install jdk by command:

```bash
sudo yum install java-1.8.0-openjdk-devel -y
```

And It work.

![](./images/image21.png)

But I saw `JAVA_HOME` not correct so I will try to fix it by this way:

I found `openjdk` install `JDK` in `/usr/lib/jvm/java-1.8.0-openjdk/bin` so I edit `/etc/profile` and add:

![](./images/image22.png)

Run source `/etc/profile` and run Jenkins and project again

![](./images/image23.png)

Look I pass the error. But result is `FAILED` and I knew problem is maven was not work with `Proxy`.

![](./images/image24.png)

I found maven home dir is `~/.m2` so I copy `settings.xml` to that dir and
try again.

![](./images/image25.png)

And it works like charm.

![](./images/image26.png)

Reference
=========

- <https://coreos.com/os/docs/latest/using-environment-variables-in-systemd-units.html>

- <https://www.digitalocean.com/community/tutorials/how-to-set-up-jenkins-for-continuous-development-integration-on-centos-7>

- <https://medium.com/@teeks99/continuous-integration-with-jenkins-and-gitlab-fa770c62e88a>

- <https://stackoverflow.com/questions/15495843/gitlab-git-user-password>

- <https://baptiste-wicht.com/posts/2017/06/jenkins-tip-send-notifications-fixed-builds-declarative-pipeline.html>

- <https://www.liquidweb.com/kb/how-to-stop-and-disable-firewalld-on-centos-7/>

- <https://docs.gitlab.com/ee/ssh/README.html>

- <https://www.digitalocean.com/community/tutorials/how-to-install-java-on-centos-and-fedora>
- https://docs.gitlab.com/ee/ssh/README.html
- https://docs.gitlab.com/ee/gitlab-basics/create-your-ssh-keys.html