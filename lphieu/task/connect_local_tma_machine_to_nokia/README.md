# Connect Local Machine TMA to Nokia

## Connect to Gerrit

I tried to clone full repository of git Gerrit with no success because:

- Much time.
- Slow internet from TMA to Nokia.

I tried to use a trick I used before. I saw some similarities between the repository of VitalSuite in TMA and Nokia and I tried:

Clone repository of TMA (I had before so I just remove `.git` folder and reuse):

```bash
git init 
git remote add git@192.168.88.134:bahuy/main_dev.git
git pull
```

![1529978684476](assets/1529978684476.png)

Result always was success.

I tried to change remote url from Gitlab of TMA to Gerrit of Nokia:

```bash
git remote set-url origin https://gerrit.ext.net.nokia.com/gerrit/VitalSuite/main_dev
```

and pull again

![1529978978550](assets/1529978978550.png)

The result was perfect. VitalSuite repository in Gitlab TMA and Gerrit Nokia was same.

The result of `git diff`:

![1529979206739](assets/1529979206739.png)

Only one file different. 

## Connect to VLAB from browser of local machine of TMA

I used this script:

```javascript
function FindProxyForURL(url, host) {
        var goDirect = isPlainHostName(host) || dnsDomainIs(host, ".tma.com.vn") || isInNet(host, "192.168.0.0", "255.255.0.0")
                || dnsDomainIs(host, ".skype.com") || dnsDomainIs(host, ".skypeassets.com") || dnsDomainIs(host, ".live.com") || dnsDomainIs(host, ".google.com") || dnsDomainIs(host, ".google.com.vn")
                || dnsDomainIs(host, ".facebook.com") || dnsDomainIs(host, ".fbcdn.net")
                || isInNet(host, "13.229.88.0", "255.255.0.0") || isInNet(host, "135.220.0.0", "255.255.0.0") || isInNet(host, "135.3.0.0", "255.255.0.0")
                || dnsDomainIs(host, ".vsdoc");

        var useSOCK = dnsDomainIs(host, ".alcatel-lucent.com") || isInNet(host, "10.75.197.0", "255.255.255.0") || dnsDomainIs(host, ".lucent.com") || dnsDomainIs(host, "net.nokia.com") || dnsDomainIs(host, "inside.nsn.com");

        if (goDirect) {
                return "DIRECT; PROXY proxy.tma.com.vn:8080;";
        } else if (useSOCK){
                return "SOCKS5 192.168.88.57:8000";
        } else {
                return "PROXY proxy.tma.com.vn:8080; DIRECT";
        }
}
```

