# CREATE LINUX JAVA DOCKER IMAGE

## Modify builder 

script build 

```shell
#!/bin/ksh -x
# bldvnunix.sh - compile and build image if no error

if [ $# -lt 4 ];then
  echo "Usage: $0 [release|debug] x.y.z.w [weekly|daily] branch-name [clean|incr]"
  echo ""
  echo "main_dev weekly image is in /VNarchive, daily image in /VNarchive/daily."
  echo "Feature branch images are always in /VNarchive/branchname no matter weekly or not."
  echo "ftp happens no matter weekly or daily. But only weekly sends email after ftp. "
  echo "Weekly build sends email when succeeds or fails. Daily build only sends email when fails."
  echo "5th parameter is optional. If it is not clean or blank, it is incremental build."
  echo ""
  exit
fi
if [ "$1" != "release" ] && [ "$1" != "debug" ]; then
  echo "\n1st parameter must be either release or debug\n"
  exit
fi
echo $2 | grep "\." > /dev/null
if [ $? -ne 0 ];then                     # not in x.y.z.w format
  echo "\n2nd parameter must be build number in x.y.z.w format\n"
  exit
fi
mariadbName=_mariadb;
source $(dirname ${.sh.file})/../svn_migration/set_build_env_mariadb.sh
PATH=/opt/ActivePython-2.7/bin:/opt/sun/sunstudio12.1/bin:/bin:/usr/sbin:/usr/bin:/usr/ucb:/usr/local/bin:/usr/ccs/bin:/opt/csw/bin:
LM_LICENSE_FILE=/etc/opt/licenses/licenses_combined
export PATH LM_LICENSE_FILE
DBTYPE=mariadb;export DBTYPE;
umask 02

VN_ARCHIVE=/VNarchive
VV_ARCHIVE=/VVarchive

[ -z "$BuildART" ] && BuildART=1

today=`date`
VS=`/usr/bin/dirname $0`       # if run with no full path, VS will be .
echo "running $0 in $VS"
echo "PATH is $PATH"
NETWORKNAME=`uname -n`
SITE=TMA
if [ "$NETWORKNAME" = "VSBUILDLINUX_MARIADB" ]; then
	SITE=TMA
fi
if [ "$NETWORKNAME" = "usnavsvtlb01" ]; then
	SITE=NDC
fi
export SITE
echo "SITE is $SITE"

if [ "$3" = "weekly" ]; then
  # mailto="vitalsuite@tma.com.vn vipul.dhanak@nokia.com"
  mailto="lphieu@tma.com.vn"
else
	# mailto="vitalsuite@tma.com.vn"
  mailto="lphieu@tma.com.vn"
	if [ "$SITE" = "NDC" ]; then
		# mailto="vitalsuite@tma.com.vn vipul.dhanak@nokia.com"
    mailto="lphieu@tma.com.vn"
	fi
fi

cd $VS
VS=`pwd`                       # to get full path when run without full path


#build jar packages
if [ $JAVA_HOME ]; then
	echo "Use JAVAHOME = $JAVA_HOME"
else
	JAVA_HOME=/opt/jdk8/jdk1.8.0_77; export JAVA_HOME
fi

PATH=/opt/gradle/latest/bin:$PATH:${BR_ROOT}/noscm/ant/bin
export PATH

sh bldjavacommon.sh 1> bldjava_${postfix} 2>&1
sh bldvitalflow.sh 1>> bldjava_${postfix} 2>&1
sh bldjavabda.sh 1>> bldjava_${postfix} 2>&1

if [ $BuildART -eq 1 ]; then
  #The following is for VitalART
  AXIS_HOME=${BR_ROOT}/thdparty/Axis/Axis-1.2.1; export AXIS_HOME
  cd $VS/licensing/jnivslicense
  [ "$5" = "clean" ] && make cfg=$cfg clean >> clean_${postfix} 2>&1
  make cfg=$cfg -f Makefile 1> $VS/vv_${postfix} 2>&1
  cd $VS
  sh bldvitalart.sh 1>> bldjava_${postfix} 2>&1
  cd $VS
fi

rm -f $summary
for fn in vsroles others buildlibs all bldjava
do
 egrep "Error |fatal:|No rule to make|BUILD FAILED" ${fn}_${postfix} > /dev/null
 if [ $? -eq 0 ];then
#    echo "********************************" >> $summary
#    echo "*  Output from make $fn" >> $summary
#    echo "********************************" >> $summary
   cat ${fn}_${postfix} >> $summary
   echo "" >> $summary
 fi
done

count=0
if [ -s "$summary" ]; then
 postunixbld.pl VN $1 $summary $mailfn
 count=`grep "^------------" $mailfn | wc -l | awk '{print $1}'`
fi
echo "\n$cfg compilation finished with $count error\n"

```

Build command:

```shell
time vs/bldvnlinux_mariadb.sh release 69.69.69.69 daily main_dev linux_only 2>&1 | tee my_build.log
```

Rename network card from `ens32` to `eth0` for mailing because `mail.pl` use `eth0` for mailing:

```shell
/sbin/ip link set ens32 down
/sbin/ip link set ens32 name eth0
/sbin/ip link set eth0 up
```

Result with ` Linux-MariaDB No error(s)`

![1528876838837](assets/1528876838837.png)

Build log:

```
/opt/svn-vs/main_dev/vs/bldjava_
```

Ant file:

```
/opt/svn-vs/main_dev/corecom/api/translations/buildunix.xml
```

Ant Build:

```
/opt/svn-vs/main_dev/vs/build/ant-build.xml
```

Tomcat HOME:

```
/opt/svn-vs/main_dev/vs/thdparty/Apache_Tomcat_4build/linux
```

AXIS HOME:

```
/opt/svn-vs/main_dev/vs/thdparty/Axis
```

JAVACC:

```
/opt/svn-vs/main_dev/vs/thdparty/javacc
```

## Build docker image

Pull image from `Artifactory`

```
docker pull docker.repo.lab.pl.alcatel-lucent.com/aado/build_rhel7
```

tag docker image a name

```
docker tag 6ccecb52b589 npmvs_rh7_jdk8_gradle
```

Run docker image 

```
docker run -p 2227:22 --name=npmvs_rh7_jdk8_gradle -d npmvs_rh7_jdk8_gradle
```

Run docker image bash

```
docker exec -it npmvs_rh7_jdk8_gradle bash
```

Exit bash by run `exit` and copy JDK 8 from host to Docker container:

```
docker cp /opt/jdk8 npmvs_rh7_jdk8_gradle:/opt/jdk8
```

Check result in container 

![1528957880477](assets/1528957880477.png)

```
[root@huy-migration jdk8]# docker ps
CONTAINER ID        IMAGE                     COMMAND                  CREATED              STATUS              PORTS                  NAMES
b19de3630a1a        npmvs_rh7_jdk8_gradle   "/bin/sh -c '/usr/..."   About a minute ago   Up About a minute   0.0.0.0:2227->22/tcp   npmvs_rh7_jdk8_gradle
```

Commit change 

```
docker commit b19de3630a1a docker.repo.lab.pl.alcatel-lucent.com/aado/npmvs_rh7_jdk8_gradle
```

Build a docker image from docker file:

```dockerfile
FROM repo.lab.pl.alcatel-lucent.com:6555/aado/build_rhel7:latest-release
LABEL MAINTAINER='AA-DevOps'

ENV ARTIFACTORY_HTTPS_URL=http://repo.lab.pl.alcatel-lucent.com \
    GRADLE2_VERSION=2.9 \
    GRADLE2_SHA1=a054590a287a94de55ee646de186bc57b2dfffbb \
    GRADLE3_VERSION=3.5 \
    GRADLE3_SHA1=b29ccc5be25f0446183edc4f144673934bd7bb7e \
    GRADLE4_VERSION=4.1 \
    GRADLE4_SHA1=ab0cb2dd7a649fbf47b8731cb4dc531b4eb3bce5

ENV JDK_SOURCE=jdk-8u121-linux-x64.rpm
ENV JDK_VERSION=jdk1.8.0_121

RUN whoami
RUN cd /opt/

RUN yum clean all; yum -y install yum-plugin-ovl; yum update -y; yum -y install rpm-build \
         devtoolset-7 \
         libselinux-devel \
         pam-devel \
         git \
         ksh

# Install Java 8 from Nokia
RUN cd /tmp/
RUN wget $ARTIFACTORY_HTTPS_URL/oracle_jdk/$JDK_SOURCE
RUN yum -y localinstall $JDK_SOURCE
RUN rm -rf /tmp/$JDK_SOURCE
ENV JAVA_HOME=/usr/java/$JDK_VERSION


# Copy resource files over
COPY 'files/gradle-install.sh' /tmp/

# Install Gradle
WORKDIR /tmp
RUN chmod 755 gradle-install.sh && \
    ./gradle-install.sh $ARTIFACTORY_HTTPS_URL $GRADLE4_VERSION $GRADLE4_SHA1 && \
    ./gradle-install.sh $ARTIFACTORY_HTTPS_URL $GRADLE3_VERSION $GRADLE3_SHA1 && \
    ./gradle-install.sh $ARTIFACTORY_HTTPS_URL $GRADLE2_VERSION $GRADLE2_SHA1 && \
    rm /tmp/gradle-install.sh && \
    ln -s /usr/local/bin/gradle4 /usr/local/bin/gradle && \
    chown -R jenkins:jenkins /usr/local/bin/gradle

# Force gradle initialization
USER jenkins
RUN gradle -v

# Configure repo mirror to point to Artifactory
WORKDIR /home/jenkins/.gradle/
COPY files/*.gradle /home/jenkins/.gradle/
# init ./gradle/cahce/<vesion>
RUN gradle2 --init-script init.gradle -q showRepos && \
    gradle3 --init-script init.gradle -q showRepos && \
    gradle --init-script init.gradle -q showRepos && \
    rm /home/jenkins/.gradle/build.gradle

# Login message
USER root
RUN echo "$(gradle -v) ($(date --rfc-3339=seconds))" >> /etc/motd && \
    echo "$(gradle2 -v) ($(date --rfc-3339=seconds))" >> /etc/motd && \
    echo "$(gradle3 -v) ($(date --rfc-3339=seconds))" >> /etc/motd && \
    echo "===============================================================" >> /etc/motd

USER root
RUN yum -y clean all && \
    echo 'PATH=/opt/rh/devtoolset-7/root/usr/bin:$PATH' >> /home/jenkins/.bashrc && \
    echo PCP_DIR=/opt/rh/devtoolset-7/root >> /home/jenkins/.bashrc && \
    echo 'rpmlibdir=$(rpm --eval "%{_libdir}")' >> /home/jenkins/.bashrc && \
    echo 'if [ "$rpmlibdir" != "${rpmlibdir/lib64/}" ]; then' >> /home/jenkins/.bashrc && \
    echo 'rpmlibdir32=":/opt/rh/devtoolset-7/root${rpmlibdir/lib64/lib}"' >> /home/jenkins/.bashrc && \
    echo fi >> /home/jenkins/.bashrc && \
    echo 'export LD_LIBRARY_PATH=/opt/rh/devtoolset-7/root$rpmlibdir$rpmlibdir32${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}' >> /home/jenkins/.bashrc

RUN echo "$(/opt/rh/devtoolset-7/root/usr/bin/gcc --version)" >> /etc/motd && \
    echo "$(java -jar /usr/local/lib/sonar-scanner-cli-2.9.0.670.jar --version) ($(date --rfc-3339=seconds))" >> /etc/motd && \
    echo "===============================================================" >> /etc/motd

RUN cp /home/jenkins/.bashrc /root/.bashrc

WORKDIR /opt/main_dev/
RUN env

```



Build image

```
docker build -t npmvs_rh7_jdk8_gradle:1.2 .
```

> NOTE: You much removed all docker container use port 22 before do next step

Run image and mount `/opt/main_dev` to `/opt/main-dev` in container

```
docker run -p 2227:22 --name=npmvs_rh7_jdk8_gradle -d -v /opt/main_dev/:/opt/main_dev npmvs_rh7_jdk8_gradle:1.2
```

## Upload docker image to Artifactory

Create a repository on Gerrit:

![1529486657053](assets/1529486657053.png)

Link: https://gerrit.ext.net.nokia.com/gerrit/#/admin/projects/VitalSuite/npmvs_build_java

Clone this repository: https://gerrit.ext.net.nokia.com/gerrit/gitweb?p=AADO/docker/tools.git

Change Dockerfile:

```dockerfile
FROM repo.lab.pl.alcatel-lucent.com:6555/aado/build_rhel7:latest-release
LABEL MAINTAINER='AA-DevOps'

ENV ARTIFACTORY_HTTPS_URL=http://repo.lab.pl.alcatel-lucent.com \
    GRADLE2_VERSION=2.9 \
    GRADLE2_SHA1=a054590a287a94de55ee646de186bc57b2dfffbb \
    GRADLE3_VERSION=3.5 \
    GRADLE3_SHA1=b29ccc5be25f0446183edc4f144673934bd7bb7e \
    GRADLE4_VERSION=4.1 \
    GRADLE4_SHA1=ab0cb2dd7a649fbf47b8731cb4dc531b4eb3bce5

ENV JDK_SOURCE=jdk-8u121-linux-x64.rpm
ENV JDK_VERSION=jdk1.8.0_121

RUN whoami
RUN cd /opt/

RUN yum clean all; yum -y install yum-plugin-ovl; yum update -y; yum -y install rpm-build \
         devtoolset-7 \
         libselinux-devel \
         pam-devel \
         git \
         ksh

# Install Java 8 from Nokia
RUN cd /tmp/
RUN wget $ARTIFACTORY_HTTPS_URL/oracle_jdk/$JDK_SOURCE
RUN yum -y localinstall $JDK_SOURCE
RUN rm -rf /tmp/$JDK_SOURCE
ENV JAVA_HOME=/usr/java/$JDK_VERSION


# Copy resource files over
COPY 'files/gradle-install.sh' /tmp/

# Install Gradle
WORKDIR /tmp
RUN chmod 755 gradle-install.sh && \
    ./gradle-install.sh $ARTIFACTORY_HTTPS_URL $GRADLE4_VERSION $GRADLE4_SHA1 && \
    ./gradle-install.sh $ARTIFACTORY_HTTPS_URL $GRADLE3_VERSION $GRADLE3_SHA1 && \
    ./gradle-install.sh $ARTIFACTORY_HTTPS_URL $GRADLE2_VERSION $GRADLE2_SHA1 && \
    rm /tmp/gradle-install.sh && \
    ln -s /usr/local/bin/gradle4 /usr/local/bin/gradle && \
    chown -R jenkins:jenkins /usr/local/bin/gradle

# Force gradle initialization
USER jenkins
RUN gradle -v

# Configure repo mirror to point to Artifactory
WORKDIR /home/jenkins/.gradle/
COPY files/*.gradle /home/jenkins/.gradle/
# init ./gradle/cahce/<vesion>
RUN gradle2 --init-script init.gradle -q showRepos && \
    gradle3 --init-script init.gradle -q showRepos && \
    gradle --init-script init.gradle -q showRepos && \
    rm /home/jenkins/.gradle/build.gradle

# Login message
USER root
RUN echo "$(gradle -v) ($(date --rfc-3339=seconds))" >> /etc/motd && \
    echo "$(gradle2 -v) ($(date --rfc-3339=seconds))" >> /etc/motd && \
    echo "$(gradle3 -v) ($(date --rfc-3339=seconds))" >> /etc/motd && \
    echo "===============================================================" >> /etc/motd

USER root
RUN yum -y clean all && \
    echo 'PATH=/opt/rh/devtoolset-7/root/usr/bin:$PATH' >> /home/jenkins/.bashrc && \
    echo PCP_DIR=/opt/rh/devtoolset-7/root >> /home/jenkins/.bashrc && \
    echo 'rpmlibdir=$(rpm --eval "%{_libdir}")' >> /home/jenkins/.bashrc && \
    echo 'if [ "$rpmlibdir" != "${rpmlibdir/lib64/}" ]; then' >> /home/jenkins/.bashrc && \
    echo 'rpmlibdir32=":/opt/rh/devtoolset-7/root${rpmlibdir/lib64/lib}"' >> /home/jenkins/.bashrc && \
    echo fi >> /home/jenkins/.bashrc && \
    echo 'export LD_LIBRARY_PATH=/opt/rh/devtoolset-7/root$rpmlibdir$rpmlibdir32${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}' >> /home/jenkins/.bashrc

RUN echo "$(/opt/rh/devtoolset-7/root/usr/bin/gcc --version)" >> /etc/motd && \
    echo "$(java -jar /usr/local/lib/sonar-scanner-cli-2.9.0.670.jar --version) ($(date --rfc-3339=seconds))" >> /etc/motd && \
    echo "===============================================================" >> /etc/motd

RUN cp /home/jenkins/.bashrc /root/.bashrc

WORKDIR /opt/main_dev/
RUN env

```

Change Jenkinsfile:

```groovy
#!/usr/bin/env groovy

pipeline {
  agent { label 'k8s-dind' }
  environment {
    DOCKER_IMAGE = "npmvs_rh7_jdk8_gradle"
    SOURCE_REPO = "npmvs-docker-candidates"
    TARGET_REPO = "npmvs-docker-releases"
    tag_name = "${Calendar.getInstance()format('yyyyMMddHHmmss')}"
    latest_tag_name = "latest"
    jfrog_cli_version = "1.14.0"
    docker_cli_version = "17.11.0"
    EXTRA_BUILD_ARGS = "--no-cache"
    BUILDID= "${env.BUILD_ID}"
  }
  parameters {
     booleanParam(name: 'DEBUG_BUILD', defaultValue: false, description: 'Set true to retain the workspace after the job execution.') //TODO: when set true, don't delete workspace and keep containers running
  }
  triggers {
    gerrit(customUrl: '',
      gerritProjects: [[
        branches: [[
          compareType: 'PLAIN',
          pattern: 'master'
        ]],
        compareType: 'PLAIN',
        disableStrictForbiddenFileVerification: false,
        pattern: 'npmvs/docker/npmvs_rh7_jdk8_gradle'
      ]],
      serverName: 'nokia-gerrit',
      triggerOnEvents: [
        changeMerged(),
        patchsetCreated(
          excludeDrafts: true,
          excludeNoCodeChange: false,
          excludeTrivialRebase: false
        )
      ]
    )
  }
  options {
    disableConcurrentBuilds()
    timeout(time: 15, unit: 'MINUTES')
    buildDiscarder(logRotator(daysToKeepStr: '7', artifactDaysToKeepStr: '0'))
    timestamps()
  }
  stages {
    stage('Build') {
      steps {
        container('tools') {
          sh 'make clean build JFROG_CLI_VERSION=${jfrog_cli_version} DOCKER_CLI_VERSION=${docker_cli_version} BUILD_ID=$BUILDID'
        }
      }
    }
    stage('Run') {
      steps {
        container('tools') {
          sh 'make run-only BUILD_ID=$BUILDID'
        }
      }
    }
    stage('Test') {
      environment {
        AGENT_KEY = credentials("jenkins-agent-ssh-key-file")
      }
      steps {
        container('tools') {
          sh 'make test $AGENT_KEY'
        }
      }
    }
    stage('Tag release candidate') {
    // when { environment name: 'GERRIT_EVENT_TYPE', value: 'change-merged' }
      steps {
        container('tools') {
          // sh 'make tag DOCKER_REGISTRY=$DOCKER_REGISTRY_URL DOCKER_REPO_NAME=${SOURCE_REPO} TAG_NAME=${tag_name} BUILD_ID=$BUILDID'
          sh 'make tag DOCKER_REGISTRY=$DOCKER_REGISTRY_URL DOCKER_REPO_NAME=${SOURCE_REPO} TAG_NAME=${latest_tag_name} BUILD_ID=$BUILDID'
        }
      }
    }
   stage('Publish release candidate and promote') {
    // when { environment name: 'GERRIT_EVENT_TYPE', value: 'change-merged' }
      steps {
        container('jnlp') {
            script {
              def rtServer = Artifactory.server env.ARTIFACTORY_SERVER_ID
              rtServer.credentialsId = 'npmvs-artifactory'
              def rtDocker = Artifactory.docker server: rtServer, host: env.DOCKER_HOST
              // def rtBuildInfo = rtDocker.push "${SOURCE_REPO}.${ARTIFACTORY_URL}/${DOCKER_IMAGE}:${tag_name}", env.SOURCE_REPO
              def rtBuildInfoLatest = rtDocker.push "${SOURCE_REPO}.${ARTIFACTORY_URL}/${DOCKER_IMAGE}:${latest_tag_name}", env.SOURCE_REPO

              //Append the "latest" tagged image buildinfo object with version tagged image
              // rtBuildInfo.append rtBuildInfoLatest
              //rtBuildInfo.env.filter.addExclude("*CREDENTIAL*")
              // rtBuildInfo.env.capture = true

              //The environment collection is broken
              //JIRA - https://greenhopper.app.alcatel-lucent.com/browse/DO-4540
              //rtBuildInfo.env.collect()

              // Apply the buildDiscarder configuration in Jenkins to the job's builds published to Artifactory
              cto.devops.jenkins.Utils.updateBuildRetention(currentBuild, rtBuildInfoLatest)

              publishBuildInfo buildInfo: rtBuildInfoLatest, server: rtServer

              // Allow interactive promotion of the build
              def rtPromotionConfig = [
                  'buildName'          : rtBuildInfoLatest.name,
                  'buildNumber'        : rtBuildInfoLatest.number,
                  'status'             : 'Released',
                  'sourceRepo'         : env.SOURCE_REPO,
                  'targetRepo'         : env.TARGET_REPO,
                  'includeDependencies': false,
                  'copy'               : true, // "copy" must be used because "move" requires delete permission
                  'failFast'           : true
              ]

              //Programmatic Promotion
              rtServer.promote(rtPromotionConfig)
              //Interactive Promotion
              //Artifactory.addInteractivePromotion promotionConfig: rtPromotionConfig, server: rtServer, displayName: "Promote release candidate"
            }
        }
      }
    }
  }
  post {
    always {
      cleanWs()
    }
  }
}

```

Change Makefile:

```makefile
CONTAINER_START_DURATION ?= 30
DOCKER_REGISTRY ?= repo.lab.pl.alcatel-lucent.com
JFROG_CLI_VERSION ?= 1.14.0
DOCKER_CLI_VERSION ?= 17.11.0
MAJOR_MINOR_VNO ?= 7.3
MAJOR_VNO ?= 7


IMAGE_NAME = npmvs_rh7_jdk8_gradle
ifdef BUILD_ID
	TMP_IMAGE_NAME = $(IMAGE_NAME)-$(BUILD_ID)
else
	TMP_IMAGE_NAME = $(IMAGE_NAME)
endif

ifdef http_proxy
	EXTRA_BUILD_ARGS += --build-arg http_proxy=$(http_proxy)
endif
ifdef https_proxy
	EXTRA_BUILD_ARGS += --build-arg https_proxy=$(https_proxy)
endif
ifdef no_proxy
	EXTRA_BUILD_ARGS += --build-arg no_proxy=$(no_proxy)
endif

	EXTRA_BUILD_ARGS += --build-arg ARTIFACTORY_URL=$(ARTIFACTORY_URL)

help: ## Print usage
	@echo -e "$$(grep -hE '^\S+:.*##' $(MAKEFILE_LIST) | sed -e 's/:.*##\s*/:/' -e 's/^\(.\+\):\(.*\)/\\x1b[36m\1\\x1b[m:\2/' | column -c2 -t -s :)"

all: run test ## 'run test'


run: clean build run-only ## 'clean build run-only'

run-only: ## Run container at port 2227 and sleep for CONTAINER_START_DURATION [Required parameters - BUILD_ID, CONTAINER_START_DURATION]
	docker run -p 2227:22 --name=$(TMP_IMAGE_NAME) -d --restart always $(TMP_IMAGE_NAME)
	sleep $(CONTAINER_START_DURATION)

test: ## Verify the release
	ssh -i $(AGENT_KEY) -oStrictHostKeyChecking=no jenkins@localhost -p 2227 'grep "Red Hat Enterprise Linux" /etc/redhat-release' || (echo "Test failed!" && exit 1)

build: ## Build docker image [Required parameters - BUILD_ID, EXTRA_BUILD_ARGS]
	mkdir -p to_docker
	curl -SO $$ARTIFACTORY_HTTPS_URL/jfrog-cli-go/$(JFROG_CLI_VERSION)/jfrog-cli-linux-amd64/jfrog
	cp jfrog to_docker/jfrog
	curl -sO $$ARTIFACTORY_HTTPS_URL/download-docker-com/linux/static/test/x86_64/docker-$(DOCKER_CLI_VERSION)-ce-rc2.tgz
	tar xf docker-$(DOCKER_CLI_VERSION)-ce-rc2.tgz -C to_docker --strip-components=1
	cp Dockerfile to_docker/Dockerfile
	sed -e s=LOCAL_ARTIFACTORY=$$ARTIFACTORY_HTTPS_URL= \
		`pwd`/files/rhel.repo > to_docker/rhel.repo
	sed -e s=LOCAL_ARTIFACTORY=$$ARTIFACTORY_HTTPS_URL= \
                `pwd`/files/jfrog-cli.conf > to_docker/jfrog-cli.conf
	cp -R files to_docker
	docker build $(EXTRA_BUILD_ARGS) -t $(TMP_IMAGE_NAME) to_docker

clean: ## Stop and remove $(TMP_IMAGE_NAME) container and remove public key from cache
	docker stop $(TMP_IMAGE_NAME) || true # Not checking for return code as stop if not running
	rm -rf to_docker
ifeq ($(shell docker inspect $(TMP_IMAGE_NAME) &> /dev/null; echo $$?), 0)
	docker rm $(TMP_IMAGE_NAME) || true # Not checking for return code as rm will fail if still running
	ssh-keygen -R [localhost]:$(DYNAMIC_PORT)
endif

clean-all: clean ## 'clean' and remove image
	docker images | grep "$(TMP_IMAGE_NAME)" | awk '{print $$1 ":" $$2}' | xargs --no-run-if-empty docker rmi

stop: ## Stop container
	docker stop $(TMP_IMAGE_NAME)

start: ## Start container
	docker start $(TMP_IMAGE_NAME)

restart: stop start ## 'stop start'

tag: ## Tag image [Required parameters - BUILD_ID, TAG_NAME={yyyyMMddHHmm|latest}, DOCKER_REGISTRY]
	docker tag $(TMP_IMAGE_NAME) $(DOCKER_REPO_NAME).$(DOCKER_REGISTRY)/$(IMAGE_NAME):$(TAG_NAME)

```

And push it to Gerrit repository just created.

Visit link 

```
https://build.lab.pl.alcatel-lucent.com/job/DigitalOperations/job/VitalSuite/job/docker-images/
```

Create new pipeline with name `npmvs_rh7_jdk8_gradle`

![1529484343757](assets/1529484343757.png)

Configure pipeline 

![1529318019150](assets/1529318019150.png)

Build every push to Gerrit:

![1529486136359](assets/1529486136359.png)

Result:

![1529486916829](assets/1529486916829.png)

Pushing image log:

![1529486987330](assets/1529486987330.png)

Full log: https://build.lab.pl.alcatel-lucent.com/job/DigitalOperations/job/VitalSuite/job/docker-images/job/npmvs_rh7_jdk8_gradle/52/consoleFull

Pull a image from Artifact:

![1529487654312](assets/1529487654312.png)

```
sudo docker pull npmvs-docker-releases.repo.lab.pl.alcatel-lucent.com/npmvs_rh7_jdk8_gradle:latest
```

with format:

```bash
# Syntax
# docker pull <registry hostname and port>/<image path>:<tag>
  
# Example
docker pull <pu>-docker-candidates.repo.lab.pl.alcatel-lucent.com/build_centos7:latest-release
```

try recompile VS code

![1529568909862](assets/1529568909862.png)

Now, cleanup builder script (only build java on linux):

```shell
#!/bin/ksh -x

# Commmand: time vs/bldvnlinux_mariadb_java.sh 2>&1 | tee my_build.log

source $(dirname ${.sh.file})/../svn_migration/set_build_env_mariadb.sh
PATH=/opt/ActivePython-2.7/bin:/opt/sun/sunstudio12.1/bin:/bin:/usr/sbin:/usr/bin:/usr/ucb:/usr/local/bin:/usr/ccs/bin:/opt/csw/bin:
LM_LICENSE_FILE=/etc/opt/licenses/licenses_combined
export PATH LM_LICENSE_FILE

VN_ARCHIVE=/VNarchive
VV_ARCHIVE=/VVarchive
#
[ -z "$BuildART" ] && BuildART=1
#
# today=`date`
VS=`/usr/bin/dirname $0`       # if run with no full path, VS will be .
echo "running $0 in $VS"
echo "PATH is $PATH"
export TMA

cd $VS
VS=`pwd`                       # to get full path when run without full path


#build jar packages
if [ $JAVA_HOME ]; then
	echo "Use JAVAHOME = $JAVA_HOME"
else
	JAVA_HOME=/opt/jdk8/jdk1.8.0_77; export JAVA_HOME
fi

PATH=/opt/gradle/latest/bin:$PATH:${BR_ROOT}/noscm/ant/bin
export PATH

sh bldjavacommon.sh 1> bldjava_${postfix} 2>&1
sh bldvitalflow.sh 1>> bldjava_${postfix} 2>&1
sh bldjavabda.sh 1>> bldjava_${postfix} 2>&1

if [ $BuildART -eq 1 ]; then
  #The following is for VitalART
  AXIS_HOME=${BR_ROOT}/thdparty/Axis/Axis-1.2.1; export AXIS_HOME
  cd $VS/licensing/jnivslicense
  # [ "$5" = "clean" ] && make cfg=$cfg clean >> clean_${postfix} 2>&1
  make cfg=$cfg -f Makefile 1> $VS/vv_${postfix} 2>&1
  cd $VS
  sh bldvitalart.sh 1>> bldjava_${postfix} 2>&1
  cd $VS
fi

```

Command build:

```shell
time vs/bldvnlinux_mariadb_java.sh 2>&1 | tee my_build.log
```

## Pipeline build java on linux

Create a pipeline 

![1529651544629](assets/1529651544629.png)

build every commit

![1529651569766](assets/1529651569766.png)

pipeline from Jenkinsfile 

![1529651599786](assets/1529651599786.png)





































## Reference

- https://unix.stackexchange.com/questions/205010/centos-7-rename-network-interface-without-rebooting
- https://confluence.app.alcatel-lucent.com/pages/viewpage.action?spaceKey=AACTODEVOPS&title=Getting+started+-+Jenkins+build+farm
- https://confluence.app.alcatel-lucent.com/display/AACTODEVOPS/Docker+images
- https://confluence.app.alcatel-lucent.com/display/AACTODEVOPS/Pipeline+jobs
- https://gerrit.ext.net.nokia.com/gerrit/gitweb?p=AADO/docker/tools.git;a=blob;f=Jenkinsfile;hb=refs/heads/master
- https://gerrit.app.alcatel-lucent.com/gerrit/gitweb?p=AADO/docker/build_centos7.git;a=tree;h=refs/heads/master;hb=refs/heads/master
- https://confluence.app.alcatel-lucent.com/display/AACTODEVOPS/On-boarding+of+PUs
- https://confluence.app.alcatel-lucent.com/display/AACTODEVOPS/Software+repositories#Softwarerepositories-Docker