# This is the configuration file used by the Gerrit server-side hooks
# to enforce project policies. The syntax of the configurarion is:
# [HEADING]
# variable_name1=value1
# variable_name2=value2
# ...etc...
#
# Currently supported HEADING types are:
# 1. JIRA (optional)
# 2. FILEPOLICY (optional)
# 3. MESSAGE (optional)
# 4. NOTIFICATION (optional)
# -------

# JIRA heading is currently mandatory : this may be changed in future hooks-code releases
[JIRA]
# definition of server is currently mandatory : this may be changed in future hooks-code releases
# if the project does not use JIRA, comment out the proxy definition and set other JIRA definitions to false
# the hooks-code will then not enforce any JIRA policies
# server=<server url>
server=https://greenhopper.app.alcatel-lucent.com/

# JIRA definitions below this point are optional.

# For some external JIRA servers, you may have to uncomment to use a proxy to reach it.
# proxy=https://global.proxy.alcatel-lucent.com:8000

# jiraidmandatory=true/false    If true, hooks will enforce that commit messages begin with text which looks like a JIRA ID
jiraidmandatory=true

# checkidinjira=true/false      If true, hooks will check that the JIRA ID is actually a valid JIRA issue
checkidinjira=true

# jiraidmatch=true/false        If false, hooks will not perform JIRA checks relating to usermatch or statusmatch. If true, hooks will check that the JIRA ID is actually a valid JIRA issue.
jiraidmatch=false

# usermatch=true/false          If true, hooks will check that the user who made the commit is the assignee in JIRA
usermatch=false

# statusmatch=true/false        If true, hooks will check that the referenced JIRA issue has state in the list defined by validstates
statusmatch=false

# validstates=<valid JIRA states delimited by comma>     Mandatory if statusmatch set to true. Used with statusmatch.
# validstates=Open,Reopened,In Progress
validstates=Open,Reopened,In Progress

# updatejirawithchangeset=true/false    If true, hooks will attempt to update the referenced JIRA issue with the changeset info derived from the commit
updatejirawithchangeset=true

# scmfield=comments     Mandatory if updatejirawithchangeset set to true        This option is about which field in a JIRA issue is updated. Only comments is currently supported.
scmfield=comments


# FILEPOLICY heading and definitions are optional
[FILEPOLICY]

# checkforbinary=true/false      If true, hooks will enforce a policy to stop binary files being added to the repository
checkforbinary=true

# preventrenamesordeletions=true/false   If true, hooks will enforce a policy to prevent rename or deletion of files or directories in the repository by users who are not in the -Config-Mgmt or Administrator group
preventrenamesordeletions=false

# You can set all the character in this parameter,then hook will check whether these illegal characters is in filename.
# For example illegal_characters=\?<>:*^ ,then hook will check "\","?","<",">",":","*","|",""","^"
# illegal_characters=\?<>:*^
# If parameter illegal_characters is empty, hook will not check the illegal characters.
illegal_characters=

# symbolic_link_check=true/false   If true, hook will check whether there is a symbolic link in each commit.
symbolic_link_check=false

# case_conflict_check=true/false   If true, hook will check each new add file whether there is existing case conflict in repository
case_conflict_check=false

# file_size_threshold=size/false   File size threshold of checking, can include K/M for kilo/mega bytes.
#                                  Set to '0' or 'false' will not check file size
file_size_threshold = 500K

# REVIEWERS heading and definitions are optional
[REVIEWERS]

# autoadd=gitowners/false    If gitowners, hook will add reviewers automaticly as the rule configured in the .reviewaccess
autoadd=gitowners

#MESSAGE heading and definitions are optional
[MESSAGE]

#you can set one regular expression of Python here, then hook will check whether your comment message match regular expression. if can't match Hooks Gerrit will show you error in the History section.
#For example: our Gerrit support team, comment_regexp=^[A-Z]{1,5}-\d{1,5}:hooks-version-\d{1,3}\.\d-?.*  this regular expression will match your commit message "TBX-13:hooks-version-3.5"
comment_regexp=false

[NOTIFICATION]

# For example notify_mail=A.a.gerrit@alcatel-sbell.com.cn,B.b.gerrit@alcatel-sbell.com.cn
#if you put email address with bad format, The script will ignore them. such as:A,B,C. B is bad format. the scritp will only send email to A and C.
#In other words, The scirpt can't check your email if is valid, it only check format, you should check your email by yourself
#if you push your code to branch directly, Also you want to inform someone to let them know you have push code, you can use this parameter.
#And, you should notice that if you submit your review into repository, you also will revieve such email
notify_mail=false
