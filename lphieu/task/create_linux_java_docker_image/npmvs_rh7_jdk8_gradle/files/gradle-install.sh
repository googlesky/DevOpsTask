#!/bin/bash

ARTIFACTORY_HTTPS_URL=$1
GRADLE_VERSION=$2
GRADLE_SHA_1=$3
VERSION_ARRAY=( ${GRADLE_VERSION//./ } )
MAJOR_VERSION=$VERSION_ARRAY

curl -O $ARTIFACTORY_HTTPS_URL/gradle-dist/gradle-$GRADLE_VERSION-bin.zip
sha1sum gradle-$GRADLE_VERSION-bin.zip | grep $GRADLE_SHA_1
ls /tmp
unzip gradle-$GRADLE_VERSION-bin.zip -d /opt
rm gradle-$GRADLE_VERSION-bin.zip
chown -R jenkins:jenkins /opt/gradle-$GRADLE_VERSION
ln -s /opt/gradle-$GRADLE_VERSION /opt/gradle$MAJOR_VERSION
ln -s /opt/gradle$MAJOR_VERSION/bin/gradle /usr/local/bin/gradle$MAJOR_VERSION
chown -R jenkins:jenkins /usr/local/bin/gradle$MAJOR_VERSION
