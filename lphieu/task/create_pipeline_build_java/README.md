# Create pipeline build java for BDA

From Jenkins installed from vlab. Create new pipeline 

![1529902297091](assets/1529902297091.png)

Add new Credentials

![1529902440742](assets/1529902440742.png)

Use Jenkinsfile from BDA folder

![1529902533768](assets/1529902533768.png)

Change proxy script for access Jenkins server from TMA

```javascript
function FindProxyForURL(url, host) {
        var goDirect = isPlainHostName(host) || dnsDomainIs(host, ".tma.com.vn") || isInNet(host, "192.168.0.0", "255.255.0.0")
                || dnsDomainIs(host, ".skype.com") || dnsDomainIs(host, ".skypeassets.com") || dnsDomainIs(host, ".live.com") || dnsDomainIs(host, ".google.com") || dnsDomainIs(host, ".google.com.vn")
                || dnsDomainIs(host, ".facebook.com") || dnsDomainIs(host, ".fbcdn.net")
                || isInNet(host, "13.229.88.0", "255.255.0.0") || isInNet(host, "135.220.0.0", "255.255.0.0") || isInNet(host, "135.3.0.0", "255.255.0.0")
                || dnsDomainIs(host, ".vsdoc");

        var useSOCK = dnsDomainIs(host, ".alcatel-lucent.com") || isInNet(host, "10.75.197.0", "255.255.255.0") || dnsDomainIs(host, ".lucent.com") || dnsDomainIs(host, "net.nokia.com") || dnsDomainIs(host, "inside.nsn.com");

        if (goDirect) {
                return "DIRECT; PROXY proxy.tma.com.vn:8080;";
        } else if (useSOCK){
                return "SOCKS5 192.168.88.57:8000";
        } else {
                return "PROXY proxy.tma.com.vn:8080; DIRECT";
        }
}
```

set Timeout for `git clone`

![1529927121062](assets/1529927121062.png)



















# Reference